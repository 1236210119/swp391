package Until;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Admin
 */
public class Account {

    private int id;
    private String username;
    private String password;
    private String email;
    private String address;
    private String fullname;
    private String phone;
    private int isPayfee;
    private int isAdmin;
    private String avatar;
    private int isStatus;
    private String verificationCode; 
    private java.sql.Timestamp verificationCodeExpiry; 

    private List<String> roles = new ArrayList<>();

    public Account() {
    }

    public Account(int id, String username, String password, String email, String address, String fullname, String phone, int isPayfee, int isAdmin, String avatar, int isStatus) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.email = email;
        this.address = address;
        this.fullname = fullname;
        this.phone = phone;
        this.isPayfee = isPayfee;
        this.isAdmin = isAdmin;
        this.avatar = avatar;
        this.isStatus = isStatus;

        this.roles.add("USER");
        if (this.isPayfee == 1) {
            this.roles.add("PAYFEE");
        }
        if (this.isAdmin == 1) {
            this.roles.add("ADMIN");
        }
        if (this.isStatus == 1) {
            this.roles.add("BLOCK");
        }
    }

    public int getIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(int isAdmin) {
        this.isAdmin = isAdmin;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getIsPayfee() {
        return isPayfee;
    }

    public void setIsPayfee(int isPayfee) {
        this.isPayfee = isPayfee;
    }

    public int getIsStatus() {
        return isStatus;
    }

    public void setIsStatus(int isStatus) {
        this.isStatus = isStatus;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    public String getVerificationCode() {
        return verificationCode;
    }

    public void setVerificationCode(String verificationCode) {
        this.verificationCode = verificationCode;
    }

    public java.sql.Timestamp getVerificationCodeExpiry() {
        return verificationCodeExpiry;
    }

    public void setVerificationCodeExpiry(java.sql.Timestamp verificationCodeExpiry) {
        this.verificationCodeExpiry = verificationCodeExpiry;
    }
}
