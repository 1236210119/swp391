package Until;

import java.sql.Timestamp;

public class RequestStatus {
    private int requestID;
    private int userID;
    private String type;
    private String purpose;
    private Timestamp createDate;
    private String processNote;
    private String file;
    private String status;
    private Timestamp lastUpdate;

    public RequestStatus() {
    }

    public RequestStatus(int requestID, int userID, String type, String purpose, Timestamp createDate, String processNote, String file, String status, Timestamp lastUpdate) {
        this.requestID = requestID;
        this.userID = userID;
        this.type = type;
        this.purpose = purpose;
        this.createDate = createDate;
        this.processNote = processNote;
        this.file = file;
        this.status = status;
        this.lastUpdate = lastUpdate;
    }

    // Getters and Setters
    public int getRequestID() {
        return requestID;
    }

    public void setRequestID(int requestID) {
        this.requestID = requestID;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    public Timestamp getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
    }

    public String getProcessNote() {
        return processNote;
    }

    public void setProcessNote(String processNote) {
        this.processNote = processNote;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Timestamp getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Timestamp lastUpdate) {
        this.lastUpdate = lastUpdate;
    }
}
