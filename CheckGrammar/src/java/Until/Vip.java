/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Until;

import java.time.LocalDate;
import java.util.Date;

/**
 *
 * @author admin
 */
public class Vip {
    private int vipID;
    private int userID;
    private int day;
    private LocalDate createDate;

    // Constructor
    public Vip() {
        // Default constructor
    }

    // Getter and Setter methods
    public int getVipID() {
        return vipID;
    }

    public void setVipID(int vipID) {
        this.vipID = vipID;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public LocalDate getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDate createDate) {
        this.createDate = createDate;
    }

    // Optional: toString() method for debugging
    @Override
    public String toString() {
        return "Vip{" +
                "vipID=" + vipID +
                ", userID=" + userID +
                ", day=" + day +
                ", createDate=" + createDate +
                '}';
    }
}
