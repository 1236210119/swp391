/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Until;

import java.util.Date;

/**
 *
 * @author admin
 */
public class Blog {

    private int id;
    private String name;
    private String image;
    private int blogID;
    private String userID;
    private String createdBy;
    private String title;
    private String detail;
    private String blogImage;
    private String blogContent;
    private int isStatus;
    private String nameadmin;
    private Date create;
    private boolean favorite;

    public Blog() {
    }

    public boolean isFavorite() {
        return favorite;
    }

    public void setFavorite(boolean favorite) {
        this.favorite = favorite;
    }
    

    public Blog(int blogID, String userID, String createdBy, String title, String detail, String blogContent, String blogImage, int isStatus) {
        this.blogID = blogID;
        this.userID = userID;
        this.createdBy = createdBy;
        this.title = title;
        this.detail = detail;
        this.blogContent = blogContent;
        this.blogImage = blogImage;
        this.isStatus = isStatus;
    }

    public String getNameadmin() {
        return nameadmin;
    }

    public void setNameadmin(String nameadmin) {
        this.nameadmin = nameadmin;
    }

    public Date getCreate() {
        return create;
    }

    public void setCreate(Date create) {
        this.create = create;
    }

    public Blog(int blogID, String userID, String createdBy, String title, String detail) {
        this.blogID = blogID;
        this.createdBy = createdBy;
        this.title = title;
        this.detail = detail;
        this.userID = userID;
    }

    public Blog(String userID, String createdBy, String title, String detail, String blogImage, int isStatus) {
        this.userID = userID;
        this.createdBy = createdBy;
        this.title = title;
        this.detail = detail;
        this.blogImage = blogImage;

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getBlogID() {
        return blogID;
    }

    public void setBlogID(int blogID) {
        this.blogID = blogID;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getBlogImage() {
        return blogImage;
    }

    public void setBlogImage(String blogImage) {
        this.blogImage = blogImage;
    }

    public String getBlogContent() {
        return blogContent;
    }

    public void setBlogContent(String blogContent) {
        this.blogContent = blogContent;
    }

    public int getIsStatus() {
        return isStatus;
    }

    public void setIsStatus(int isStatus) {
        this.isStatus = isStatus;
    }

    @Override
    public String toString() {
        return "Blog{" + "blogID=" + blogID + ", favorite=" + favorite + '}';
    }

   

}
