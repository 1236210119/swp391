package Until;

import java.util.Date;

/**
 *
 * @author admin
 */
public class History {

    private int HistoryID;
    private int UserID;
    private String TextContent;
    private String WrongContent;
    private String type;
    private Date UploadDate;

    public History() {
    }

    public History(int HistoryID, int UserID, String TextContent, String WrongContent, String type, Date UploadDate) {
        this.HistoryID = HistoryID;
        this.UserID = UserID;
        this.TextContent = TextContent;
        this.WrongContent = WrongContent;
        this.type = type;
        this.UploadDate = new Date(); // Tự động lấy ngày hiện tại
    }

    public int getHistoryID() {
        return HistoryID;
    }

    public void setHistoryID(int HistoryID) {
        this.HistoryID = HistoryID;
    }

    public int getUserID() {
        return UserID;
    }

    public void setUserID(int UserID) {
        this.UserID = UserID;
    }

    public String getTextContent() {
        return TextContent;
    }

    public void setTextContent(String TextContent) {
        this.TextContent = TextContent;
    }

    public String getWrongContent() {
        return WrongContent;
    }

    public void setWrongContent(String WrongContent) {
        this.WrongContent = WrongContent;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getUploadDate() {
        return UploadDate;
    }

    public void setUploadDate(Date UploadDate) {
        this.UploadDate = UploadDate;
    }

    @Override
    public String toString() {
        return "History{" + "HistoryID=" + HistoryID + ", UserID=" + UserID + ", TextContent=" + TextContent + ", WrongContent=" + WrongContent + ", type=" + type + ", UploadDate=" + UploadDate + '}';
    }

}
