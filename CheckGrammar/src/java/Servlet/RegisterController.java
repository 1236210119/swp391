package Servlet;

import Mail.EmailSender;
import Until.Account;
import config.RandomCodeGenerator;
import dao.AccountDao;
import java.io.IOException;
import java.sql.Timestamp;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.logging.Level;
import java.util.logging.Logger;

public class RegisterController extends HttpServlet {

    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");
        String codeFromRequest = request.getParameter("code");

        if ("verify".equals(action)) {
            if (codeFromRequest == null || codeFromRequest.isEmpty()) {
                request.setAttribute("errorMessage", "Invalid verification code.");
                request.getRequestDispatcher("/view/register.jsp").forward(request, response);
                return;
            }

            AccountDao accountDao = new AccountDao();
            Account user = null;
            try {
                user = accountDao.getAccountByVerificationCode(codeFromRequest);
            } catch (Exception ex) {
                Logger.getLogger(RegisterController.class.getName()).log(Level.SEVERE, null, ex);
            }

            if (user == null) {
                request.setAttribute("errorMessage", "Registration failed. No user information found.");
                request.getRequestDispatcher("/view/register.jsp").forward(request, response);
                return;
            }

            boolean isVerified = false;
            try {
                isVerified = accountDao.verifyAccount(user.getId());
            } catch (Exception ex) {
                Logger.getLogger(RegisterController.class.getName()).log(Level.SEVERE, null, ex);
            }

            if (isVerified) {
                request.setAttribute("successMessage", "Registration successful.");
                request.getRequestDispatcher("/view/login.jsp").forward(request, response);
            } else {
                request.setAttribute("errorMessage", "Registration failed. Please try again.");
                request.getRequestDispatcher("/view/register.jsp").forward(request, response);
            }
        } else {
            // Nếu không có action là "verify", chỉ cần hiển thị trang đăng ký
            request.getRequestDispatcher("/view/register.jsp").forward(request, response);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        AccountDao accountDao = new AccountDao();

        String name = request.getParameter("name");
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        String confirmPassword = request.getParameter("password_confirmation");
        String address = request.getParameter("address");
        String phone = request.getParameter("phone");
        String username = request.getParameter("username");

        if (accountDao.isExistUsername(username)) {
            request.setAttribute("errorMessageUsername", "Username already exists.");
            forwardToRegisterPageWithInputData(request, response, name, username, email, address, phone);
            return;
        }

        if (!isValidEmail(email)) {
            request.setAttribute("errorMessageEmail1", "Invalid email format.");
            forwardToRegisterPageWithInputData(request, response, name, username, email, address, phone);
            return;
        }

        if (accountDao.isExistEmail(email)) {
            request.setAttribute("errorMessageEmail2", "Email already exists.");
            forwardToRegisterPageWithInputData(request, response, name, username, email, address, phone);
            return;
        }

        if (!password.equals(confirmPassword)) {
            request.setAttribute("errorMessage", "Passwords do not match.");
            forwardToRegisterPageWithInputData(request, response, name, username, email, address, phone);
            return;
        }

        if (!isValidPhone(phone)) {
            request.setAttribute("errorMessagePhone", "Invalid phone number format.");
            forwardToRegisterPageWithInputData(request, response, name, username, email, address, phone);
            return;
        }

        Account account = new Account();
        account.setUsername(username);
        account.setPassword(password);
        account.setEmail(email);
        account.setAddress(address);
        account.setFullname(name);
        account.setPhone(phone);
        account.setIsPayfee(0);
        account.setIsAdmin(0);
        account.setAvatar("");
        account.setIsStatus(0);

        String verificationCode = RandomCodeGenerator.generateCode();
        account.setVerificationCode(verificationCode);
        account.setVerificationCodeExpiry(new Timestamp(System.currentTimeMillis() + 30 * 60 * 1000)); // Mã hết hạn sau 30 phút

        boolean isRegistered = accountDao.registerAccount(account);

        if (!isRegistered) {
            request.setAttribute("errorMessage", "Registration failed. Please try again.");
            forwardToRegisterPageWithInputData(request, response, name, username, email, address, phone);
            return;
        }

        EmailSender emailSender = new EmailSender();
        String emailSubject = "Welcome to Check Grammar";
        String url = "http://localhost:8080/CheckGrammar/register?action=verify&code=" + verificationCode;
        String emailBody = "Dear " + name + ",\n\nYour verification code is: " + verificationCode + "\n\nClick the following link to complete your registration: " + url + "\n\nThank you for registering with us.";
        emailSender.sendEmail(email, emailSubject, emailBody);

        request.setAttribute("infoMessage", "We have sent an email. Please check and confirm registration!");
        forwardToRegisterPageWithInputData(request, response, name, username, email, address, phone);
    }

    private boolean isValidEmail(String email) {
        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\.[a-zA-Z0-9_+&*-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,7}$";
        return email.matches(emailRegex);
    }

    private boolean isValidPhone(String phone) {
        String phoneRegex = "^0\\d{9}$";
        return phone.matches(phoneRegex);
    }

    private void forwardToRegisterPageWithInputData(HttpServletRequest request, HttpServletResponse response, String name, String username, String email, String address, String phone)
            throws ServletException, IOException {
        request.setAttribute("nameValue", name);
        request.setAttribute("usernameValue", username);
        request.setAttribute("emailValue", email);
        request.setAttribute("addressValue", address);
        request.setAttribute("phoneValue", phone);
        request.getRequestDispatcher("/view/register.jsp").forward(request, response);
    }
}
