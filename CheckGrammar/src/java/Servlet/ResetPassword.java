package Servlet;

import Until.Account;
import dao.AccountDao;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Admin
 */
public class ResetPassword extends HttpServlet {

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        AccountDao accountDao = new AccountDao();
        String code = request.getParameter("code");
        Account user = null;

        if (code != null && !code.isEmpty()) {
            try {
                user = accountDao.getAccountByVerificationCode(code);
            } catch (Exception ex) {
                Logger.getLogger(ResetPassword.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        try {
            if (user == null || accountDao.isVerificationCodeExpired(user.getId())) {
                request.setAttribute("errorMessage", "Invalid or expired verification code.");
                request.getRequestDispatcher("/view/forgot.jsp").forward(request, response);
                return;
            }
        } catch (Exception ex) {
            Logger.getLogger(ResetPassword.class.getName()).log(Level.SEVERE, null, ex);
        }

        request.getSession().setAttribute("accForgot", user);
        request.getSession().setAttribute("code", code);
        request.getRequestDispatcher("/view/resetPassword.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        AccountDao accountDao = new AccountDao();
        HttpSession session = request.getSession();
        Account user = (Account) session.getAttribute("accForgot");
        
        if (user != null) {
            String password = request.getParameter("password");
            String confirmPassword = request.getParameter("password_confirmation");

            if (!password.equals(confirmPassword)) {
                request.setAttribute("errorMessage", "Passwords do not match.");
                request.getRequestDispatcher("/view/resetPassword.jsp").forward(request, response);
                return;
            }

            boolean isSuccess = false;
            try {
                isSuccess = accountDao.updatePassword(user.getId(), password);
            } catch (Exception ex) {
                Logger.getLogger(ResetPassword.class.getName()).log(Level.SEVERE, null, ex);
            }

            if (isSuccess) {
                try {
                    accountDao.clearVerificationCode(user.getId());
                } catch (Exception ex) {
                    Logger.getLogger(ResetPassword.class.getName()).log(Level.SEVERE, null, ex);
                }
                session.removeAttribute("code");
                session.removeAttribute("accForgot");
                request.setAttribute("successMessage", "Password reset successfully.");
                request.getRequestDispatcher("/view/login.jsp").forward(request, response);
                return;
            } else {
                request.setAttribute("errorMessage", "Password reset failed. Please try again.");
                request.getRequestDispatcher("/view/resetPassword.jsp").forward(request, response);
                return;
            }
        }

        request.setAttribute("errorMessage", "Password reset failed. Please try again.");
        request.getRequestDispatcher("/view/resetPassword.jsp").forward(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
