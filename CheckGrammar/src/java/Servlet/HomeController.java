/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Servlet;

import Until.Account;
import Until.Blog;
import Until.Feedback;
import dao.BlogDao;
import dao.FeedbackDAO;
import dao.WordDAO;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author admin
 */
public class HomeController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
//        int page = 1;
//        String paging = request.getParameter("page");
//        if (paging != null) {
//            page = Integer.parseInt(paging);
//        }
        BlogDao blogDao = new BlogDao();
//        List<Blog> blogs = blogDao.getAll();
//        int totalPage = blogs.size() % 5 == 0 ? (blogs.size() / 5) : (blogs.size() / 5 + 1);
//        List<Blog> subList = blogs.subList((page - 1) * 5, Math.min(blogs.size(), page * 5));
//        request.setAttribute("blogs", subList);
//        request.setAttribute("total", totalPage);
//        request.setAttribute("page", page);
        FeedbackDAO fbDao = new FeedbackDAO();

          int currentPage = 1;
    int itemsPerPage = 3; // Default value

    // Try to get 'page' and 'size' from request parameters
    try {
        if (request.getParameter("page") != null) {
            currentPage = Integer.parseInt(request.getParameter("page"));
        }
        if (request.getParameter("size") != null) {
            itemsPerPage = Integer.parseInt(request.getParameter("size"));
        }
    } catch (NumberFormatException e) {
        // Use default values if parsing fails
    }

    // Get the total number of feedbacks
    int totalFeedbacks = fbDao.getTotalFeedbackCount();
    
    // Calculate total pages
    int totalPages = (int) Math.ceil((double) totalFeedbacks / itemsPerPage);

    // Ensure currentPage is within bounds
    if (currentPage < 1) {
        currentPage = 1;
    } else if (currentPage > totalPages) {
        currentPage = totalPages;
    }

    // Calculate the start index for the current page
    int startIndex = (currentPage - 1) * itemsPerPage;

    // Get the feedbacks for the current page
    ArrayList<Feedback> feedbacks = fbDao.getPaginatedFeedbacks(startIndex, itemsPerPage);

    // Set attributes for pagination and feedbacks
    request.setAttribute("currentPage", currentPage);
    request.setAttribute("totalPages", totalPages);
    request.setAttribute("itemsPerPage", itemsPerPage); // Also pass the itemsPerPage to JSP
    request.getSession().setAttribute("feedbacks", feedbacks);


      
        HttpSession session = request.getSession();
        Account user = (Account) session.getAttribute("loginedUser");

        if (user != null) {
            request.setAttribute("check", "check");
            request.setAttribute("userId", user.getId());
        } else {
            request.setAttribute("check", "nocheck");
            request.setAttribute("userId", null);
        }

        request.getRequestDispatcher("/view/home.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String text = request.getParameter("text");
        request.setAttribute("text", text);
        WordDAO wordDAO = new WordDAO();
        List<String> list = wordDAO.getWordsByContent(text);
        request.setAttribute("list", list);
        if (list.contains(text)) {
            request.setAttribute("report", "kiểm tra đúng chính tả");
        } else {
            request.setAttribute("error", "Không đúng chính tả hoặc từ này chưa tồn tại trong hệ thống");
        }
        BlogDao blogDao = new BlogDao();
        List<Blog> blogs = blogDao.getAll();
        request.setAttribute("blogs", blogs);
        request.getRequestDispatcher("/view/index.jsp").forward(request, response);
    }

}
