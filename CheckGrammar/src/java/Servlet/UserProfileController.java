package Servlet;

import Until.Account;
import dao.AccountDao;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import jakarta.servlet.http.Part;
import java.nio.file.Paths;
import java.io.File;
import java.nio.file.Files;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import org.apache.commons.io.FilenameUtils;

/**
 * Servlet implementation class UserProfileController
 */
@MultipartConfig
public class UserProfileController extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet UserProfileController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet UserProfileController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        Account accLogin = (Account) session.getAttribute("loginedUser");

        AccountDao ad = new AccountDao();

        Account acc = ad.getAccountByID(accLogin.getId());

        request.setAttribute("acc", acc);

        request.getRequestDispatcher("/view/viewprofile.jsp").forward(request, response);
    }

    @Override
protected void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
    String phone = request.getParameter("phone");
    String address = request.getParameter("address");
    String fullname = request.getParameter("fullname");
    int accId = Integer.parseInt(request.getParameter("Id"));

    AccountDao accDao = new AccountDao();
    Account acc = accDao.getAccountByID(accId);
    acc.setFullname(fullname);
    acc.setPhone(phone);
    acc.setAddress(address);

    List<Part> fileParts = request.getParts().stream()
            .filter(part -> "file".equals(part.getName()) && part.getSize() > 0 && !part.getSubmittedFileName().isEmpty())
            .collect(Collectors.toList());

    if (!fileParts.isEmpty()) {
        String realPath = getServletContext().getRealPath("") + File.separator + "images";
        for (Part part : fileParts) {
            UUID uuid = UUID.randomUUID();
            String filename = Paths.get(part.getSubmittedFileName()).getFileName().toString();
            String fileExtension = FilenameUtils.getExtension(filename);

            if (!Files.exists(Paths.get(realPath))) {
                Files.createDirectory(Paths.get(realPath));
            }

            filename = uuid + "." + fileExtension;
            part.write(realPath + File.separator + filename);

            String pathImage = "images" + "/" + filename;
            acc.setAvatar(pathImage);
        }
    }

    accDao.UpdateAccountProfile(acc);
    response.sendRedirect("profile");
}


    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
