package Servlet;

import Until.Account;
import Until.History;
import com.google.gson.JsonObject;
import dao.HistoryDAO;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.languagetool.JLanguageTool;
import org.languagetool.Languages;
import org.languagetool.rules.RuleMatch;
import org.languagetool.rules.spelling.SpellingCheckRule;
import org.languagetool.rules.Rule;

public class CheckSpellController extends HttpServlet {

    private final int NormalWordCheck = 500;
    private final int GuestWordCheck = 50;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("view/index.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        List<History> lHistory;
        HttpSession session = request.getSession();
        boolean isCheck = false;
        String message = "";

        String inputText = request.getParameter("input-text");
        int wordCount = inputText.split("\\s+").length;

        Account user = (Account) session.getAttribute("loginedUser");
        if (user != null) {
            System.out.print("abc");
            if (user.getIsPayfee() == 1) {
                isCheck = true;
            } else {
                if (wordCount <= NormalWordCheck) {
                    isCheck = true;
                } else {
                    message = "Logged in users can check texts up to " + NormalWordCheck + " words.";
                }
            }
        } else {
            if (wordCount <= GuestWordCheck) {
                isCheck = true;
            } else {
                message = "Guests can check texts up to " + GuestWordCheck + " words.";
            }
        }

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");

        // JSON response
        JsonObject jsonResponse = new JsonObject();
        PrintWriter out = response.getWriter();

        if (isCheck) {
            JLanguageTool langTool = new JLanguageTool(Languages.getLanguageForShortCode("en-GB"));
            // Disable all rules except spell checking
            List<Rule> allRules = langTool.getAllRules();
            for (Rule rule : allRules) {
                if (!(rule instanceof SpellingCheckRule)) {
                    langTool.disableRule(rule.getId());
                }
            }

            StringBuilder result = new StringBuilder();
            StringBuilder correctedText = new StringBuilder(inputText);
            StringBuilder resultHist = new StringBuilder();
            List<RuleMatch> matches = langTool.check(inputText);

            int count = 1;
            int errorCount = matches.size();
            int paragraphCount = inputText.split("\\r?\\n").length;
            double errorDensity = ((double) errorCount / wordCount) * 100;

            StringBuilder htmlResult = new StringBuilder();

            int lastPos = 0;
            int offsetCorrected = 0;
            for (RuleMatch match : matches) {
                int fromPos = match.getFromPos();
                int toPos = match.getToPos();
                String incorrectWord = inputText.substring(fromPos, toPos);

                resultHist.append("<div>");
                resultHist.append("Error #").append(count).append(": ");
                resultHist.append("Potential error from location ")
                        .append(match.getFromPos()).append(" to ").append(match.getToPos()).append(": ")
                        .append(match.getMessage());
                resultHist.append("</div>");

                resultHist.append("<div>");
                resultHist.append("Wrong word #").append(errorCount).append(": \"");
                resultHist.append("<span style=\"color: red;\">");
                resultHist.append(incorrectWord);
                resultHist.append("</span>");
                resultHist.append("\"");
                resultHist.append("</div>");

                resultHist.append("<div>");

                result.append("<br>");
                htmlResult.append(inputText, lastPos, fromPos);

                htmlResult.append("<span style=\"color: red;\">").append(incorrectWord).append("</span>");

                lastPos = toPos;

                result.append("<li style=\"border: 1px solid black; box-shadow: 4px 4px 8px rgba(0, 0, 0, 0.1); border-radius: 4px; padding: 10px; margin: 5px; background-color: #f2f2f2;\">")
                        .append("<strong>").append(count).append("</strong> \"").append(incorrectWord).append("\" -&gt; ");

                List<String> suggestedCorrections = match.getSuggestedReplacements();

                String bestSuggestion = getBestSuggestion(incorrectWord, suggestedCorrections);
                result.append("\"").append(bestSuggestion).append("\"");
                resultHist.append("Error correction suggestions: ")
                        .append(bestSuggestion);
                resultHist.append("</div>");
                result.append("</li>");

                if (!suggestedCorrections.isEmpty()) {
                    correctedText.replace(fromPos + offsetCorrected, toPos + offsetCorrected, "<span style='color: green;'>" + bestSuggestion + "</span>");
                    offsetCorrected += ("<span style='color: green;'>" + bestSuggestion + "</span>").length() - incorrectWord.length();
                }
                
                count++;
            }

            htmlResult.append(inputText.substring(lastPos));
            if (user != null) {
                HistoryDAO histDAO = new HistoryDAO();
                History hist = new History();
                hist.setUserID(user.getId());
                hist.setTextContent(inputText);
                hist.setWrongContent(resultHist.toString());
                hist.setType("Check Spell");
                hist.setUploadDate(new Date());
                try {
                    histDAO.insertHistory(hist);
                } catch (Exception ex) {
                    Logger.getLogger(CheckSpellController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            jsonResponse.addProperty("spellCheckResults", result.toString());
            jsonResponse.addProperty("inputText", htmlResult.toString());
            jsonResponse.addProperty("wordCount", wordCount);
            jsonResponse.addProperty("paragraphCount", paragraphCount);
            jsonResponse.addProperty("errorCount", errorCount);
            jsonResponse.addProperty("errorDensity", errorDensity);
            jsonResponse.addProperty("correctedText", correctedText.toString());

        } else {
            jsonResponse.addProperty("error", message);
        }

        out.println(jsonResponse.toString());
        out.flush();
    }

    private String getBestSuggestion(String incorrectWord, List<String> suggestions) {
        // Simple heuristic to choose the best suggestion
        if (suggestions.isEmpty()) {
            return incorrectWord;
        }
        return suggestions.get(0);
    }
}
