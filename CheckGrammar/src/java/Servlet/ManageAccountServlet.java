package Servlet;

import dao.AccountDao;
import Until.Account;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Servlet implementation class ManageAccountServlet
 */
@WebServlet(name = "ManageAccountServlet", urlPatterns = {"/manage-account", "/details"})
public class ManageAccountServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String search = request.getParameter("search");
        String isPayFee = request.getParameter("isPayFee");
        String isStatus = request.getParameter("isStatus");
        int currentPage = request.getParameter("page") == null ? 1 : Integer.parseInt(request.getParameter("page"));

        AccountDao accountDao = new AccountDao();
        int pageSize = 5;
        int totalAccounts = accountDao.getTotalAccounts(search, isPayFee, isStatus);
        int endPage = (int) Math.ceil((double) totalAccounts / pageSize);
        List<Account> list = accountDao.getAccounts(search, isPayFee, isStatus, currentPage, pageSize);

        request.setAttribute("list", list);
        request.setAttribute("currentPage", currentPage);
        request.setAttribute("endPage", endPage);
        request.setAttribute("paramSearch", search);
        request.setAttribute("paramIsPayFee", isPayFee);
        request.setAttribute("paramIsStatus", isStatus);
        
        request.getRequestDispatcher("/view/Admin/managerAccount.jsp").forward(request, response);
    }

    protected void showAccountDetails(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int accountId = Integer.parseInt(request.getParameter("id"));
        AccountDao accountDao = new AccountDao();
        Account account = accountDao.getAccountByID(accountId);
        request.setAttribute("account", account);
        request.getRequestDispatcher("/view/Admin/accountDetail.jsp").forward(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getServletPath();
        if (action.equals("/details")) {
            showAccountDetails(request, response);
        } else {
            processRequest(request, response);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int accId = Integer.parseInt(request.getParameter("accId"));
        int status = Integer.parseInt(request.getParameter("status"));

        AccountDao accountDao = new AccountDao();
        accountDao.updateAccountStatus(accId, status);

        response.sendRedirect("manage-account");
    }

    @Override
    public String getServletInfo() {
        return "Manage Account Servlet";
    }
}
