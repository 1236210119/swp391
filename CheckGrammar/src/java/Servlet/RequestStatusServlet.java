package Servlet;

import Until.Account;
import Until.RequestStatus;
import dao.RequestStatusDao;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import jakarta.servlet.http.Part;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Timestamp;

/**
 *
 * @author admin
 */
@MultipartConfig
public class RequestStatusServlet extends HttpServlet {

    private static final String UPLOAD_DIR = "uploads";

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        Account user = (Account) session.getAttribute("loginedUser");
        if (user == null) {
            response.sendRedirect("login");
            return;
        }
        request.getRequestDispatcher("/view/requestStatus.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        Account user = (Account) session.getAttribute("loginedUser");
        if (user == null) {
            response.sendRedirect("login");
            return;
        }

        String errorType = request.getParameter("errorType");
        String description = request.getParameter("description");
        Part filePart = request.getPart("fileUpload");

        String filePath = null;
        if (filePart != null && filePart.getSize() > 0) {
            String fileName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString();
            String uploadPath = getServletContext().getRealPath("") + File.separator + UPLOAD_DIR;
            File uploadDir = new File(uploadPath);
            if (!uploadDir.exists()) {
                uploadDir.mkdirs(); // Use mkdirs() to create any necessary parent directories
            }

            filePath = uploadPath + File.separator + fileName;

            try {
                Files.copy(filePart.getInputStream(), Paths.get(filePath));
            } catch (Exception e) {
                e.printStackTrace();
                request.setAttribute("errorMessage", "File upload failed: " + e.getMessage());
                request.getRequestDispatcher("/view/requestStatus.jsp").forward(request, response);
                return;
            }
        }

        Timestamp timenow = new Timestamp(System.currentTimeMillis());
        RequestStatus requestStatus = new RequestStatus();
        requestStatus.setType(errorType);
        requestStatus.setPurpose(description);
        requestStatus.setFile(filePath);
        requestStatus.setUserID(user.getId());
        requestStatus.setStatus("Waiting for response");
        requestStatus.setCreateDate(timenow);

        RequestStatusDao requestStatusDao = new RequestStatusDao();
        boolean isCreated = requestStatusDao.createRequestStatus(requestStatus);

        if (!isCreated) {
            request.setAttribute("errorMessage", "Failed to create request status.");
            request.getRequestDispatcher("/view/requestStatus.jsp").forward(request, response);
            return;
        }

        response.sendRedirect("viewRequestStatus");
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
