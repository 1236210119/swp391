/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Servlet;

import Mail.EmailSender;
import Until.Account;
import config.RandomCodeGenerator;
import dao.AccountDao;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author admin
 */
public class ForgotPasswordServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        request.getRequestDispatcher("/view/forgot.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        AccountDao accountDao = new AccountDao();
        String email = request.getParameter("email");

        if (!accountDao.isExistEmail(email)) {
            request.setAttribute("errorMessage", "Email does not exist!");
            request.getRequestDispatcher("/view/forgot.jsp").forward(request, response);
            return;
        }
        Account acc = null;
        try {
            acc = accountDao.getAccountByEmail(email);
        } catch (Exception ex) {
            Logger.getLogger(ForgotPasswordServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        String verificationCode = RandomCodeGenerator.generateCode();
        try {
            accountDao.updateVerificationCode(acc.getId(), verificationCode);
        } catch (Exception ex) {
            Logger.getLogger(ForgotPasswordServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        EmailSender emailSender = new EmailSender();
        String emailSubject = "Check Grammar - Forgot Password";
        String Url = "http://localhost:8080/CheckGrammar/resetpass";
        String emailBody = "\n\nYour verification code is: " + Url + "?code=" + verificationCode + "\n\nClick the link to reset your password!";
        emailSender.sendEmail(email, emailSubject, emailBody);
        request.getSession().setAttribute("code", verificationCode);
        request.getSession().setAttribute("accForgot", acc);
        request.setAttribute("errorMessage", "We have sent an email. Please check now!");
        request.getRequestDispatcher("/view/forgot.jsp").forward(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
