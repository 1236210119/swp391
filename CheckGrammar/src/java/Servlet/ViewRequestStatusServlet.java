package Servlet;

import Until.Account;
import Until.RequestStatus;
import dao.RequestStatusDao;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.net.URLEncoder;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.sql.Date;

public class ViewRequestStatusServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        Account user = (Account) session.getAttribute("loginedUser");
        RequestStatusDao requestStatusDao = new RequestStatusDao();
        List<RequestStatus> rqStatusList = new ArrayList<>();
        if (user == null) {
            response.sendRedirect("login");
            return;
        }

        String status = request.getParameter("status");
        String startDateStr = request.getParameter("startDate");
        String endDateStr = request.getParameter("endDate");
        String purpose = request.getParameter("purpose");

        Date startDate = null;
        Date endDate = null;
        try {
            if (startDateStr != null && !startDateStr.isEmpty()) {
                startDate = Date.valueOf(startDateStr);
            }
            if (endDateStr != null && !endDateStr.isEmpty()) {
                endDate = Date.valueOf(endDateStr);
            }
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }

        int currentPage = 1;
        int recordsPerPage = 10;
        if (request.getParameter("page") != null) {
            currentPage = Integer.parseInt(request.getParameter("page"));
        }

        int rows;
        if (user.getIsAdmin() == 1) {
            request.setAttribute("isAdmin", true);
            rows = requestStatusDao.getNumberOfRows(status, startDate, endDate, purpose);
            rqStatusList = requestStatusDao.getFilteredRequestStatuses(status, startDate, endDate, purpose, (currentPage - 1) * recordsPerPage, recordsPerPage);
        } else {
            rows = requestStatusDao.getNumberOfRowsByUserID(user.getId(), status, startDate, endDate, purpose);
            rqStatusList = requestStatusDao.getFilteredRequestStatusesByUserID(user.getId(), status, startDate, endDate, purpose, (currentPage - 1) * recordsPerPage, recordsPerPage);
        }

        int nOfPages = rows / recordsPerPage;
        if (rows % recordsPerPage > 0) {
            nOfPages++;
        }

        List<RequestStatus> encodedRqStatusList = new ArrayList<>();
        for (RequestStatus statusObj : rqStatusList) {
            RequestStatus encodedStatus = new RequestStatus();
            encodedStatus.setUserID(statusObj.getUserID());
            encodedStatus.setRequestID(statusObj.getRequestID());
            encodedStatus.setType(statusObj.getType());
            encodedStatus.setPurpose(statusObj.getPurpose());
            encodedStatus.setCreateDate(statusObj.getCreateDate());
            encodedStatus.setProcessNote(statusObj.getProcessNote());
            if (statusObj.getFile() != null && !statusObj.getFile().isEmpty()) {
                encodedStatus.setFile(URLEncoder.encode(statusObj.getFile(), "UTF-8"));
            } else {
                encodedStatus.setFile(statusObj.getFile());
            }
            encodedStatus.setStatus(statusObj.getStatus());
            encodedStatus.setLastUpdate(statusObj.getLastUpdate());
            encodedRqStatusList.add(encodedStatus);
        }

        request.setAttribute("listRequest", encodedRqStatusList);
        request.setAttribute("noOfPages", nOfPages);
        request.setAttribute("currentPage", currentPage);

        request.getRequestDispatcher("/view/viewRequestStatus.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestStatusDao requestStatusDao = new RequestStatusDao();
        String idRequest = request.getParameter("idRequest");
        String feedbackContent = request.getParameter("feedbackContent");
        try {
            int idRequests = Integer.parseInt(idRequest);
            RequestStatus rqStatusList = requestStatusDao.getRequestStatusByID(idRequests);
            if (rqStatusList != null) {

                Timestamp timenow = new Timestamp(System.currentTimeMillis());

                requestStatusDao.updateRequestStatus(idRequests, feedbackContent, "Processed", timenow);
                response.sendRedirect("viewRequestStatus");
            }
        } catch (NumberFormatException e) {
            e.printStackTrace(); // Hoặc log lại lỗi để debug
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
