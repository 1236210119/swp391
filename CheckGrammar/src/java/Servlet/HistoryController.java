package Servlet;

import Until.Account;
import Until.History;
import dao.AccountDao;
import dao.HistoryDAO;
import java.io.IOException;
import java.util.List;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.logging.Level;
import java.util.logging.Logger;

public class HistoryController extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, Exception {
        response.setContentType("text/html;charset=UTF-8");

        HttpSession session = request.getSession();
        Account accLogin = (Account) session.getAttribute("loginedUser");

        if (accLogin == null) {
            response.sendRedirect("login.jsp");
            return;
        }

        Integer userID = accLogin.getId();
        String searchText = request.getParameter("searchText");
        String startDate = request.getParameter("startDate");
        String endDate = request.getParameter("endDate");
        String searchType = request.getParameter("searchType");
        int page = 1;
        int recordsPerPage = 5;

        if (request.getParameter("page") != null) {
            page = Integer.parseInt(request.getParameter("page"));
        }

        HistoryDAO historyDAO = new HistoryDAO();
        List<History> histories;

        if ((searchText != null && !searchText.trim().isEmpty())
                || (startDate != null && !startDate.trim().isEmpty())
                || (endDate != null && !endDate.trim().isEmpty())
                || (searchType != null && !searchType.trim().isEmpty())) {
            histories = historyDAO.searchHistories(userID, searchText, startDate, endDate, searchType, (page - 1) * recordsPerPage, recordsPerPage);
        } else {
            histories = historyDAO.getHistoriesByUser(userID, (page - 1) * recordsPerPage, recordsPerPage);
        }

        int noOfRecords = historyDAO.getNoOfRecords();
        int noOfPages = (int) Math.ceil(noOfRecords * 1.0 / recordsPerPage);

        request.setAttribute("histories", histories);
        request.setAttribute("noOfPages", noOfPages);
        request.setAttribute("currentPage", page);
        request.setAttribute("searchText", searchText);
        request.setAttribute("startDate", startDate);
        request.setAttribute("endDate", endDate);
        request.setAttribute("searchType", searchType);
        request.getRequestDispatcher("view/history.jsp").forward(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(HistoryController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(HistoryController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public String getServletInfo() {
        return "History Controller";
    }
}
