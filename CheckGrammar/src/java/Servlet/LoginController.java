/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Servlet;

import Until.Account;
import dao.AccountDao;
import dao.VipDao;
import Until.Vip;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import utils.AppUtils;

/**
 *
 * @author admin
 */
public class LoginController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        request.setAttribute("redirectId", request.getParameter("redirectId"));
        request.getRequestDispatcher("/view/login.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");

        AccountDao accountDAO = new AccountDao();

        String username = request.getParameter("user");
        String password = request.getParameter("pass");
        HttpSession session = request.getSession();
        Account user = null;
        try {
            user = accountDAO.login(username, password);
        } catch (Exception ex) {
            Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (user == null) {
            session.setAttribute("loginMessage", "Wrong account or password !");
            response.sendRedirect("login");
        }else if(user.getIsStatus()==0){
             session.setAttribute("loginMessage", "Account is blocked !!!");
            response.sendRedirect("login");
        }
        else {

            LocalDate currentDate = LocalDate.now();
            VipDao vipDao = new VipDao();
            Vip vip = vipDao.getVipByUserID(user.getId());
            if (vip != null) {
                LocalDate CreateDate = vip.getCreateDate();
                long daysBetween = ChronoUnit.DAYS.between(vip.getCreateDate(), currentDate);
                if (daysBetween < vip.getDay()) {
                    accountDAO.updateVip(user.getId(), 1);
                } else {
                    accountDAO.updateVip(user.getId(), 0);
                }
            }

            session.setAttribute("loginedUser", user);

            if (user.getIsAdmin() == 1) {
                response.sendRedirect("manage-account");
            } else {
                response.sendRedirect("home");
            }
        }
    }
}
