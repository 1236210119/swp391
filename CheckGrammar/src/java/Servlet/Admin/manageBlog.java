/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Servlet.Admin;

import Until.Account;
import Until.Blog;
import dao.AccountDao;
import dao.BlogDao;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;
import java.util.List;

@WebServlet(name = "manageBlog", urlPatterns = {"/manage-blog"})
public class ManageBlog extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet manageBlog</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet manageBlog at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    private static final long serialVersionUID = 1L;
    private static final int RECORDS_PER_PAGE = 5;

    @Override
   protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    String search = request.getParameter("search");
    int currentPage = request.getParameter("page") != null ? Integer.parseInt(request.getParameter("page")) : 1;

    BlogDao blogDao = new BlogDao();
    List<Blog> list;
    int totalRecords;
    if (search == null || search.isEmpty()) {
        list = blogDao.getBlogsss(currentPage, RECORDS_PER_PAGE);
        totalRecords = blogDao.getTotalRecords();
    } else {
        list = blogDao.searchBlogs(search, currentPage, RECORDS_PER_PAGE);
        totalRecords = blogDao.getTotalRecords(search);
    }
    int endPage = (int) Math.ceil(totalRecords / (double) RECORDS_PER_PAGE);

    request.setAttribute("list", list);
    request.setAttribute("endPage", endPage);
    request.setAttribute("currentPage", currentPage);
    request.setAttribute("search", search);
    request.getRequestDispatcher("/view/Admin/managerBlog.jsp").forward(request, response);
}
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("action");
        String blogID = request.getParameter("blogID");
        String status = request.getParameter("status");
        BlogDao blogDao = new BlogDao();
        System.out.println("Action: " + action);
        if ("delete".equals(action) && blogID != null && !blogID.isEmpty()) {
            blogDao.deleteBlog(Integer.parseInt(blogID));
        } else if (blogID != null && status != null) {
            blogDao.updateBlogStatus(Integer.parseInt(blogID), Integer.parseInt(status));
        } else if ("viewDetails".equals(action) && blogID != null) {
            Blog blogDetail = blogDao.getBlogById(Integer.parseInt(blogID));
            request.setAttribute("blog", blogDetail);
            request.getRequestDispatcher("/view/Admin/blog-detail.jsp").forward(request, response);
            return;
        } else if ("update".equals(action) && blogID != null) {
            Blog blogDetail = blogDao.getBlogById(Integer.parseInt(blogID));
            AccountDao accountDao = new AccountDao();
            List<Account> users = accountDao.getAllUsers();
            request.setAttribute("blog", blogDetail);
            request.setAttribute("users", users);
            request.getRequestDispatcher("/view/Admin/blog-update.jsp").forward(request, response);
            return;
        } else if ("updateBlog".equals(action) && blogID != null) {
            String userID = request.getParameter("userID");
            String createdBy = request.getParameter("createdBy");
            String title = request.getParameter("title");
            String detail = request.getParameter("detail");
            String content = request.getParameter("content");
            Part blogImagePart = request.getPart("blogImage");
            String blogImage = null;
            if (blogImagePart != null && blogImagePart.getSize() > 0) {
                blogImage = blogImagePart.getSubmittedFileName();
                blogImagePart.write(getServletContext().getRealPath("/uploads/") + blogImage);
            }
            Blog blog = new Blog(Integer.parseInt(blogID), userID, createdBy, title, detail, content, blogImage, 0);
            blogDao.updateBlog(blog);
        } else if ("create".equals(action)) {
            AccountDao accountDao = new AccountDao();
            List<Account> users = accountDao.getAllUsers();
            request.setAttribute("users", users);
            request.getRequestDispatcher("/view/Admin/blog-create.jsp").forward(request, response);
            return;
        }

        response.sendRedirect(request.getContextPath() + "/manage-blog");
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
