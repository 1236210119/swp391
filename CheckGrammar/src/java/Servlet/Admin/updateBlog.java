/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Servlet.Admin;

import Until.Account;
import Until.Blog;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import dao.BlogDao;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import jakarta.servlet.http.Part;
import java.io.BufferedReader;

@WebServlet(name = "updateBlog", urlPatterns = {"/api/updateBlog"})
public class UpdateBlog extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        BlogDao blogDao = new BlogDao();
        
        request.setCharacterEncoding("UTF-8");
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");

        String blogImage = null;
        HttpSession session = request.getSession();
        Account user = (Account) session.getAttribute("loginedUser");

        if (user == null) {
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            PrintWriter out = response.getWriter();
            Gson gson = new Gson();
            JsonResponse jsonResponse = new JsonResponse(false, "User not logged in");
            out.print(gson.toJson(jsonResponse));
            out.flush();
            return;
        }
        BufferedReader reader = request.getReader();
        JsonObject jsonObject = JsonParser.parseReader(reader).getAsJsonObject();

        String blogID = jsonObject.get("blogID").getAsString();
       
        String title = jsonObject.get("title").getAsString();
        String detail = jsonObject.get("detail").getAsString();
         String userID = String.valueOf(user.getId());
        String createdBy = String.valueOf(user.getId());
        Blog blog = new Blog(Integer.parseInt(blogID), userID, createdBy, title, detail);
        boolean success = blogDao.updateBlog(blog);

        // Trả về kết quả dưới dạng JSON
        if (success) {
            response.sendRedirect("manage-blog");
        } else {
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            PrintWriter out = response.getWriter();
            Gson gson = new Gson();
            JsonResponse jsonResponse = new JsonResponse(false, "Failed to create blog");
            out.print(gson.toJson(jsonResponse));
            out.flush();
        }
    }

    private class JsonResponse {

        private boolean success;
        private String message;

        public JsonResponse(boolean success, String message) {
            this.success = success;
            this.message = message;
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
