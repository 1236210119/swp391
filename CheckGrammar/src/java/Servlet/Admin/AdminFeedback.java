package Servlet.Admin;

import Until.Feedback;
import dao.FeedbackDAO;
import java.io.IOException;
import java.util.List;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

/**
 * AdminFeedback Servlet to manage feedback operations for admins. Handles
 * displaying all feedback and updating feedback status.
 *
 */
public class AdminFeedback extends HttpServlet {

    private static final int RECORDS_PER_PAGE = 5;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        FeedbackDAO fbDao = new FeedbackDAO();
        int page = 1;
        if (request.getParameter("page") != null) {
            page = Integer.parseInt(request.getParameter("page"));
        }
        int start = (page - 1) * RECORDS_PER_PAGE;
        List<Feedback> feedbacks = fbDao.getFeedbacks(start, RECORDS_PER_PAGE);
        int totalRecords = fbDao.getFeedbackCount();
        int totalPages = (int) Math.ceil((double) totalRecords / RECORDS_PER_PAGE);

        request.setAttribute("feedbacks", feedbacks);
        request.setAttribute("currentPage", page);
        request.setAttribute("totalPages", totalPages);

        request.getRequestDispatcher("/view/Admin/manageFeedback.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int fbId = Integer.parseInt(request.getParameter("fbId"));
        int status = Integer.parseInt(request.getParameter("status"));

        FeedbackDAO fbDao = new FeedbackDAO();
        fbDao.updateFeedbackStatus(fbId, status);

        doGet(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Servlet for admin to manage feedback";
    }
}
