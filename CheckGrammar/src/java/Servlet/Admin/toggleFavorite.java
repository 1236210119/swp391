/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Servlet.Admin;

import Until.Account;
import Until.Blog;
import com.google.gson.Gson;
import dao.BlogDao;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.List;

@WebServlet(name = "toggleFavorite", urlPatterns = {"/toggleFavorite"})
public class ToggleFavorite extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet toggleFavorite</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet toggleFavorite at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        Account user = (Account) session.getAttribute("loginedUser");

        BlogDao blogDao = new BlogDao();

        String action = request.getParameter("action");
        if ("loadMore".equals(action)) {
            int offset = Integer.parseInt(request.getParameter("offset"));
            int limit = Integer.parseInt(request.getParameter("limit"));
            List<Blog> blogss = blogDao.getAllBlogsFavoriteID(user.getId(), offset, limit);
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            PrintWriter out = response.getWriter();
            Gson gson = new Gson();
            out.print(gson.toJson(blogss));
            out.flush();
            return; // Dừng xử lý sau khi gửi phản hồi JSON
        }
        List<Blog> blogss = blogDao.getAllBlogsFavoriteID(user.getId(), 0, 6);
        request.setAttribute("blogas", blogss);
        request.getRequestDispatcher("/view/Admin/favorite.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        Account user = (Account) session.getAttribute("loginedUser");

        System.out.println("vao đây ");

        if (session != null) {

            int blogId = Integer.parseInt(request.getParameter("blogId"));
            boolean isFavorite = Boolean.parseBoolean(request.getParameter("isFavorite"));
            System.out.println(blogId);
            System.out.println(isFavorite);

            BlogDao blogDao = new BlogDao();
            boolean success = blogDao.toggleFavorite(user.getId(), blogId, isFavorite);
            if (success) {
                response.setContentType("application/json");
                response.getWriter().write("{\"status\": \"success\"}");
            } else {
                response.setContentType("application/json");
                response.getWriter().write("{\"status\": \"error\"}");
            }
        } else {
            response.setContentType("application/json");
            response.getWriter().write("{\"status\": \"error\", \"message\": \"User session not found.\"}");
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
