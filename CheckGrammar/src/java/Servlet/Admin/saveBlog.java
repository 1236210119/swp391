/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Servlet.Admin;

import Until.Account;
import Until.Blog;
import com.google.gson.Gson;
import dao.BlogDao;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import jakarta.servlet.http.Part;
import java.io.BufferedReader;
import Until.Account;
import Until.Blog;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;
import dao.BlogDao;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

@WebServlet(name = "saveBlog", urlPatterns = {"/saveBlog"})
@MultipartConfig
public class SaveBlog extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet saveBlog</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet saveBlog at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        BlogDao blogDao = new BlogDao();
        String userID = request.getParameter("userID");
        String createdBy = request.getParameter("createdBy");
        String title = request.getParameter("title");
        String detail = request.getParameter("detail");
        String content = request.getParameter("content");

        String blogImage = null;

        Blog blog = new Blog(userID, createdBy, title, detail, content,0);
        blogDao.createBlog(blog);

        request.getRequestDispatcher("manage-blog").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    private static final long serialVersionUID = 1L;
 
     
     @Override
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        request.setCharacterEncoding("UTF-8");
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");

        HttpSession session = request.getSession();
        Account user = (Account) session.getAttribute("loginedUser");

        if (user == null) {
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            PrintWriter out = response.getWriter();
            Gson gson = new Gson();
            JsonResponse jsonResponse = new JsonResponse(false, "User not logged in");
            out.print(gson.toJson(jsonResponse));
            out.flush();
            return;
        }

        // Process parts from multipart request
        String title = null;
        String detail = null;
        String fileName = null;

        for (Part part : request.getParts()) {
            if (part.getName().equals("title")) {
                title = readPartAsString(part);
            } else if (part.getName().equals("detail")) {
                detail = readPartAsString(part);
            } else if (part.getName().equals("blogImage")) {
                String originalFileName = Paths.get(part.getSubmittedFileName()).getFileName().toString();
                String uniqueFileName = UUID.randomUUID().toString() + "_" + originalFileName;
                String uploadDir = getServletContext().getRealPath("") + File.separator + "uploads";
                Files.createDirectories(Paths.get(uploadDir));

                Path filePath = Paths.get(uploadDir, uniqueFileName);
                try (InputStream fileContent = part.getInputStream()) {
                    Files.copy(fileContent, filePath);
                }
                fileName = uniqueFileName;
            }
        }

        if (title == null || detail == null || fileName == null) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().write("{\"error\":\"Missing parameters\"}");
            return;
        }

        BlogDao blogDao = new BlogDao();
        String userID = String.valueOf(user.getId());
        String createdBy = String.valueOf(user.getId());

        Blog blog = new Blog(userID, createdBy, title, detail, fileName,0);
        boolean success = blogDao.createBlog(blog);

       
        if (success) {
            response.sendRedirect("manage-blog");
        } else {
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            PrintWriter out = response.getWriter();
            Gson gson = new Gson();
            JsonResponse jsonResponse = new JsonResponse(false, "Failed to create blog");
            out.print(gson.toJson(jsonResponse));
            out.flush();
        }
    }

    private String readPartAsString(Part part) throws IOException {
        InputStream inputStream = part.getInputStream();
        StringBuilder stringBuilder = new StringBuilder();
        byte[] buffer = new byte[1024];
        int bytesRead;
        while ((bytesRead = inputStream.read(buffer)) != -1) {
            stringBuilder.append(new String(buffer, 0, bytesRead, "UTF-8"));
        }
        return stringBuilder.toString();
    }

    private class JsonResponse {
        private boolean success;
        private String message;

        public JsonResponse(boolean success, String message) {
            this.success = success;
            this.message = message;
        }
    }
}
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */


