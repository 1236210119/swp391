package Servlet;

import Until.Account;
import Until.Vip;
import dao.AccountDao;
import dao.VipDao;
import java.io.IOException;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.time.LocalDate;

public class PaymentController extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String responeCode = request.getParameter("vnp_ResponseCode");
        String amountString = request.getParameter("vnp_Amount");
        HttpSession session = request.getSession();
        Account accLogin = (Account) session.getAttribute("loginedUser");
        
        if (accLogin == null) {
            response.sendRedirect("login.jsp");
            return;
        }

        if (responeCode == null || responeCode.isEmpty()) {
            request.setAttribute("paymentSuccess", false);
            request.setAttribute("errorMessage", "Response code is missing.");
            RequestDispatcher dispatcher = request.getRequestDispatcher("/view/payment.jsp");
            dispatcher.forward(request, response);
            return;
        }
        if (amountString == null || amountString.isEmpty()) {
            request.setAttribute("paymentSuccess", false);
            request.setAttribute("errorMessage", "Amount is missing.");
            RequestDispatcher dispatcher = request.getRequestDispatcher("/view/payment.jsp");
            dispatcher.forward(request, response);
            return;
        }
        try {
            Account user = (Account) session.getAttribute("loginedUser");
            int amount = Integer.parseInt(amountString);
            int day = 0;
            switch (amount / 2500000) {
                case 36:
                    day = 365;
                    break;
                case 16:
                    day = 90;
                    break;
                case 8:
                    day = 30;
                    break;
                case 3:
                    day = 7;
                    break;
                default:
                    request.setAttribute("paymentSuccess", false);
                    request.setAttribute("errorMessage", "Invalid payment amount: " + amount);
                    RequestDispatcher dispatcher = request.getRequestDispatcher("/view/payment.jsp");
                    dispatcher.forward(request, response);
                    return;
            }
            AccountDao aDAO = new AccountDao();
            VipDao vipDao = new VipDao();
            Vip vip = new Vip();
            vip.setUserID(user.getId());
            vip.setCreateDate(LocalDate.now());
            vip.setDay(day);
            vipDao.insertVip(vip);
            aDAO.updateVip(user.getId(), 1);
            request.setAttribute("paymentSuccess", true);
            RequestDispatcher dispatcher = request.getRequestDispatcher("/view/payment.jsp");
            dispatcher.forward(request, response);
        } catch (Exception e) {
            request.setAttribute("paymentSuccess", false);
            request.setAttribute("errorMessage", "An error occurred during payment processing: " + e.getMessage());
            RequestDispatcher dispatcher = request.getRequestDispatcher("/view/payment.jsp");
            dispatcher.forward(request, response);
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
