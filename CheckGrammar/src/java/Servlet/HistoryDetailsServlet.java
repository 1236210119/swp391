package Servlet;

import Until.History;
import dao.HistoryDAO;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import com.google.gson.Gson;

public class HistoryDetailsServlet extends HttpServlet {
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json;charset=UTF-8");

        try {
            int historyId = Integer.parseInt(request.getParameter("historyId"));
            HistoryDAO historyDAO = new HistoryDAO();
            History history = historyDAO.getHistoryById(historyId);

            if (history != null) {
                String json = new Gson().toJson(history);
                System.out.println("History JSON: " + json); // Debugging log
                response.getWriter().write(json);
            } else {
                System.out.println("No history found for ID: " + historyId); // Debugging log
                response.getWriter().write("{}");
            }
        } catch (Exception e) {
            e.printStackTrace();
            response.getWriter().write("{}");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "HistoryDetailsServlet";
    }
}
