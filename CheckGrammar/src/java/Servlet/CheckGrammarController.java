package Servlet;

import Until.Account;
import Until.History;
import dao.HistoryDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.languagetool.JLanguageTool;
import org.languagetool.Languages;
import org.languagetool.rules.Rule;
import org.languagetool.rules.RuleMatch;
import org.languagetool.rules.spelling.SpellingCheckRule;

public class CheckGrammarController extends HttpServlet {

    private final int NormalWordCheck = 500;
    private final int GuestWordCheck = 50;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            request.getRequestDispatcher("view/checkGrammar.jsp").forward(request, response);
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String inputText = request.getParameter("input-text");
        int wordCount = inputText.split("\\s+").length;

        HttpSession session = request.getSession();
        Account user = (Account) session.getAttribute("loginedUser");
        boolean isCheck = false;
        String message = "";

        if (user != null) {
            if (user.getIsPayfee() == 1) {
                isCheck = true;
            } else {
                if (wordCount <= NormalWordCheck) {
                    isCheck = true;
                } else {
                    message = "Logged in users can check texts up to " + NormalWordCheck + " words.";
                }
            }
        } else {
            if (wordCount <= GuestWordCheck) {
                isCheck = true;
            } else {
                message = "Guests can check texts up to " + GuestWordCheck + " words.";
            }
        }

        if (isCheck) {
            JLanguageTool langTool = new JLanguageTool(Languages.getLanguageForShortCode("en-GB"));

            List<Rule> allRules = langTool.getAllRules();
            for (Rule rule : allRules) {
                if ((rule instanceof SpellingCheckRule)) {
                    langTool.disableRule(rule.getId());
                }
            }

            StringBuilder result = new StringBuilder();
            StringBuilder correctedText = new StringBuilder(inputText);
            StringBuilder highlightedText = new StringBuilder(inputText);

            List<RuleMatch> matches = langTool.check(inputText);
            int offsetCorrected = 0;
            int offsetHighlighted = 0;

            int errorCount = 0;
            for (RuleMatch match : matches) {
                errorCount++;

                String incorrectPart = inputText.substring(match.getFromPos(), match.getToPos());

                result.append("<div>");
                result.append("Error #").append(errorCount).append(": ");
                result.append("Potential error from location ")
                        .append(match.getFromPos()).append(" to ").append(match.getToPos()).append(": ")
                        .append(match.getMessage());
                result.append("</div>");

                result.append("<div>");
                result.append("Wrong word #").append(errorCount).append(": \"");
                result.append("<span style=\"color: red;\">");
                result.append(incorrectPart);
                result.append("</span>");
                result.append("\"");
                result.append("</div>");

                result.append("<div>");
                result.append("Error correction suggestions: ")
                        .append(match.getSuggestedReplacements());
                result.append("</div>");

                result.append("<br>");

                if (!match.getSuggestedReplacements().isEmpty()) {
                    String replacement = match.getSuggestedReplacements().get(0);
                    correctedText.replace(match.getFromPos() + offsetCorrected, match.getToPos() + offsetCorrected, "<span style='color: green;'>" + replacement + "</span>");
                    offsetCorrected += ("<span style='color: green;'>" + replacement + "</span>").length() - incorrectPart.length();
                }

                highlightedText.replace(match.getFromPos() + offsetHighlighted, match.getToPos() + offsetHighlighted, "<span style='color: red;'>" + incorrectPart + "</span>");
                offsetHighlighted += ("<span style='color: red;'>" + incorrectPart + "</span>").length() - incorrectPart.length();
            }

            if (user != null) {
                HistoryDAO histDAO = new HistoryDAO();
                History hist = new History();
                hist.setUserID(user.getId());
                hist.setTextContent(inputText);
                hist.setWrongContent(result.toString());
                hist.setType("Check Grammar");
                hist.setUploadDate(new Date());
                try {
                    histDAO.insertHistory(hist);
                } catch (Exception ex) {
                    Logger.getLogger(CheckGrammarController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            request.setAttribute("input", highlightedText.toString());
            request.setAttribute("error", result.length() > 0 ? result.toString() : "Your text has no grammatical errors.");
            request.setAttribute("corrected", correctedText.toString());
        } else {
            request.setAttribute("error", message);
        }

        doGet(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
