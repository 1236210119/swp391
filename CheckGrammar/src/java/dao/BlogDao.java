/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import Until.Blog;
import context.DBContext;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.sql.Timestamp;

/**
 *
 * @author admin
 */
public class BlogDao extends DBContext {

    public List<Blog> getAll() {
        try {
            List<Blog> blogs = new ArrayList<>();
            String sql = """
                         SELECT [BlogID]
                               ,[CreatedBy]
                               ,[Name]
                               ,[Detail]
                               ,[Image]
                           FROM [CheckGrammar].[dbo].[Blog]""";
            Connection connection = getConnection();
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            AccountDao accountDao = new AccountDao();
            while (rs.next()) {
                Blog blog = new Blog();
                blog.setId(rs.getInt(1));
                blog.setName(rs.getString(3));
                blog.setDetail(rs.getString(4));
                blog.setImage(rs.getString(5));
                blogs.add(blog);
            }
            return blogs;
        } catch (Exception ex) {
            Logger.getLogger(BlogDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Collections.EMPTY_LIST;
    }

    public List<Blog> getBlogsss(int currentPage, int recordsPerPage) {
        List<Blog> list = new ArrayList<>();
        int offset = (currentPage - 1) * recordsPerPage;
        String sql = "SELECT b.*, "
                + "       CASE WHEN f.BlogID IS NOT NULL THEN 1 ELSE 0 END AS favorite "
                + "FROM Blog b "
                + "LEFT JOIN Favorites f ON b.BlogID = f.BlogID AND b.isStatus = 1 "
                + "ORDER BY b.BlogID DESC "
                + "OFFSET ? ROWS "
                + "FETCH NEXT ? ROWS ONLY;";

        try (Connection conn = getConnection(); PreparedStatement ps = conn.prepareStatement(sql)) {
            System.out.println("Executing query: " + sql);
            System.out.println("Offset: " + offset + ", Limit: " + recordsPerPage);

            ps.setInt(1, offset);
            ps.setInt(2, recordsPerPage);

            try (ResultSet rs = ps.executeQuery()) {
                System.out.println("Query executed successfully");
                while (rs.next()) {
                    Blog blog = new Blog();
                    blog.setBlogID(rs.getInt("BlogID"));
                    blog.setUserID(rs.getString("UserID"));
                    blog.setImage(rs.getString("BlogImage"));
                    blog.setCreatedBy(rs.getString("CreatedBy"));
                    blog.setTitle(rs.getString("Title"));
                    blog.setIsStatus(rs.getInt("isStatus"));
                    blog.setFavorite(rs.getBoolean("favorite"));
                    list.add(blog);

                    System.out.println("Fetched blog: " + blog.getTitle());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public List<Blog> getBlogss(int offset, int limit) {
        List<Blog> list = new ArrayList<>();
        String sql = "SELECT b.*, "
                + "       CASE WHEN f.BlogID IS NOT NULL THEN 1 ELSE 0 END AS favorite "
                + "FROM Blog b "
                + "LEFT JOIN Favorites f ON b.BlogID = f.BlogID where b.[isStatus] =1  "
                + "ORDER BY b.BlogID "
                + "OFFSET ? ROWS "
                + "FETCH NEXT ? ROWS ONLY;";

        try (Connection conn = getConnection(); PreparedStatement ps = conn.prepareStatement(sql)) {
            System.out.println("Executing query: " + sql);
            System.out.println("Offset: " + offset + ", Limit: " + limit);

            ps.setInt(1, offset);
            ps.setInt(2, limit);

            try (ResultSet rs = ps.executeQuery()) {
                System.out.println("Query executed successfully");
                while (rs.next()) {
                    Blog blog = new Blog();
                    blog.setBlogID(rs.getInt("BlogID"));
                    blog.setUserID(rs.getString("UserID"));
                    blog.setImage(rs.getString("BlogImage"));
                    blog.setCreatedBy(rs.getString("CreatedBy"));
                    blog.setTitle(rs.getString("Title"));
                    blog.setIsStatus(rs.getInt("isStatus"));
                    blog.setFavorite(rs.getBoolean("favorite"));
                    list.add(blog);

                    System.out.println("Fetched blog: " + blog.getTitle());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public List<Blog> getBlogs(int offset, int limit, int userId) {
        List<Blog> list = new ArrayList<>();
        String sql = "SELECT b.*, "
                + "       CASE WHEN f.BlogID IS NOT NULL THEN 1 ELSE 0 END AS favorite "
                + "FROM Blog b "
                + "LEFT JOIN Favorites f ON b.BlogID = f.BlogID AND f.UserID = ? where b.[isStatus] =1 "
                + // Thêm điều kiện này để kiểm tra UserID
                "ORDER BY b.BlogID DESC "
                + "OFFSET ? ROWS "
                + "FETCH NEXT ? ROWS ONLY;";

        try (Connection conn = getConnection(); PreparedStatement ps = conn.prepareStatement(sql)) {
            System.out.println("Executing query: " + sql);
            System.out.println("Offset: " + offset + ", Limit: " + limit);

            ps.setInt(1, userId);  // Thêm tham số userId
            ps.setInt(2, offset);
            ps.setInt(3, limit);

            try (ResultSet rs = ps.executeQuery()) {
                System.out.println("Query executed successfully");
                while (rs.next()) {
                    Blog blog = new Blog();
                    blog.setBlogID(rs.getInt("BlogID"));
                    blog.setUserID(rs.getString("UserID"));
                    blog.setImage(rs.getString("BlogImage"));
                    blog.setCreatedBy(rs.getString("CreatedBy"));
                    blog.setTitle(rs.getString("Title"));
                    blog.setIsStatus(rs.getInt("isStatus"));
                    blog.setFavorite(rs.getBoolean("favorite"));
                    list.add(blog);

                    System.out.println("Fetched blog: " + blog.getTitle());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public List<Blog> searchBlogs(String search, int currentPage, int recordsPerPage) {
        List<Blog> list = new ArrayList<>();
        String sql = "SELECT * FROM (SELECT ROW_NUMBER() OVER (ORDER BY BlogID) AS RowNum, * FROM Blog WHERE Title LIKE ? OR UserID LIKE ? OR CreatedBy LIKE ?) AS Result WHERE RowNum >= ? AND RowNum <= ?";
        int start = (currentPage - 1) * recordsPerPage + 1;
        int end = currentPage * recordsPerPage;
        try (Connection conn = getConnection(); PreparedStatement ps = conn.prepareStatement(sql)) {
            ps.setString(1, "%" + search + "%");
            ps.setString(2, "%" + search + "%");
            ps.setString(3, "%" + search + "%");
            ps.setInt(4, start);
            ps.setInt(5, end);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Blog blog = new Blog();
                blog.setBlogID(rs.getInt("BlogID"));
                blog.setUserID(rs.getString("UserID"));
                blog.setCreatedBy(rs.getString("CreatedBy"));
                blog.setTitle(rs.getString("Title"));
                blog.setIsStatus(rs.getInt("isStatus"));
                list.add(blog);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public int getTotalRecords() {
        String sql = "SELECT COUNT(*) FROM Blog";
        try (Connection conn = getConnection(); PreparedStatement ps = conn.prepareStatement(sql); ResultSet rs = ps.executeQuery()) {
            if (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public int getTotalRecords(String search) {
        String sql = "SELECT COUNT(*) FROM Blog WHERE Title LIKE ? OR UserID LIKE ? OR CreatedBy LIKE ?";
        try (Connection conn = getConnection(); PreparedStatement ps = conn.prepareStatement(sql)) {
            ps.setString(1, "%" + search + "%");
            ps.setString(2, "%" + search + "%");
            ps.setString(3, "%" + search + "%");
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public void deleteBlog(int blogID) {
        String sql = "DELETE FROM Blog WHERE BlogID = ?";
        try (Connection conn = getConnection(); PreparedStatement ps = conn.prepareStatement(sql)) {
            ps.setInt(1, blogID);
            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Blog getBlogById(int blogID) {
        Blog blog = null;
        String sql = "SELECT a.*,a.CreatedDate ,b.FullName"
                + " FROM Blog a join  Account b on a.UserID = b.UserID "
                + "WHERE a.BlogID = ?";
        try (Connection conn = getConnection(); PreparedStatement ps = conn.prepareStatement(sql)) {
            ps.setInt(1, blogID);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                blog = new Blog();
                blog.setBlogID(rs.getInt("BlogID"));
                blog.setUserID(rs.getString("UserID"));
                blog.setCreatedBy(rs.getString("CreatedBy"));
                blog.setTitle(rs.getString("Title"));
                blog.setDetail(rs.getString("Detail"));
                blog.setBlogContent(rs.getString("BlogContent"));
                blog.setIsStatus(rs.getInt("isStatus"));
                blog.setNameadmin(rs.getString("FullName"));
                blog.setCreate(rs.getDate("CreatedDate"));

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return blog;
    }

    public boolean updateBlog(Blog blog) {
        String sql = "UPDATE Blog SET Title = ?, CreatedBy = ?, Detail = ? ,UserID =?"
                + " WHERE BlogID = ?";
        try (Connection conn = getConnection(); PreparedStatement ps = conn.prepareStatement(sql)) {
            ps.setString(1, blog.getTitle());
            ps.setString(2, blog.getCreatedBy());
            ps.setString(3, blog.getDetail());
            ps.setString(4, blog.getUserID());
            ps.setInt(5, blog.getBlogID());
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    public boolean createBlog(Blog blog) {
        String sql = "INSERT INTO Blog (UserID, CreatedBy, Title, Detail, BlogImage, CreatedDate) "
                + "VALUES (?, ?, ?, ?, ?, ?)";
        try (Connection conn = getConnection(); PreparedStatement ps = conn.prepareStatement(sql)) {
            ps.setString(1, blog.getUserID());
            ps.setString(2, blog.getCreatedBy());
            ps.setString(3, blog.getTitle());
            ps.setString(4, blog.getDetail());
            ps.setString(5, blog.getBlogImage());
            ps.setTimestamp(6, new Timestamp(System.currentTimeMillis())); 
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public void updateBlogStatus(int blogID, int status) {
        String sql = "UPDATE Blog SET isStatus = ?"
                + " WHERE BlogID = ?";
        try (Connection conn = getConnection(); PreparedStatement ps = conn.prepareStatement(sql)) {
            ps.setInt(1, status);
            ps.setInt(2, blogID);
            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Blog> getAllBlogsID(int userid, int offset, int limit) {
        List<Blog> blogs = new ArrayList<>();
        String sql = "SELECT b.*, \n"
                + "       CASE WHEN f.BlogID IS NOT NULL THEN 1 ELSE 0 END AS favorite\n"
                + "FROM blog b \n"
                + "LEFT JOIN Favorites f ON b.BlogID = f.BlogID   \n"
                + "ORDER BY b.BlogID \n"
                + "OFFSET ? ROWS \n"
                + "FETCH NEXT ? ROWS ONLY;";
        try (Connection conn = getConnection(); PreparedStatement ps = conn.prepareStatement(sql)) {
            ps.setInt(1, userid);
            ps.setInt(2, offset);
            ps.setInt(3, limit);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Blog blog = new Blog();
                blog.setId(rs.getInt("BlogID"));
                blog.setTitle(rs.getString("Title"));
                blog.setImage(rs.getString("BlogImage"));
                blog.setFavorite(rs.getBoolean("favorite"));
                blogs.add(blog);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return blogs;
    }

    public List<Blog> getAllBlogs(int offset, int limit) {
        List<Blog> blogs = new ArrayList<>();
        String sql = "SELECT b.*, "
                + "       CASE WHEN f.BlogID IS NOT NULL THEN 1 ELSE 0 END AS favorite "
                + "FROM Blog b "
                + "LEFT JOIN Favorites f ON b.BlogID = f.BlogID "
                + "ORDER BY b.BlogID "
                + "OFFSET ? ROWS "
                + "FETCH NEXT ? ROWS ONLY;";
        try (Connection conn = getConnection(); PreparedStatement ps = conn.prepareStatement(sql)) {
            ps.setInt(1, offset);
            ps.setInt(2, limit);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Blog blog = new Blog();
                blog.setId(rs.getInt("BlogID"));
                blog.setTitle(rs.getString("Title"));
                blog.setImage(rs.getString("BlogImage"));
                blog.setFavorite(rs.getBoolean("favorite"));
                blogs.add(blog);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return blogs;
    }

    public List<Blog> getAllBlogsFavoriteID(int userid, int offset, int limit) {
        List<Blog> blogs = new ArrayList<>();
        String sql = "SELECT b.*, f.FavoriteID,\n"
                + "       CASE WHEN f.BlogID IS NOT NULL THEN 1 ELSE 0 END AS favorite\n"
                + "FROM  Favorites f  \n"
                + "LEFT JOIN blog b  ON b.BlogID = f.BlogID where f.UserID= ? \n"
                + "ORDER BY f.FavoriteID \n"
                + "OFFSET ? ROWS \n"
                + "FETCH NEXT ? ROWS ONLY;";
        try (Connection conn = getConnection(); PreparedStatement ps = conn.prepareStatement(sql)) {
            ps.setInt(1, userid);
            ps.setInt(2, offset);
            ps.setInt(3, limit);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Blog blog = new Blog();
                blog.setId(rs.getInt("BlogID"));
                blog.setTitle(rs.getString("Title"));
                blog.setImage(rs.getString("BlogImage"));
                blog.setFavorite(rs.getBoolean("favorite"));
                blogs.add(blog);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return blogs;
    }

    public boolean toggleFavorite(int userId, int blogId, boolean isFavorite) {
        String sql;
        if (isFavorite) {
            sql = "DELETE FROM Favorites WHERE UserID = ? AND BlogID = ?";
        } else {
            sql = "INSERT INTO Favorites (UserID, BlogID, CreatedDate) VALUES (?, ?, GETDATE())";
        }

        try (Connection conn = getConnection(); PreparedStatement ps = conn.prepareStatement(sql)) {
            ps.setInt(1, userId);
            ps.setInt(2, blogId);
            ps.executeUpdate();
            return true; // Trả về true nếu thành công
        } catch (Exception e) {
            e.printStackTrace();
            return false; // Trả về false nếu có lỗi
        }
    }

    public boolean isFavorite(String blogId, int userId) {
        boolean exists = false;
        String sql = "SELECT COUNT(*) "
                + "FROM favorites"
                + " WHERE BlogID = ? AND UserID = ?";

        try (Connection connection = getConnection(); PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, blogId);
            statement.setInt(2, userId);

            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    exists = resultSet.getInt(1) > 0;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return exists;
    }

    public static void main(String[] args) {
        BlogDao blogDao = new BlogDao();

        // Tạo một blog mới
        Blog newBlog = new Blog();
        newBlog.setUserID("1");
        newBlog.setCreatedBy("Admin");
        newBlog.setTitle("Test Blog Title");
        newBlog.setDetail("This is a test blog detail.");
        newBlog.setBlogImage("image/path.jpg");

        // Thêm blog mới vào database
        boolean isCreated = blogDao.createBlog(newBlog);
        if (isCreated) {
            System.out.println("Blog created successfully!");
        } else {
            System.out.println("Failed to create blog.");
        }

        // Lấy danh sách tất cả các blog để kiểm tra
        List<Blog> blogs = blogDao.getAllBlogs(0, 10);  // Lấy 10 blog đầu tiên
        for (Blog blog : blogs) {
            System.out.println(blog);
        }
    }

}
