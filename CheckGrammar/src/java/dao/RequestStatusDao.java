package dao;

import Until.RequestStatus;
import context.DBContext;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.sql.Timestamp;
import java.sql.Date;

public class RequestStatusDao extends DBContext {

    private int noOfRecords;

    public int getNoOfRecords() {
        return noOfRecords;
    }

    // Create - Tạo một RequestStatus mới
    public boolean createRequestStatus(RequestStatus requestStatus) {
        String sql = "INSERT INTO RequestStatus (UserID, Type, Purpose, CreateDate, ProcessNote, [File], Status, LastUpdate) "
                + "VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
        try (Connection connection = getConnection(); PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setInt(1, requestStatus.getUserID());
            ps.setString(2, requestStatus.getType());
            ps.setString(3, requestStatus.getPurpose());
            ps.setTimestamp(4, requestStatus.getCreateDate());
            ps.setString(5, requestStatus.getProcessNote());
            ps.setString(6, requestStatus.getFile());
            ps.setString(7, requestStatus.getStatus());
            ps.setTimestamp(8, requestStatus.getLastUpdate());

            int rowsAffected = ps.executeUpdate();
            return rowsAffected > 0;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }

    // Read - Lấy RequestStatus bằng ID
    public RequestStatus getRequestStatusByID(int requestID) {
        try {
            String sql = "SELECT [RequestID], [UserID], [Type], [Purpose], [CreateDate], [ProcessNote], [File], [Status], [LastUpdate] "
                    + "FROM RequestStatus WHERE RequestID = ?";
            Connection connection = getConnection();
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, requestID);

            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                RequestStatus rsObj = new RequestStatus();
                rsObj.setRequestID(rs.getInt("RequestID"));
                rsObj.setUserID(rs.getInt("UserID"));
                rsObj.setType(rs.getString("Type"));
                rsObj.setPurpose(rs.getString("Purpose"));
                rsObj.setCreateDate(rs.getTimestamp("CreateDate"));
                rsObj.setProcessNote(rs.getString("ProcessNote"));
                rsObj.setFile(rs.getString("File"));
                rsObj.setStatus(rs.getString("Status"));
                rsObj.setLastUpdate(rs.getTimestamp("LastUpdate"));
                return rsObj;
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public List<RequestStatus> getAllRequestStatusByUserID(int userID) {
        List<RequestStatus> requestStatuses = new ArrayList<>();
        String sql = "SELECT [RequestID], [UserID], [Type], [Purpose], [CreateDate], [ProcessNote], [File], [Status], [LastUpdate] "
                + "FROM RequestStatus WHERE UserID = ?";
        try (Connection connection = getConnection(); PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setInt(1, userID);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                RequestStatus rsObj = new RequestStatus();
                rsObj.setRequestID(rs.getInt("RequestID"));
                rsObj.setUserID(rs.getInt("UserID"));
                rsObj.setType(rs.getString("Type"));
                rsObj.setPurpose(rs.getString("Purpose"));
                rsObj.setCreateDate(rs.getTimestamp("CreateDate"));
                rsObj.setProcessNote(rs.getString("ProcessNote"));
                rsObj.setFile(rs.getString("File"));
                rsObj.setStatus(rs.getString("Status"));
                rsObj.setLastUpdate(rs.getTimestamp("LastUpdate"));
                requestStatuses.add(rsObj);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return requestStatuses;
    }

    // Read - Lấy tất cả RequestStatus
    public List<RequestStatus> getAllRequestStatus() {
        List<RequestStatus> list = new ArrayList<>();
        try {
            String sql = "SELECT [RequestID], [UserID], [Type], [Purpose], [CreateDate], [ProcessNote], [File], [Status], [LastUpdate] FROM RequestStatus";
            Connection connection = getConnection();
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                RequestStatus rsObj = new RequestStatus();
                rsObj.setRequestID(rs.getInt("RequestID"));
                rsObj.setUserID(rs.getInt("UserID"));
                rsObj.setType(rs.getString("Type"));
                rsObj.setPurpose(rs.getString("Purpose"));
                rsObj.setCreateDate(rs.getTimestamp("CreateDate"));
                rsObj.setProcessNote(rs.getString("ProcessNote"));
                rsObj.setFile(rs.getString("File"));
                rsObj.setStatus(rs.getString("Status"));
                rsObj.setLastUpdate(rs.getTimestamp("LastUpdate"));
                list.add(rsObj);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return list;
    }

    public boolean updateRequestStatus(int requestID, String processNote, String status, Timestamp lastUpdate) {
        String sql = "UPDATE RequestStatus SET ProcessNote = ?, Status = ?, LastUpdate = ? WHERE RequestID = ?";
        try (Connection connection = getConnection(); PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setString(1, processNote);
            ps.setString(2, status);
            ps.setTimestamp(3, lastUpdate);
            ps.setInt(4, requestID);

            int rowsAffected = ps.executeUpdate();
            return rowsAffected > 0;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }

    // Delete - Xóa một RequestStatus bằng ID
    public boolean deleteRequestStatus(int requestID) {
        String sql = "DELETE FROM RequestStatus WHERE RequestID = ?";
        try (Connection connection = getConnection(); PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setInt(1, requestID);
            int rowsAffected = ps.executeUpdate();
            return rowsAffected > 0;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }

    public List<RequestStatus> getFilteredRequestStatuses(String status, Date startDate, Date endDate, String purpose, int offset, int noOfRecords) {
        List<RequestStatus> list = new ArrayList<>();
        StringBuilder sql = new StringBuilder("SELECT [RequestID], [UserID], [Type], [Purpose], [CreateDate], [ProcessNote], [File], [Status], [LastUpdate] FROM RequestStatus WHERE 1=1");

        if (status != null && !status.isEmpty()) {
            sql.append(" AND Status = ?");
        }
        if (startDate != null) {
            sql.append(" AND CreateDate >= ?");
        }
        if (endDate != null) {
            sql.append(" AND CreateDate <= ?");
        }
        if (purpose != null && !purpose.isEmpty()) {
            sql.append(" AND Purpose LIKE ?");
        }
        sql.append(" ORDER BY RequestID OFFSET ? ROWS FETCH NEXT ? ROWS ONLY");

        try (Connection connection = getConnection(); PreparedStatement ps = connection.prepareStatement(sql.toString())) {
            int paramIndex = 1;

            if (status != null && !status.isEmpty()) {
                ps.setString(paramIndex++, status);
            }
            if (startDate != null) {
                ps.setDate(paramIndex++, startDate);
            }
            if (endDate != null) {
                ps.setDate(paramIndex++, endDate);
            }
            if (purpose != null && !purpose.isEmpty()) {
                ps.setString(paramIndex++, "%" + purpose + "%");
            }
            ps.setInt(paramIndex++, offset);
            ps.setInt(paramIndex++, noOfRecords);

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                RequestStatus rsObj = new RequestStatus();
                rsObj.setRequestID(rs.getInt("RequestID"));
                rsObj.setUserID(rs.getInt("UserID"));
                rsObj.setType(rs.getString("Type"));
                rsObj.setPurpose(rs.getString("Purpose"));
                rsObj.setCreateDate(rs.getTimestamp("CreateDate"));
                rsObj.setProcessNote(rs.getString("ProcessNote"));
                rsObj.setFile(rs.getString("File"));
                rsObj.setStatus(rs.getString("Status"));
                rsObj.setLastUpdate(rs.getTimestamp("LastUpdate"));
                list.add(rsObj);
            }

            // Lấy tổng số bản ghi
            sql.setLength(0); // Reset SQL string builder
            sql.append("SELECT COUNT(*) FROM RequestStatus WHERE 1=1");
            if (status != null && !status.isEmpty()) {
                sql.append(" AND Status = ?");
            }
            if (startDate != null) {
                sql.append(" AND CreateDate >= ?");
            }
            if (endDate != null) {
                sql.append(" AND CreateDate <= ?");
            }
            if (purpose != null && !purpose.isEmpty()) {
                sql.append(" AND Purpose LIKE ?");
            }
            try (PreparedStatement psCount = connection.prepareStatement(sql.toString())) {
                paramIndex = 1;
                if (status != null && !status.isEmpty()) {
                    psCount.setString(paramIndex++, status);
                }
                if (startDate != null) {
                    psCount.setDate(paramIndex++, startDate);
                }
                if (endDate != null) {
                    psCount.setDate(paramIndex++, endDate);
                }
                if (purpose != null && !purpose.isEmpty()) {
                    psCount.setString(paramIndex++, "%" + purpose + "%");
                }
                ResultSet rsCount = psCount.executeQuery();
                if (rsCount.next()) {
                    this.noOfRecords = rsCount.getInt(1);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return list;
    }

    public int getNumberOfRows(String status, Date startDate, Date endDate, String purpose) {
        StringBuilder sql = new StringBuilder("SELECT COUNT(*) FROM RequestStatus WHERE 1=1");

        if (status != null && !status.isEmpty()) {
            sql.append(" AND Status = ?");
        }
        if (startDate != null) {
            sql.append(" AND CreateDate >= ?");
        }
        if (endDate != null) {
            sql.append(" AND CreateDate <= ?");
        }
        if (purpose != null && !purpose.isEmpty()) {
            sql.append(" AND Purpose LIKE ?");
        }

        try (Connection connection = getConnection(); PreparedStatement ps = connection.prepareStatement(sql.toString())) {
            int paramIndex = 1;

            if (status != null && !status.isEmpty()) {
                ps.setString(paramIndex++, status);
            }
            if (startDate != null) {
                ps.setDate(paramIndex++, startDate);
            }
            if (endDate != null) {
                ps.setDate(paramIndex++, endDate);
            }
            if (purpose != null && !purpose.isEmpty()) {
                ps.setString(paramIndex++, "%" + purpose + "%");
            }

            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return 0;
    }

    public List<RequestStatus> getFilteredRequestStatusesByUserID(int userID, String status, Date startDate, Date endDate, String purpose, int offset, int noOfRecords) {
        List<RequestStatus> list = new ArrayList<>();
        StringBuilder sql = new StringBuilder("SELECT [RequestID], [UserID], [Type], [Purpose], [CreateDate], [ProcessNote], [File], [Status], [LastUpdate] FROM RequestStatus WHERE UserID = ?");

        if (status != null && !status.isEmpty()) {
            sql.append(" AND Status = ?");
        }
        if (startDate != null) {
            sql.append(" AND CreateDate >= ?");
        }
        if (endDate != null) {
            sql.append(" AND CreateDate <= ?");
        }
        if (purpose != null && !purpose.isEmpty()) {
            sql.append(" AND Purpose LIKE ?");
        }
        sql.append(" ORDER BY RequestID OFFSET ? ROWS FETCH NEXT ? ROWS ONLY");

        try (Connection connection = getConnection(); PreparedStatement ps = connection.prepareStatement(sql.toString())) {
            int paramIndex = 1;
            ps.setInt(paramIndex++, userID);

            if (status != null && !status.isEmpty()) {
                ps.setString(paramIndex++, status);
            }
            if (startDate != null) {
                ps.setDate(paramIndex++, startDate);
            }
            if (endDate != null) {
                ps.setDate(paramIndex++, endDate);
            }
            if (purpose != null && !purpose.isEmpty()) {
                ps.setString(paramIndex++, "%" + purpose + "%");
            }
            ps.setInt(paramIndex++, offset);
            ps.setInt(paramIndex++, noOfRecords);

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                RequestStatus rsObj = new RequestStatus();
                rsObj.setRequestID(rs.getInt("RequestID"));
                rsObj.setUserID(rs.getInt("UserID"));
                rsObj.setType(rs.getString("Type"));
                rsObj.setPurpose(rs.getString("Purpose"));
                rsObj.setCreateDate(rs.getTimestamp("CreateDate"));
                rsObj.setProcessNote(rs.getString("ProcessNote"));
                rsObj.setFile(rs.getString("File"));
                rsObj.setStatus(rs.getString("Status"));
                rsObj.setLastUpdate(rs.getTimestamp("LastUpdate"));
                list.add(rsObj);
            }

            // Lấy tổng số bản ghi
            sql.setLength(0); // Reset SQL string builder
            sql.append("SELECT COUNT(*) FROM RequestStatus WHERE UserID = ?");
            if (status != null && !status.isEmpty()) {
                sql.append(" AND Status = ?");
            }
            if (startDate != null) {
                sql.append(" AND CreateDate >= ?");
            }
            if (endDate != null) {
                sql.append(" AND CreateDate <= ?");
            }
            if (purpose != null && !purpose.isEmpty()) {
                sql.append(" AND Purpose LIKE ?");
            }
            try (PreparedStatement psCount = connection.prepareStatement(sql.toString())) {
                paramIndex = 1;
                psCount.setInt(paramIndex++, userID);
                if (status != null && !status.isEmpty()) {
                    psCount.setString(paramIndex++, status);
                }
                if (startDate != null) {
                    psCount.setDate(paramIndex++, startDate);
                }
                if (endDate != null) {
                    psCount.setDate(paramIndex++, endDate);
                }
                if (purpose != null && !purpose.isEmpty()) {
                    psCount.setString(paramIndex++, "%" + purpose + "%");
                }
                ResultSet rsCount = psCount.executeQuery();
                if (rsCount.next()) {
                    this.noOfRecords = rsCount.getInt(1);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return list;
    }

    public int getNumberOfRowsByUserID(int userID, String status, Date startDate, Date endDate, String purpose) {
        StringBuilder sql = new StringBuilder("SELECT COUNT(*) FROM RequestStatus WHERE UserID = ?");

        if (status != null && !status.isEmpty()) {
            sql.append(" AND Status = ?");
        }
        if (startDate != null) {
            sql.append(" AND CreateDate >= ?");
        }
        if (endDate != null) {
            sql.append(" AND CreateDate <= ?");
        }
        if (purpose != null && !purpose.isEmpty()) {
            sql.append(" AND Purpose LIKE ?");
        }

        try (Connection connection = getConnection(); PreparedStatement ps = connection.prepareStatement(sql.toString())) {
            int paramIndex = 1;
            ps.setInt(paramIndex++, userID);

            if (status != null && !status.isEmpty()) {
                ps.setString(paramIndex++, status);
            }
            if (startDate != null) {
                ps.setDate(paramIndex++, startDate);
            }
            if (endDate != null) {
                ps.setDate(paramIndex++, endDate);
            }
            if (purpose != null && !purpose.isEmpty()) {
                ps.setString(paramIndex++, "%" + purpose + "%");
            }

            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return 0;
    }

    public static void main(String[] args) {
        // Tạo một đối tượng RequestStatus mẫu
        RequestStatus requestStatus = new RequestStatus();
        requestStatus.setUserID(18);
        requestStatus.setType("SampleType");
        requestStatus.setPurpose("SamplePurpose");
        requestStatus.setCreateDate(new Timestamp(System.currentTimeMillis()));
        requestStatus.setProcessNote("");
        requestStatus.setFile("SampleFile.txt");
        requestStatus.setStatus("Waiting for response");
        requestStatus.setLastUpdate(new Timestamp(System.currentTimeMillis()));

        // Tạo đối tượng của lớp chứa phương thức createRequestStatus
        RequestStatusDao dao = new RequestStatusDao();

        // Gọi phương thức createRequestStatus và kiểm tra kết quả
        boolean isCreated = dao.createRequestStatus(requestStatus);

        // In ra kết quả kiểm tra
        if (isCreated) {
            System.out.println("RequestStatus đã được tạo thành công.");
        } else {
            System.out.println("Tạo RequestStatus thất bại.");
        }
    }
}
