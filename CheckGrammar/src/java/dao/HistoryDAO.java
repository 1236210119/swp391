package dao;

import Until.History;
import context.DBContext;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class HistoryDAO extends DBContext {

    private int noOfRecords;
    
    public List<History> getHistoriesByUser(int userID, int offset, int noOfRecords) throws Exception {
        List<History> histories = new ArrayList<>();
        String sql = "SELECT * FROM History WHERE UserID = ? ORDER BY UploadDate OFFSET ? ROWS FETCH NEXT ? ROWS ONLY";
        try (Connection conn = getConnection(); PreparedStatement ps = conn.prepareStatement(sql)) {
            ps.setInt(1, userID);
            ps.setInt(2, offset);
            ps.setInt(3, noOfRecords);
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    histories.add(mapRowToHistory(rs));
                }
            }
            // Sử dụng COUNT để lấy tổng số bản ghi
            try (PreparedStatement countStmt = conn.prepareStatement("SELECT COUNT(*) FROM History WHERE UserID = ?")) {
                countStmt.setInt(1, userID);
                try (ResultSet countRs = countStmt.executeQuery()) {
                    if (countRs.next()) {
                        this.noOfRecords = countRs.getInt(1);
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return histories;
    }

    public List<History> searchHistories(int userID, String searchText, String startDate, String endDate, String searchType, int offset, int noOfRecords) {
        List<History> histories = new ArrayList<>();
        StringBuilder query = new StringBuilder("SELECT * FROM History WHERE UserID = ?");

        if (searchText != null && !searchText.trim().isEmpty()) {
            query.append(" AND TextContent LIKE ?");
        }
        if (startDate != null && !startDate.trim().isEmpty()) {
            query.append(" AND UploadDate >= ?");
        }
        if (endDate != null && !endDate.trim().isEmpty()) {
            query.append(" AND UploadDate <= ?");
        }
        if (searchType != null && !searchType.trim().isEmpty()) {
            query.append(" AND type = ?");
        }

        query.append(" ORDER BY UploadDate OFFSET ? ROWS FETCH NEXT ? ROWS ONLY");

        try (Connection conn = getConnection(); PreparedStatement ps = conn.prepareStatement(query.toString())) {

            int paramIndex = 1;
            ps.setInt(paramIndex++, userID);

            if (searchText != null && !searchText.trim().isEmpty()) {
                ps.setString(paramIndex++, "%" + searchText + "%");
            }
            if (startDate != null && !startDate.trim().isEmpty()) {
                ps.setString(paramIndex++, startDate);
            }
            if (endDate != null && !endDate.trim().isEmpty()) {
                ps.setString(paramIndex++, endDate);
            }
            if (searchType != null && !searchType.trim().isEmpty()) {
                ps.setString(paramIndex++, searchType);
            }
            ps.setInt(paramIndex++, offset);
            ps.setInt(paramIndex, noOfRecords);

            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    histories.add(mapRowToHistory(rs));
                }
            }

            // Get the total number of records
            String countQuery = "SELECT COUNT(*) FROM History WHERE UserID = ?";
            if (searchText != null && !searchText.trim().isEmpty()) {
                countQuery += " AND TextContent LIKE ?";
            }
            if (startDate != null && !startDate.trim().isEmpty()) {
                countQuery += " AND UploadDate >= ?";
            }
            if (endDate != null && !endDate.trim().isEmpty()) {
                countQuery += " AND UploadDate <= ?";
            }
            if (searchType != null && !searchType.trim().isEmpty()) {
                countQuery += " AND type = ?";
            }

            try (PreparedStatement countStmt = conn.prepareStatement(countQuery)) {
                paramIndex = 1;
                countStmt.setInt(paramIndex++, userID);

                if (searchText != null && !searchText.trim().isEmpty()) {
                    countStmt.setString(paramIndex++, "%" + searchText + "%");
                }
                if (startDate != null && !startDate.trim().isEmpty()) {
                    countStmt.setString(paramIndex++, startDate);
                }
                if (endDate != null && !endDate.trim().isEmpty()) {
                    countStmt.setString(paramIndex++, endDate);
                }
                if (searchType != null && !searchType.trim().isEmpty()) {
                    countStmt.setString(paramIndex, searchType);
                }

                try (ResultSet countRs = countStmt.executeQuery()) {
                    if (countRs.next()) {
                        this.noOfRecords = countRs.getInt(1);
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return histories;
    }

    public int getNoOfRecords() {
        return noOfRecords;
    }

    private History mapRowToHistory(ResultSet rs) throws SQLException {
        History history = new History();
        history.setHistoryID(rs.getInt("HistoryID"));
        history.setUserID(rs.getInt("UserID"));
        history.setTextContent(rs.getString("TextContent"));
        history.setWrongContent(rs.getString("WrongContent"));
        history.setType(rs.getString("Type"));
        history.setUploadDate(rs.getDate("UploadDate"));
        return history;
    }

    public boolean insertHistory(History history) throws Exception {
        String query = "INSERT INTO History (UserID, TextContent, WrongContent, Type, UploadDate) "
                + "VALUES (?, ?, ?, ?, ?)";

        try (Connection con = getConnection(); PreparedStatement ps = con.prepareStatement(query)) {

            ps.setInt(1, history.getUserID());
            ps.setString(2, history.getTextContent());
            ps.setString(3, history.getWrongContent());
            ps.setString(4, history.getType());
            ps.setDate(5, new java.sql.Date(history.getUploadDate().getTime())); // Assuming UploadDate is a java.util.Date

            int rowsInserted = ps.executeUpdate();

            return rowsInserted > 0;

        } catch (SQLException e) {
            e.printStackTrace();
            return false; // Insert failed due to exception
        }
    }

    public History getHistoryById(int historyId) throws Exception {
        History history = null;
        String sql = "SELECT * FROM History WHERE HistoryID = ?";

        try (Connection conn = getConnection(); PreparedStatement ps = conn.prepareStatement(sql)) {
            ps.setInt(1, historyId);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    history = new History();
                    history.setHistoryID(rs.getInt("HistoryID"));
                    history.setUserID(rs.getInt("UserID"));
                    history.setTextContent(rs.getString("TextContent"));
                    history.setWrongContent(rs.getString("WrongContent"));
                    history.setType(rs.getString("Type"));
                    history.setUploadDate(rs.getDate("UploadDate"));
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return history;
    }

    public static void main(String[] args) {
        HistoryDAO historyDAO = new HistoryDAO();

        // Test history ID
        int testHistoryId = 14; // Replace with an actual history ID from your database

        try {
            // Fetch history by ID
            History history = historyDAO.getHistoryById(testHistoryId);

            // Check if history is found and print details
            if (history != null) {
                System.out.println("History found:");
                System.out.println("History ID: " + history.getHistoryID());
                System.out.println("User ID: " + history.getUserID());
                System.out.println("Text Content: " + history.getTextContent());
                System.out.println("Wrong Content: " + history.getWrongContent());
                System.out.println("Type: " + history.getType());
                System.out.println("Upload Date: " + history.getUploadDate());
            } else {
                System.out.println("No history found with ID: " + testHistoryId);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
