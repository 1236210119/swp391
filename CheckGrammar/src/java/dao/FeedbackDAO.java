/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import Until.Feedback;
import context.DBContext;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.sql.ResultSet;
import java.util.List;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
/**
 *
 * @author hung
 */
public class FeedbackDAO extends DBContext {
public boolean canSubmitFeedback(int userId, Date currentDate) {
        String query = "SELECT COUNT(*) FROM [dbo].[Feedback] WHERE [UserID] = ? AND [FeedbackDate] = ?";
        try (Connection conn = getConnection(); 
             PreparedStatement ps = conn.prepareStatement(query)) {
            ps.setInt(1, userId);
            ps.setDate(2, currentDate);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getInt(1) == 0
                        ;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
    public void CreatFeedback(Feedback feedback) {
        String query = "INSERT INTO [dbo].[Feedback]\n"
                + "           ([UserID]\n"
                + "           ,[Feedback]\n"
                + "           ,[FeedbackDate]\n"
                + "           ,[Status])\n"
                + "     VALUES\n"
                + "           (?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?)";

        try {
            Connection conn = getConnection(); //mở kết nối sql server
            PreparedStatement ps = conn.prepareStatement(query);
            ps.setInt(1, feedback.getUserId());
            ps.setString(2, feedback.getFeedback());
            ps.setDate(3, feedback.getFeedbackDate());
            ps.setInt(4, feedback.getStatus());
            ps.executeUpdate();

        } catch (Exception e) {

            e.printStackTrace();
        }
    }

    public ArrayList<Feedback> getPaginatedFeedbacks(int start, int total) {
    ArrayList<Feedback> feedbacks = new ArrayList<>();
    String query = "SELECT [FeedbackID], f.[UserID], [Feedback], [FeedbackDate], [Status], a.[FullName] " +
                   "FROM [Feedback] f JOIN [Account] a ON f.UserID = a.UserID " +
                   "ORDER BY FeedbackDate DESC, FeedbackID ASC " +
                   "OFFSET ? ROWS FETCH NEXT ? ROWS ONLY";
    try {
        Connection conn = getConnection();
        PreparedStatement ps = conn.prepareStatement(query);
        ps.setInt(1, start);
        ps.setInt(2, total);
        ResultSet rs = ps.executeQuery();
        Feedback fb;
        while (rs.next()) {
            fb = new Feedback();
            fb.setId(rs.getInt(1));
            fb.setUserId(rs.getInt(2));
            fb.setFeedback(rs.getString(3));
            fb.setFeedbackDate(rs.getDate(4));
            fb.setStatus(rs.getInt(5));
            fb.setName(rs.getString(6));
            feedbacks.add(fb);
        }
    } catch (Exception e) {
        e.printStackTrace();
    }
    return feedbacks;
}

public int getTotalFeedbackCount() {
    int count = 0;
    String query = "SELECT COUNT(*) FROM [Feedback]";
    try {
        Connection conn = getConnection();
        PreparedStatement ps = conn.prepareStatement(query);
        ResultSet rs = ps.executeQuery();
        if (rs.next()) {
            count = rs.getInt(1);
        }
    } catch (Exception e) {
        e.printStackTrace();
    }
    return count;
}


    public void updateFeedbackStatus(int fbId, int status) {
        String query = "UPDATE [Feedback]\n"
                + "   SET [Status] = ?\n"
                + " WHERE FeedBackID = ?\n";
        try {
            Connection conn = getConnection();
            PreparedStatement ps = conn.prepareStatement(query);
            ps.setInt(1, status);
            ps.setInt(2, fbId);
            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Feedback> getFeedbacks(int start, int total) {
        List<Feedback> list = new ArrayList<>();
        String query = "SELECT * FROM [Feedback] ORDER BY FeedbackDate DESC, FeedbackID ASC OFFSET ? ROWS FETCH NEXT ? ROWS ONLY";

        try (Connection con = getConnection(); PreparedStatement ps = con.prepareStatement(query)) {
            ps.setInt(1, start);
            ps.setInt(2, total);
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    Feedback fb = new Feedback();
                    fb.setId(rs.getInt("FeedbackID"));
                    fb.setFeedback(rs.getString("Feedback"));
                    fb.setFeedbackDate(rs.getDate("FeedbackDate"));
                    fb.setUserId(rs.getInt("UserID"));
                    fb.setStatus(rs.getInt("Status"));
                    list.add(fb);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public int getFeedbackCount() {
        int count = 0;
        String query = "SELECT COUNT(*) FROM [Feedback]";

        try (Connection con = getConnection(); PreparedStatement ps = con.prepareStatement(query); ResultSet rs = ps.executeQuery()) {
            if (rs.next()) {
                count = rs.getInt(1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return count;
    }    
}
