package dao;

import Until.Vip;
import context.DBContext;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

public class VipDao extends DBContext {

    public List<Vip> getAllVip() {
        List<Vip> vipList = new ArrayList<>();
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            conn = getConnection();
            String sql = "SELECT * FROM Vip";
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Vip vip = new Vip();
                vip.setVipID(rs.getInt("VipID"));
                vip.setUserID(rs.getInt("UserID"));
                vip.setDay(rs.getInt("Day"));
                vip.setCreateDate(rs.getDate("CreateDate").toLocalDate());  // Chuyển đổi sang LocalDate
                vipList.add(vip);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } 
        return vipList;
    }

    public Vip getVipByID(int vipID) {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            conn = getConnection();
            String sql = "SELECT * FROM Vip WHERE VipID = ?";
            ps = conn.prepareStatement(sql);
            ps.setInt(1, vipID);
            rs = ps.executeQuery();
            if (rs.next()) {
                Vip vip = new Vip();
                vip.setVipID(rs.getInt("VipID"));
                vip.setUserID(rs.getInt("UserID"));
                vip.setDay(rs.getInt("Day"));
                vip.setCreateDate(rs.getDate("CreateDate").toLocalDate());  // Chuyển đổi sang LocalDate
                return vip;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } 
        return null;
    }

    public Vip getVipByUserID(int userID) {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            conn = getConnection();
            String sql = "SELECT * FROM Vip WHERE UserID = ?";
            ps = conn.prepareStatement(sql);
            ps.setInt(1, userID);
            rs = ps.executeQuery();
            if (rs.next()) {
                Vip vip = new Vip();
                vip.setVipID(rs.getInt("VipID"));
                vip.setUserID(rs.getInt("UserID"));
                vip.setDay(rs.getInt("Day"));
                vip.setCreateDate(rs.getDate("CreateDate").toLocalDate());  // Chuyển đổi sang LocalDate
                return vip;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } 
        return null;
    }
    
    public boolean insertVip(Vip vip) {
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            conn = getConnection();
            String sql = "INSERT INTO Vip (UserID, Day, CreateDate) VALUES (?, ?, ?)";
            ps = conn.prepareStatement(sql);
            ps.setInt(1, vip.getUserID());
            ps.setInt(2, vip.getDay());
            ps.setDate(3, java.sql.Date.valueOf(vip.getCreateDate())); 
            int rowsAffected = ps.executeUpdate();
            return rowsAffected > 0;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        } 
    }

    public boolean updateVip(Vip vip) {
        Connection conn = null;
        PreparedStatement ps = null;
        try {
             conn = getConnection();
            String sql = "UPDATE Vip SET UserID = ?, Day = ?, CreateDate = ? WHERE VipID = ?";
            ps = conn.prepareStatement(sql);
            ps.setInt(1, vip.getUserID());
            ps.setInt(2, vip.getDay());
            ps.setDate(3, java.sql.Date.valueOf(vip.getCreateDate()));  // Chuyển đổi LocalDate sang java.sql.Date
            ps.setInt(4, vip.getVipID());
            int rowsAffected = ps.executeUpdate();
            return rowsAffected > 0;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        } 
    }

    public boolean deleteVip(int vipID) {
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            conn = getConnection();
            String sql = "DELETE FROM Vip WHERE VipID = ?";
            ps = conn.prepareStatement(sql);
            ps.setInt(1, vipID);
            int rowsAffected = ps.executeUpdate();
            return rowsAffected > 0;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        } 
    }
}
