package dao;

import Until.Account;
import context.DBContext;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Admin
 */
public class AccountDao extends DBContext {

    public boolean updateAccountStatus(int userId, int newStatus) {
        try {
            String sql = "UPDATE [CheckGrammar].[dbo].[Account] SET [isStatus] = ? WHERE [UserID] = ?";
            Connection connection = getConnection();
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, newStatus);
            ps.setInt(2, userId);

            int rowsAffected = ps.executeUpdate();
            return rowsAffected > 0;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }

    public List<Account> getAllUsers() {
        List<Account> list = new ArrayList<>();
        String sql = "SELECT UserID, FullName FROM Account";
        try (Connection conn = getConnection(); PreparedStatement ps = conn.prepareStatement(sql)) {
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Account user = new Account();
                user.setId(rs.getInt("UserID"));
                user.setFullname(rs.getString("FullName"));
                list.add(user);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public Account getAccountByID(int id) {
        try {
            String sql = "SELECT [UserID], [Username], [Password], [Email], [Address], [FullName], [Phone], [isPayfee], [isAdmin], [Avatar], [isStatus] "
                    + "FROM [CheckGrammar].[dbo].[Account] WHERE UserID = ?";
            Connection connection = getConnection();
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, id);

            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                Account acc = new Account();
                acc.setId(rs.getInt("UserID"));
                acc.setUsername(rs.getString("Username"));
                acc.setPassword(rs.getString("Password"));
                acc.setEmail(rs.getString("Email"));
                acc.setAddress(rs.getString("Address"));
                acc.setFullname(rs.getString("FullName"));
                acc.setPhone(rs.getString("Phone"));
                acc.setIsPayfee(rs.getInt("isPayfee"));
                acc.setIsAdmin(rs.getInt("isAdmin"));
                acc.setAvatar(rs.getString("Avatar"));
                acc.setIsStatus(rs.getInt("isStatus"));
                return acc;
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    Connection conn = null;
    PreparedStatement ps = null;
    ResultSet rs = null;

    public boolean registerAccount(Account account) {
        String sql = "INSERT INTO Account (Username, Password, Email, Address, FullName, Phone, isPayfee, isAdmin, Avatar, isStatus, VerificationCode, VerificationCodeExpiry) "
                + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        try (Connection connection = getConnection(); PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setString(1, account.getUsername());
            ps.setString(2, account.getPassword());
            ps.setString(3, account.getEmail());
            ps.setString(4, account.getAddress());
            ps.setString(5, account.getFullname());
            ps.setString(6, account.getPhone());
            ps.setInt(7, account.getIsPayfee());
            ps.setInt(8, account.getIsAdmin());
            ps.setString(9, account.getAvatar());
            ps.setInt(10, account.getIsStatus());
            ps.setString(11, account.getVerificationCode());
            ps.setTimestamp(12, account.getVerificationCodeExpiry());

            int rowsAffected = ps.executeUpdate();
            return rowsAffected > 0;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }

    public boolean verifyAccount(String email) throws Exception {
        String sql = "UPDATE Account SET isStatus = 1 WHERE Email = ?";
        try (Connection connection = getConnection(); PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setString(1, email);
            int rowsAffected = ps.executeUpdate();
            return rowsAffected > 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public Account login(String username, String password) throws Exception {
        String query = "SELECT * FROM Account WHERE Username = ? AND Password = ?";
        try (Connection connection = getConnection(); PreparedStatement ps = connection.prepareStatement(query)) {

            ps.setString(1, username);
            ps.setString(2, password);

            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                Account acc = new Account();
                acc.setId(rs.getInt("UserID"));
                acc.setUsername(rs.getString("Username"));
                acc.setPassword(rs.getString("Password"));
                acc.setEmail(rs.getString("Email"));
                acc.setAddress(rs.getString("Address"));
                acc.setFullname(rs.getString("FullName"));
                acc.setPhone(rs.getString("Phone"));
                acc.setIsPayfee(rs.getInt("isPayfee"));
                acc.setIsAdmin(rs.getInt("isAdmin"));
                acc.setAvatar(rs.getString("Avatar"));
                acc.setIsStatus(rs.getInt("isStatus"));
                return acc;
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public void signUp(String user, String pass, String email) {
        String query = """
                INSERT INTO [dbo].[Account]
                           ([Username]
                           ,[Password]
                           ,[Email]
                           ,[isPayfee])
                     VALUES
                           (?
                           ,?
                           ,?
                           ,0)""";

        try {

            conn = new DBContext().getConnection(); // mở kết nối sql server
            ps = conn.prepareStatement(query);
            ps.setString(1, user);
            ps.setString(2, pass);
            ps.setString(3, email);
            ps.executeUpdate();

        } catch (Exception e) {

            e.printStackTrace();
        }
    }

    public String getUsernameById(String userId) {

        String result = "";
        String query = "select userName from Account where id = ?";
        try {

            conn = new DBContext().getConnection(); // mở kết nối sql server
            ps = conn.prepareStatement(query);
            ps.setString(1, userId);
            rs = ps.executeQuery();

            while (rs.next()) {

                result = rs.getString(1);
            }
        } catch (Exception e) {

            e.printStackTrace();
        }

        return result;
    }

    public void resetRole() {
        String query = "update Account set isPayfee = 0, isAdmin = 0";
        try {

            conn = new DBContext().getConnection(); // mở kết nối sql server
            ps = conn.prepareStatement(query);
            ps.executeUpdate();
        } catch (Exception e) {

            e.printStackTrace();
        }
    }

    public void UpdateAccountProfile(Account account) {
        String query = "UPDATE [dbo].[Account] "
                + "SET [Address] = ?, [FullName] =?, [Phone] = ?, [Avatar] = ? "
                + "WHERE UserID = ?";
        try {
            conn = new DBContext().getConnection(); // mở kết nối sql server
            ps = conn.prepareStatement(query);

            ps.setString(1, account.getAddress());
            ps.setString(2, account.getFullname());
            ps.setString(3, account.getPhone());
            ps.setString(4, account.getAvatar());
            ps.setInt(5, account.getId());

            ps.executeUpdate();
        } catch (Exception e) {

            e.printStackTrace();
        }
    }

    public Account getAccountByUsername(String username) {
        try {
            String sql = """
                    SELECT [UserID], [Username], [Password], [Email], [Address], [FullName], [Phone], [isPayfee], [isAdmin], [Avatar], [isStatus]
                    FROM [CheckGrammar].[dbo].[Account] WHERE Username = ?""";
            Connection connection = getConnection();
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, username);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Account acc = new Account();
                acc.setId(rs.getInt("UserID"));
                acc.setUsername(rs.getString("Username"));
                acc.setPassword(rs.getString("Password"));
                acc.setEmail(rs.getString("Email"));
                acc.setAddress(rs.getString("Address"));
                acc.setFullname(rs.getString("FullName"));
                acc.setPhone(rs.getString("Phone"));
                acc.setIsPayfee(rs.getInt("isPayfee"));
                acc.setIsAdmin(rs.getInt("isAdmin"));
                acc.setAvatar(rs.getString("Avatar"));
                acc.setIsStatus(rs.getInt("isStatus"));
                return acc;
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public Account getAccountByEmail(String email) throws Exception {
        try {
            String sql = """
                SELECT [UserID], [Username], [Password], [Email], [Address], [FullName], [Phone], [isPayfee], [isAdmin], [Avatar], [isStatus]
                FROM [CheckGrammar].[dbo].[Account] WHERE Email = ?""";
            Connection connection = getConnection();
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, email);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                Account acc = new Account();
                acc.setId(rs.getInt("UserID"));
                acc.setUsername(rs.getString("Username"));
                acc.setPassword(rs.getString("Password"));
                acc.setEmail(rs.getString("Email"));
                acc.setAddress(rs.getString("Address"));
                acc.setFullname(rs.getString("FullName"));
                acc.setPhone(rs.getString("Phone"));
                acc.setIsPayfee(rs.getInt("isPayfee"));
                acc.setIsAdmin(rs.getInt("isAdmin"));
                acc.setAvatar(rs.getString("Avatar"));
                acc.setIsStatus(rs.getInt("isStatus"));
                return acc;
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public void insertUser(Account account) {
        String sql = "INSERT INTO Account (Username, Password, Email, Address, FullName, Phone, isPayfee, isAdmin, avatar, isStatus) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        try (Connection connection = getConnection(); PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setString(1, account.getUsername());
            ps.setString(2, account.getPassword());
            ps.setString(3, account.getEmail());
            ps.setString(4, account.getAddress());
            ps.setString(5, account.getFullname());
            ps.setString(6, account.getPhone());
            ps.setInt(7, account.getIsPayfee());
            ps.setInt(8, account.getIsAdmin());
            ps.setString(9, account.getAvatar());
            ps.setInt(10, account.getIsStatus());

            ps.executeUpdate();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public boolean isExistEmail(String email) {
        String query = "SELECT * FROM Account WHERE Email = ?";
        try (Connection conn = getConnection(); PreparedStatement ps = conn.prepareStatement(query)) {
            ps.setString(1, email);
            rs = ps.executeQuery();
            return rs.next();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean isExistUsername(String username) {
        //
        String result = "";
        String query = "select * from Account where userName = ?";
        try {

            conn = new DBContext().getConnection(); // mở kết nối sql server
            ps = conn.prepareStatement(query);
            ps.setString(1, username);
            rs = ps.executeQuery();

            while (rs.next()) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean updatePassword(int userID, String newPassword) throws Exception {
        String sql = "UPDATE Account SET Password = ? WHERE UserID = ?";
        try (Connection connection = getConnection(); PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setString(1, newPassword);
            ps.setInt(2, userID);
            int rowsAffected = ps.executeUpdate();
            System.out.println("Rows affected: " + rowsAffected);
            return rowsAffected > 0;
        } catch (SQLException ex) {
            System.err.println("Error updating password: " + ex.getMessage());
            ex.printStackTrace();
        }
        return false;
    }

    public boolean updateVip(int userID, int isPayfee) {
        String sql = "UPDATE Account SET isPayfee = ? WHERE UserID = ?";
        try (Connection connection = getConnection(); PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setInt(1, isPayfee);
            ps.setInt(2, userID);
            int rowsAffected = ps.executeUpdate();
            return rowsAffected > 0;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }

    public int getTotalAccounts(String search, String isPayFee, String isStatus) {
        StringBuilder sql = new StringBuilder("SELECT COUNT(*) FROM Account WHERE 1=1 ");
        if (search != null && !search.isEmpty()) {
            sql.append(" AND (Email LIKE ? OR FullName LIKE ?)");
        }
        if (isPayFee != null && !isPayFee.isEmpty()) {
            sql.append(" AND isPayfee = ?");
        }
        if (isStatus != null && !isStatus.isEmpty()) {
            sql.append(" AND isStatus = ?");
        }

        try (Connection conn = getConnection(); PreparedStatement ps = conn.prepareStatement(sql.toString())) {
            int paramIndex = 1;
            if (search != null && !search.isEmpty()) {
                ps.setString(paramIndex++, "%" + search + "%");
                ps.setString(paramIndex++, "%" + search + "%");
            }
            if (isPayFee != null && !isPayFee.isEmpty()) {
                ps.setInt(paramIndex++, Integer.parseInt(isPayFee));
            }
            if (isStatus != null && !isStatus.isEmpty()) {
                ps.setInt(paramIndex++, Integer.parseInt(isStatus));
            }

            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return 0;
    }

    public List<Account> getAccounts(String search, String isPayFee, String isStatus, int currentPage, int pageSize) {
        List<Account> list = new ArrayList<>();
        StringBuilder sql = new StringBuilder("SELECT * FROM Account WHERE 1=1 ");
        if (search != null && !search.isEmpty()) {
            sql.append(" AND (Email LIKE ? OR FullName LIKE ?)");
        }
        if (isPayFee != null && !isPayFee.isEmpty()) {
            sql.append(" AND isPayfee = ?");
        }
        if (isStatus != null && !isStatus.isEmpty()) {
            sql.append(" AND isStatus = ?");
        }
        sql.append(" ORDER BY UserID OFFSET ? ROWS FETCH NEXT ? ROWS ONLY");

        try (Connection conn = getConnection(); PreparedStatement ps = conn.prepareStatement(sql.toString())) {
            int paramIndex = 1;
            if (search != null && !search.isEmpty()) {
                ps.setString(paramIndex++, "%" + search + "%");
                ps.setString(paramIndex++, "%" + search + "%");
            }
            if (isPayFee != null && !isPayFee.isEmpty()) {
                ps.setInt(paramIndex++, Integer.parseInt(isPayFee));
            }
            if (isStatus != null && !isStatus.isEmpty()) {
                ps.setInt(paramIndex++, Integer.parseInt(isStatus));
            }
            ps.setInt(paramIndex++, (currentPage - 1) * pageSize);
            ps.setInt(paramIndex++, pageSize);

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Account acc = new Account();
                acc.setId(rs.getInt("UserID"));
                acc.setUsername(rs.getString("Username"));
                acc.setEmail(rs.getString("Email"));
                acc.setFullname(rs.getString("FullName"));
                acc.setAddress(rs.getString("Address"));
                acc.setPhone(rs.getString("Phone"));
                acc.setIsPayfee(rs.getInt("isPayfee"));
                acc.setIsStatus(rs.getInt("isStatus"));
                list.add(acc);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return list;
    }

    public void updateVerificationCode(int id, String verificationCode) throws Exception {
        try (Connection connection = getConnection()) {
            String sql = "UPDATE Account SET VerificationCode = ?, VerificationCodeExpiry = ? WHERE UserID = ?";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, verificationCode);
            ps.setTimestamp(2, new Timestamp(System.currentTimeMillis() + 30 * 60 * 1000)); // Mã hết hạn sau 30 phút
            ps.setInt(3, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Account getAccountByVerificationCode(String code) throws Exception {
        String sql = "SELECT * FROM Account WHERE VerificationCode = ?";
        try (Connection connection = getConnection(); PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setString(1, code);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                Account acc = new Account();
                acc.setId(rs.getInt("UserID"));
                acc.setUsername(rs.getString("Username"));
                acc.setPassword(rs.getString("Password"));
                acc.setEmail(rs.getString("Email"));
                acc.setAddress(rs.getString("Address"));
                acc.setFullname(rs.getString("FullName"));
                acc.setPhone(rs.getString("Phone"));
                acc.setIsPayfee(rs.getInt("isPayfee"));
                acc.setIsAdmin(rs.getInt("isAdmin"));
                acc.setAvatar(rs.getString("Avatar"));
                acc.setIsStatus(rs.getInt("isStatus"));
                acc.setVerificationCode(rs.getString("VerificationCode"));
                acc.setVerificationCodeExpiry(rs.getTimestamp("VerificationCodeExpiry"));
                return acc;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean verifyAccount(int userId) throws Exception {
        String sql = "UPDATE Account SET isStatus = 1 WHERE UserID = ?";
        try (Connection connection = getConnection(); PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setInt(1, userId);
            int rowsAffected = ps.executeUpdate();
            return rowsAffected > 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean isVerificationCodeExpired(int id) throws Exception {
        try (Connection connection = getConnection()) {
            String sql = "SELECT VerificationCodeExpiry FROM Account WHERE UserID = ?";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                Timestamp expiry = rs.getTimestamp("VerificationCodeExpiry");
                if (expiry != null && expiry.before(new Timestamp(System.currentTimeMillis()))) {
                    return true;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public void clearVerificationCode(int id) throws Exception {
        try (Connection connection = getConnection()) {
            String sql = "UPDATE Account SET VerificationCode = NULL, VerificationCodeExpiry = NULL WHERE UserID = ?";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        AccountDao accountDao = new AccountDao();

        // Tạo đối tượng Account với thông tin đăng ký
        Account newAccount = new Account();
        newAccount.setUsername("testuser");
        newAccount.setPassword("password123");
        newAccount.setEmail("testuser@example.com");
        newAccount.setAddress("123 Test Street");
        newAccount.setFullname("Test User");
        newAccount.setPhone("0123456789");
        newAccount.setIsPayfee(0);
        newAccount.setIsAdmin(0);
        newAccount.setAvatar("");
        newAccount.setIsStatus(0);

        // Gọi phương thức registerAccount để đăng ký tài khoản
        boolean isRegistered = accountDao.registerAccount(newAccount);

        // Kiểm tra kết quả đăng ký và in ra thông tin tương ứng
        if (isRegistered) {
            System.out.println("Registration successful.");
        } else {
            System.out.println("Registration failed.");
        }
    }
}
