/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import context.DBContext;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author admin
 */
public class WordDAO extends DBContext {

    public List<String> getWordsByContent(String word) {
        try {
            List<String> list = new ArrayList<>();
            String sql = """
                         SELECT [id]
                                 ,[word],dbo.ufn_removeMark(word) as word2
                             FROM [CheckGrammar].[dbo].[Words] where dbo.ufn_removeMark(word) = ? or 
                             word = ?""";
            Connection connection = getConnection();
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, word);
            ps.setString(2, word);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(rs.getString(2));
            }
            return list;
        } catch (Exception ex) {
            Logger.getLogger(BlogDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
