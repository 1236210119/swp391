(() => {
  "use strict";
  var e,
    t,
    r,
    a,
    n,
    o = {},
    s = {};
  function c(e) {
    var t = s[e];
    if (void 0 !== t) {
      if (void 0 !== t.error) throw t.error;
      return t.exports;
    }
    var r = (s[e] = { id: e, loaded: !1, exports: {} });
    try {
      o[e].call(r.exports, r, r.exports, c);
    } catch (e) {
      throw ((r.error = e), e);
    }
    return (r.loaded = !0), r.exports;
  }
  (c.m = o),
    (c.amdO = {}),
    (e = []),
    (c.O = (t, r, a, n) => {
      if (!r) {
        var o = 1 / 0;
        for (d = 0; d < e.length; d++) {
          for (var [r, a, n] = e[d], s = !0, l = 0; l < r.length; l++)
            (!1 & n || o >= n) && Object.keys(c.O).every((e) => c.O[e](r[l]))
              ? r.splice(l--, 1)
              : ((s = !1), n < o && (o = n));
          if (s) {
            e.splice(d--, 1);
            var i = a();
            void 0 !== i && (t = i);
          }
        }
        return t;
      }
      n = n || 0;
      for (var d = e.length; d > 0 && e[d - 1][2] > n; d--) e[d] = e[d - 1];
      e[d] = [r, a, n];
    }),
    (c.n = (e) => {
      var t = e && e.__esModule ? () => e.default : () => e;
      return c.d(t, { a: t }), t;
    }),
    (r = Object.getPrototypeOf
      ? (e) => Object.getPrototypeOf(e)
      : (e) => e.__proto__),
    (c.t = function (e, a) {
      if ((1 & a && (e = this(e)), 8 & a)) return e;
      if ("object" == typeof e && e) {
        if (4 & a && e.__esModule) return e;
        if (16 & a && "function" == typeof e.then) return e;
      }
      var n = Object.create(null);
      c.r(n);
      var o = {};
      t = t || [null, r({}), r([]), r(r)];
      for (var s = 2 & a && e; "object" == typeof s && !~t.indexOf(s); s = r(s))
        Object.getOwnPropertyNames(s).forEach((t) => (o[t] = () => e[t]));
      return (o.default = () => e), c.d(n, o), n;
    }),
    (c.d = (e, t) => {
      for (var r in t)
        c.o(t, r) &&
          !c.o(e, r) &&
          Object.defineProperty(e, r, { enumerable: !0, get: t[r] });
    }),
    (c.f = {}),
    (c.e = (e) =>
      Promise.all(Object.keys(c.f).reduce((t, r) => (c.f[r](e, t), t), []))),
    (c.u = (e) =>
      7768 === e
        ? "js/_new/react.bundle.js"
        : "js/_new/chunks/" +
          {
            713: "character_reading",
            782: "languagetool-es-external-react",
            881: "character_idle",
            1078: "languagetool-pt-br-external-react",
            1514: "languagetool-ca-external-react",
            1676: "papaparse",
            1906: "languagetool-fr-external-react",
            2796: "user-management-view",
            3704: "lottie-lib",
            4516: "languagetool-nl-external-react",
            4606: "languagetool-pl-external-react",
            5487: "languagetool-ru-external-react",
            6008: "editor",
            6128: "user-statistics-view",
            6146: "rephrasing_empty_state",
            6225: "languagetool-de-external-react",
            6303: "shortcut_onboarding",
            6502: "languagetool-pt-external-react",
            7881: "standalone-styles",
            7987: "react-transition-group_CSSTransition",
            8059: "languagetool-it-external-react",
            8697: "languagetool-en-external-react",
            8966: "languagetool-uk-external-react",
            9339: "character_error",
            9826: "qb_paraphraser-core",
          }[e] +
          ".js?id=" +
          {
            713: "1d736324766f9702",
            782: "beddc97c388be6fc",
            881: "e6180ec3f3c49985",
            1078: "e2de205c6347af45",
            1514: "d1c5bba807e7c385",
            1676: "98137ef4d8046ada",
            1906: "d618763d7e1cfaa5",
            2796: "974f3e2d4c2da5c9",
            3704: "d3dea2b2e5a4b3a0",
            4516: "c8580e9587a5077a",
            4606: "0cbc06bec0946b7c",
            5487: "541b5562c9e9a0da",
            6008: "631f3eb2da95707b",
            6128: "fdd79b34ffa9fbc1",
            6146: "8ff347ccb72b8aaa",
            6225: "14f0f3612a376b89",
            6303: "a832b19749cea728",
            6502: "2840a51427881109",
            7881: "32ef92cd0bcfae66",
            7987: "a1701dc70e73ad27",
            8059: "b721f74c3e07cf0e",
            8697: "2c00e3d4e9de54ad",
            8966: "9b99036645f25b9a",
            9339: "13cb41c2bef1d6e6",
            9826: "c5f89f0b24b635af",
          }[e]),
    (c.miniCssF = (e) =>
      ({
        752: "css/_new/onboarding",
        876: "css/_new/dev",
        2338: "css/_new/about",
        2618: "css/_new/offboarding",
        2996: "css/_new/cookie-banner",
        3490: "css/_new/webextension",
        3993: "css/_new/business",
        4146: "css/_new/premium",
        4223: "css/_new/media_assets",
        4364: "css/_new/app",
        4522: "css/_new/errors",
        5864: "css/_new/desktop_app_download_success",
        6982: "css/_new/lp_desktop_app",
        7213: "css/_new/sponsoring",
        7253: "css/_new/webextension/uninstall",
        8181: "css/_new/finish_installation",
        8296: "css/_new/client_login",
        8993: "css/_new/premium_new",
        9005: "css/_new/api",
        9619: "css/_new/checkout",
        9920: "css/_new/print",
      }[e] + ".css")),
    (c.g = (function () {
      if ("object" == typeof globalThis) return globalThis;
      try {
        return this || new Function("return this")();
      } catch (e) {
        if ("object" == typeof window) return window;
      }
    })()),
    (c.o = (e, t) => Object.prototype.hasOwnProperty.call(e, t)),
    (a = {}),
    (n = "@languagetool-web/website:"),
    (c.l = (e, t, r, o) => {
      if (a[e]) a[e].push(t);
      else {
        var s, l;
        if (void 0 !== r)
          for (
            var i = document.getElementsByTagName("script"), d = 0;
            d < i.length;
            d++
          ) {
            var u = i[d];
            if (
              u.getAttribute("src") == e ||
              u.getAttribute("data-webpack") == n + r
            ) {
              s = u;
              break;
            }
          }
        s ||
          ((l = !0),
          ((s = document.createElement("script")).charset = "utf-8"),
          (s.timeout = 120),
          c.nc && s.setAttribute("nonce", c.nc),
          s.setAttribute("data-webpack", n + r),
          (s.src = e)),
          (a[e] = [t]);
        var f = (t, r) => {
            (s.onerror = s.onload = null), clearTimeout(b);
            var n = a[e];
            if (
              (delete a[e],
              s.parentNode && s.parentNode.removeChild(s),
              n && n.forEach((e) => e(r)),
              t)
            )
              return t(r);
          },
          b = setTimeout(
            f.bind(null, void 0, { type: "timeout", target: s }),
            12e4
          );
        (s.onerror = f.bind(null, s.onerror)),
          (s.onload = f.bind(null, s.onload)),
          l && document.head.appendChild(s);
      }
    }),
    (c.r = (e) => {
      "undefined" != typeof Symbol &&
        Symbol.toStringTag &&
        Object.defineProperty(e, Symbol.toStringTag, { value: "Module" }),
        Object.defineProperty(e, "__esModule", { value: !0 });
    }),
    (c.nmd = (e) => ((e.paths = []), e.children || (e.children = []), e)),
    (c.p = "/"),
    (() => {
      var e = {
        3531: 0,
        4522: 0,
        3490: 0,
        9920: 0,
        4364: 0,
        5864: 0,
        2996: 0,
        8181: 0,
        6982: 0,
        9619: 0,
        4223: 0,
        2338: 0,
        2618: 0,
        752: 0,
        876: 0,
        9005: 0,
        8296: 0,
        7213: 0,
        3993: 0,
        8993: 0,
        4146: 0,
        7253: 0,
      };
      (c.f.j = (t, r) => {
        var a = c.o(e, t) ? e[t] : void 0;
        if (0 !== a)
          if (a) r.push(a[2]);
          else if (
            /^(2(338|618|996)|3(490|531|993)|4(146|223|364|522)|7(213|253|52)|8(181|296|76|993)|9(005|619|920)|5864|6982)$/.test(
              t
            )
          )
            e[t] = 0;
          else {
            var n = new Promise((r, n) => (a = e[t] = [r, n]));
            r.push((a[2] = n));
            var o = c.p + c.u(t),
              s = new Error();
            c.l(
              o,
              (r) => {
                if (c.o(e, t) && (0 !== (a = e[t]) && (e[t] = void 0), a)) {
                  var n = r && ("load" === r.type ? "missing" : r.type),
                    o = r && r.target && r.target.src;
                  (s.message =
                    "Loading chunk " + t + " failed.\n(" + n + ": " + o + ")"),
                    (s.name = "ChunkLoadError"),
                    (s.type = n),
                    (s.request = o),
                    a[1](s);
                }
              },
              "chunk-" + t,
              t
            );
          }
      }),
        (c.O.j = (t) => 0 === e[t]);
      var t = (t, r) => {
          var a,
            n,
            [o, s, l] = r,
            i = 0;
          if (o.some((t) => 0 !== e[t])) {
            for (a in s) c.o(s, a) && (c.m[a] = s[a]);
            if (l) var d = l(c);
          }
          for (t && t(r); i < o.length; i++)
            (n = o[i]), c.o(e, n) && e[n] && e[n][0](), (e[n] = 0);
          return c.O(d);
        },
        r = (self.webpackChunk_languagetool_web_website =
          self.webpackChunk_languagetool_web_website || []);
      r.forEach(t.bind(null, 0)), (r.push = t.bind(null, r.push.bind(r)));
    })(),
    (c.nc = void 0);
})();
