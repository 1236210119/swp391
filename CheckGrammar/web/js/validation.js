document.addEventListener("DOMContentLoaded", () => {
    const form = document.getElementById('registerForm');
    const emailInput = document.getElementById('email');
    const errorPopup = document.getElementById('errorPopup');
    const errorMessage = document.getElementById('errorMessage');

    form.addEventListener('submit', function(event) {
        if (!validateEmail(emailInput.value)) {
            event.preventDefault();
            showErrorMessage('Invalid email address');
        }
    });

    function validateEmail(email) {
        const re = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
        return re.test(String(email).toLowerCase());
    }

    function showErrorMessage(message) {
        errorMessage.textContent = message;
        errorPopup.style.display = 'block';
    }

    window.closePopup = function() {
        errorPopup.style.display = 'none';
    }
});
