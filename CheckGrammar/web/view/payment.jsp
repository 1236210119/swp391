<%-- 
    Document   : payment
    Created on : Jun 11, 2024, 2:42:40 AM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <style>
            body {
                font-family: Arial, sans-serif;
            }
            .container {
                width: 80%;
                margin: 0 auto;
                border: 1px solid #ddd;
                border-radius: 5px;
                padding: 20px;
                box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
            }
            .header {
                text-align: center;
                margin-bottom: 20px;
            }
            table {
                width: 100%;
                border-collapse: collapse;
            }
            th, td {
                border: 1px solid #ddd;
                padding: 10px;
                text-align: center;
            }
            th {
                background-color: #f4f4f4;
            }
            .btn {
                background-color: #007bff;
                color: white;
                border: none;
                padding: 10px 20px;
                text-align: center;
                text-decoration: none;
                display: inline-block;
                font-size: 14px;
                margin: 4px 2px;
                cursor: pointer;
                border-radius: 5px;
            }
            .search-box {
                text-align: right;
                margin-bottom: 10px;
            }
            .search-box input {
                padding: 5px;
                width: 200px;
            }

            /* Popup container - can be anything you want */
            .popup {
                position: fixed;
                display: none;
                width: 100%;
                height: 100%;
                top: 0;
                left: 0;
                right: 0;
                bottom: 0;
                background-color: rgba(0,0,0,0.5);
                z-index: 2;
            }

            /* Popup content */
            .popup-content {
                margin: 15% auto;
                padding: 20px;
                border: 1px solid #888;
                width: 30%;
                background-color: white;
                border-radius: 10px;
                text-align: center;
            }

            .close {
                color: #aaa;
                float: right;
                font-size: 28px;
                font-weight: bold;
            }

            .close:hover,
            .close:focus {
                color: black;
                text-decoration: none;
                cursor: pointer;
            }

            .error-message {
                color: red;
                text-align: center;
                margin-bottom: 20px;
            }
        </style>
    </head>
    <body>
        <jsp:include page="head.jsp"></jsp:include>  
        <br>
        <div class="container">
            <div class="header">
                <h1>Membership Plan List</h1>
            </div>
            <table>
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Package Name</th>
                        <th>Price</th>
                        <th>Duration</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1</td>
                        <td>Yearly</td>
                        <td>36 USD</td>
                        <td>1 year</td>
                        <td><button class="btn" onclick="openPopup('Yearly', '36', '1 year')">Purchase</button></td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>3 months</td>
                        <td>16 USD</td>
                        <td>3 months</td>
                        <td><button class="btn" onclick="openPopup('3 months', '16', '3 months')">Purchase</button></td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>1 month</td>
                        <td>8 USD</td>
                        <td>1 month</td>
                        <td><button class="btn" onclick="openPopup('1 month', '8', '1 month')">Purchase</button></td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td>1 week</td>
                        <td>3 USD</td>
                        <td>1 week</td>
                        <td><button class="btn" onclick="openPopup('1 week', '3', '1 week')">Purchase</button></td>
                    </tr>
                </tbody>
            </table>
            
        </div>

        <div id="popup" class="popup">
            <div class="popup-content">
                <span class="close" onclick="closePopup()">&times;</span>
                <h2>Purchase Subscription</h2>
                <p><strong>Package:</strong> <span id="packageName"></span></p>
                <p><strong>Price:</strong> <span id="price"></span></p>
                <p><strong>Duration:</strong> <span id="duration"></span></p>
                <button class="btn" onclick="processPayment()">VnPay</button>
            </div>
        </div>

        <!-- Payment Success Popup -->
        <div id="successPopup" class="popup">
            <div class="popup-content">
                <span class="close" onclick="closeSuccessPopup()">&times;</span>
                <h2>Payment Successful</h2>
                <p>Thank you for your purchase!</p>
            </div>
        </div>

        <br>
        <jsp:include page="footer.jsp"></jsp:include>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script>
            function openPopup(packageName, price, duration) {
                const popup = document.getElementById("popup");
                popup.style.display = "block";
                document.getElementById("packageName").innerText = packageName;
                document.getElementById("price").innerText = price + ' USD';
                document.getElementById("duration").innerText = duration;
                popup.dataset.packageName = packageName;
                popup.dataset.price = price;
                popup.dataset.duration = duration;
            }

            function closePopup() {
                document.getElementById("popup").style.display = "none";
            }

            function closeSuccessPopup() {
                document.getElementById("successPopup").style.display = "none";
            }

            function processPayment() {
                const popup = document.getElementById("popup");
                const packageName = popup.dataset.packageName;
                const price = popup.dataset.price;
                const duration = popup.dataset.duration;

                const postData = {
                    packageName: packageName,
                    price: price,
                    duration: duration
                };

                $.ajax({
                    type: "POST",
                    url: "VnpayController",
                    data: postData,
                    dataType: 'JSON',
                    success: function (response) {
                        if (response.code === '00') {
                            window.location.href = response.data;
                        } else {
                            alert(response.message);
                        }
                    },
                    error: function () {
                        alert("Payment processing failed!");
                    }
                });
            }

            <% if (request.getAttribute("paymentSuccess") != null && (Boolean) request.getAttribute("paymentSuccess")) { %>
                $(document).ready(function () {
                    document.getElementById("successPopup").style.display = "block";
                });
            <% } %>
        </script>
    </body>
</html>
