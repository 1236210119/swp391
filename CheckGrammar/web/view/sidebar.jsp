<%-- 
    Document   : sidebar
    Created on : May 28, 2024, 11:58:22 PM
    Author     : hung
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-alpha1/dist/css/bootstrap.min.css">
    </head>
    <body>
        <div class="sidebar" role="cdb-sidebar" color="white">
            <div class="sidebar-container">
                <div class="sidebar-header">
                    <a class="sidebar-brand">Contrast Light</a>
                    <a class="sidebar-toggler"><i class="fa fa-bars"></i></a>
                </div>
                <div class="sidebar-nav">
                    <div class="sidenav">
                        <a class="sidebar-item">
                            <div class="sidebar-item-content">
                                <i class="fa fa-th-large sidebar-icon sidebar-icon-lg"></i>
                                <span>Dashboard</span>
                                <div class="suffix">
                                    <div class="badge rounded-pill bg-danger">new</div>
                                </div>
                            </div>
                        </a>
                        <a class="sidebar-item">
                            <div class="sidebar-item-content">
                                <i class="fa fa-sticky-note sidebar-icon"></i>
                                <span>Components</span>
                            </div>
                        </a>
                        <a class="sidebar-item">
                            <div class="sidebar-item-content">
                                <i class="fa fa-sticky-note sidebar-icon"></i>
                                <span>Bootstrap</span>
                            </div>
                        </a>
                    </div>
                    <div class="sidebar-footer">Sidebar Footer</div>
                </div>
            </div>
        </div>
    </body>

    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
</html>
