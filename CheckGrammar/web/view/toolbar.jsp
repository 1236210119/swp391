<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">
        <style>
            .toolbar {
                display: flex;
                justify-content: space-between;
                background-color: #555;
                padding: 10px;
                color: white;
                position: relative;
            }
            .toolbar div {
                cursor: pointer;
                display: flex;
                align-items: center;
            }
            .toolbar div i {
                margin-right: 5px;
            }
        </style>
    </head>
    <body>
        <div class="toolbar">
            <div id="copy"><i class="fas fa-copy"></i>COPY</div>
            <div id="paste"><i class="fas fa-paste"></i>PASTE</div>
            <div id="clear"><i class="fas fa-eraser"></i>CLEAR</div>
            <div id="paraphrasing"><i class="fas fa-sync-alt"></i>PARAPHRASING</div>
            <div id="paste-sample"><i class="fas fa-flask"></i>PASTE SAMPLE TEXT</div>
        </div>

        <script>
            // Toolbar functionality
            document.getElementById('copy').addEventListener('click', function () {
                var text = document.getElementById('input-text').value;
                navigator.clipboard.writeText(text).then(function () {
                    alert('Text copied to clipboard');
                });
            });

            document.getElementById('paste').addEventListener('click', function () {
                navigator.clipboard.readText().then(function (text) {
                    document.getElementById('input-text').value = text;
                });
            });

            document.getElementById('clear').addEventListener('click', function () {
                document.getElementById('input-text').value = '';
            });

            document.getElementById('paraphrasing').addEventListener('click', function () {
                alert('Paraphrasing clicked');
            });

            document.getElementById('paste-sample').addEventListener('click', function () {
                var sampleText = "Tommorow, I will be going to the libary to study for my exxams. I realy need to pass this tim. I have been strugling with my math and sceince classes. My freind Sarah, who is also my clasmate, will be joining me. We plan to studdy for atleast three hours.";
                document.getElementById('input-text').value = sampleText;
            });
        </script>
    </body>
</html>
