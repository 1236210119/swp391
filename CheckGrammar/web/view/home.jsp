<%-- 
    Document   : home
    Created on : Jun 1, 2024, 11:51:15 PM
    Author     : ducpm
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <body>
        <jsp:include page="head.jsp"></jsp:include>  
        <jsp:include page="homeContent.jsp"></jsp:include> 
        <jsp:include page="footer.jsp"></jsp:include>     
    </body>
</html>
