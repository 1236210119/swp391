<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
        <style>
            #backToHomepage {
                display: none;
            }
        </style>
    </head>
    <body>
        <h1>We have sent a verification code to your email.</h1>
        <p id="countdown">Time remaining: </p>


        <form action="/TechStore/VerifyCode" method="post" id="verifyForm">
            <label for="authcode">Enter the verification code:</label>
            <input type="text" name="authcode" id="authcodeInput" required>
            <input type="submit" value="Verify" id="verifyButton">
        </form>
        <div id="errorcode">
            <c:if test="${not empty errorMessage}">
                <h3 style="color: red">${errorMessage}</h3>
            </c:if>
        </div>
        <div id="backToHomepage" class="mt-3">
            <a href="/TechStore/invalidateSession" class="btn btn-secondary">Back to Homepage</a>
        </div>

        <script>
            // Thời gian hết hạn (trong milliseconds)
            var expirationTime = ${sessionScope.verificationTime} + 30000; // Thời gian hết hạn là 1 phút sau thời điểm gửi

            // Cập nhật hiển thị thời gian còn lại
            var countdownElement = document.getElementById("countdown");
            var verifyForm = document.getElementById("verifyForm");
            var backToHomepage = document.getElementById("backToHomepage");
            var errorcode = document.getElementById("errorcode");
            function updateCountdown() {
                var currentTime = new Date().getTime();
                var timeRemaining = expirationTime - currentTime;

                if (timeRemaining <= 0) {
                    // Thời gian đã hết, ngăn chặn nhập mã xác nhận và dừng đếm ngược
                    clearInterval(timer);
                    errorcode.style.display="none";
                    verifyForm.style.display = "none"; // Ẩn form nhập mã xác nhận
                    countdownElement.textContent = "Time expired";
                    backToHomepage.style.display = "block"; // Hiển thị nút "Back to Homepage"
                    return;
                }

                var seconds = Math.floor((timeRemaining % (1000 * 60)) / 1000);
                countdownElement.textContent = "Time remaining: " + seconds + "s";
            }

            var timer = setInterval(updateCountdown, 1000); // Cập nhật mỗi giây
            updateCountdown(); // Cập nhật ban đầu
        </script>
    </body>
</html>
