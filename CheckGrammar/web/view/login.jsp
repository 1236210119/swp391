<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html
    lang="en"
    >
    <head>
        <meta charset="utf-8" />
        <meta
            http-equiv="Content-Security-Policy"
            content="base-uri 'self';connect-src *;default-src 'self';form-action 'self' http://localhost:8080;frame-src https:;img-src https: http: data:;media-src 'self';object-src 'self';script-src 'self' d1f8f9xcsvx3ha.cloudfront.net *.braintreegateway.com www.googletagmanager.com www.google-analytics.com ssl.google-analytics.com www.googleadservices.com www.dwin1.com bat.bing.com www.google.com languagetool.org analytics.languagetoolplus.com *.paypal.com www.paypalobjects.com *.recurly.com applepay.cdn-apple.com *.taboola.com *.fullstory.com *.cardinalcommerce.com browser.sentry-cdn.com *.stripe.com *.cloudflare.com analytics.tiktok.com *.facebook.net 'unsafe-eval' 'nonce-VmBVz7YLxfs80DgwfGV4wjUz7m288LgF';style-src 'self' 'unsafe-inline' js.recurly.com"
            />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <title>Login - LanguageTool</title>
        <link rel="stylesheet" href="./css/style.css" />
        <link href="./css/custom-style.css" rel="stylesheet" fetchpriority="high" />
        <link
            href="/fonts/Source-Sans-Pro-latin-win.woff2"
            rel="preload"
            as="font"
            type="font/woff2"
            crossorigin=""
            fetchpriority="high"
            />
        <link
            href="/fonts/Source-Sans-Pro-latin-bold-win.woff2"
            rel="preload"
            as="font"
            type="font/woff2"
            crossorigin=""
            fetchpriority="high"
            />
        <link
            href="/fonts/Source-Sans-Pro-latin-semi-bold-win.woff2"
            rel="preload"
            as="font"
            type="font/woff2"
            crossorigin=""
            fetchpriority="high"
            />
        <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" rel="stylesheet">
        <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
        <script src="./js/mainfest.js" defer="" fetchpriority="high"></script>
        <script src="./js/react.js" defer="" fetchpriority="high"></script>
        <script src="./js/custom-app.js" defer="" fetchpriority="high"></script>
        <style>
            .row {
                display: flex;
                align-items: center;
                flex-wrap: wrap;
            }

            .cm-col-lg-6, .cm-col-12 {
                flex: 1;
            }

            .logo {
                display: flex;
                align-items: center;
            }

            .site-logo {
                display: flex;
                align-items: center;
            }

            .custom-logo-link {
                margin-right: 10px;
            }

            .site-info {
                display: flex;
                flex-direction: column;
            }

            .site-title {
                font-size: 1.5em;
                margin: 0;
            }

            .site-title a {
                text-decoration: none;
                color: inherit;
            }

            .site-description {
                font-size: 1em;
                margin: 0;
            }
        </style>
    </head>
    <body class="body" data-force-appearance="default" cz-shortcut-listen="true">
        <div class="page page--login">
            <section class="content content--login">
                <div class="row align-items-center">
                    <div class="cm-col-lg-6 cm-col-12">
                        <div class="logo">
                            <div class="site-logo">
                                <a href="images/home/logo.jpg" class="custom-logo-link" rel="home">
                                    <img
                                        width="50" height="50" src="images/home/logo.jpg"
                                        class="custom-logo" alt="The POET magazine" decoding="async"
                                        fetchpriority="high" srcset="
                                        images/home/logo.jpg 100w,
                                        images/home/logo.jpg 50w,
                                        images/home/logo.jpg 30w"
                                        sizes="(max-width: 100px) 100vw, 250px"
                                        />
                                </a>
                                <div class="site-info">
                                    <span class="site-title"><a href="" rel="home">
                                            Grammar Tool
                                        </a></span>
                                    <p class="site-description">
                                        Your writing assistant, Spelling check, Plagiarism check
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="login-content-wrapper">
                    <h1 class="headline headline--3 headline--center">Login</h1>

                    <p class="paragraph--4">
                        Don’t have an account yet?
                        <a href="register" class="link link--primary"
                           >Sign up for free here</a
                        >
                    </p>

                    <div class="content-social-login">
                        <div
                            class="btn-group--social-login btn-group--social-login-collapsed collapsed-social-buttons"
                            >
                            <a
                                class="btn btn--social-login btn--social-login--google"
                                data-track-click="Login|select_provider|google"
                                href="https://accounts.google.com/o/oauth2/auth?scope=email&redirect_uri=http://localhost:8080/CheckGrammar/googlelogin&response_type=code&client_id=289394606583-ivv3jgi19f858m29dada6n7l23fi0kh4.apps.googleusercontent.com&approval_prompt=force"
                                >
                                <span class="btn--social-login__label">
                                    Continue with Google
                                </span>
                            </a>
                        </div>
                    </div>

                    <div class="separator--login">or</div>

                    <!-- form login-->
                    <c:if  test="${not empty loginMessage}">
                        <span style="color: red">${loginMessage}</span>
                    </c:if>
                    <form
                        class="form form--account"
                        method="POST"
                        action="login"
                        target="_self"
                        >       
                        <label class="label label--input">
                            <input
                                type="text"
                                name="user"
                                class="empty"
                                />
                            <span>Username</span>
                        </label>
                        <label class="label label--input">
                            <input
                                type="password"
                                name="pass"
                                class="empty"
                                />
                            <span>Password</span>
                        </label>

                        <button
                            class="btn btn--primary btn--full-size btn--large"
                            type="submit"
                            name = "loginBtn" 
                            value = "login"
                            >
                            Login
                        </button>

                        <div class="form__link">
                            <a class="link link--primary" href="forgot"
                               >Forgot your password?</a
                            >
                        </div>
                    </form>
                </div>
                <div class="footer-nav | flex">
                    <div class="footer-nav__wrapper | flex">
                        <ul class="footer-nav__wrapper__list | flex">
                            <li
                                class="made-in footer-nav__wrapper__list__item | block text-center"
                                id="made-in"
                                >
                                Made with Group 5 in FPT University
                            </li>
                            <li>
                                <ul
                                    class="footer-nav__wrapper__list__sublist | flex lt-lg:flex-col lt-lg:items-center lt-xl:mb-4"
                                    >
                                    <li class="footer-nav__wrapper__list__item">
                                        <a href="/legal/privacy">Privacy</a>
                                    </li>
                                    <li class="footer-nav__wrapper__list__item">
                                        <a href="/legal/terms">Terms &amp; Conditions</a>
                                    </li>
                                    <li class="footer-nav__wrapper__list__item">
                                        <a href="/legal/">Imprint</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </section>

            <div class="page--login__tips-container">
                <div
                    class="page--login__tips-container__content login-tips-illustration--word-card"
                    >
                    <div class="dyk-label">Did you know?</div>
                    <span class="paragraph--2">Synonyms, Articles, Pronunciation</span>
                    <p class="headline--2">
                        Double-click on any word in your text to get further information.
                    </p>
                </div>
            </div>
        </div>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
        <script type="text/javascript">
            toastr.options = {
                "closeButton": true,
                "debug": false,
                "newestOnTop": false,
                "progressBar": true,
                "positionClass": "toast-top-right",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };
            $(document).ready(function () {
                var data = '${requestScope.report}';
                if (data !== '') {
                    if (data !== 'Đăng nhập thành công') {
                        toastr.error('${requestScope.report}', 'Thất bại');
                    } else {
                        toastr.options = {
                            "closeButton": true,
                            "progressBar": true,
                            "positionClass": "toast-top-right",
                            "timeOut": "5000", // 5 giây
                            "extendedTimeOut": "1000",
                            "onHidden": function () {
                                // Chuyển trang sau khi thông báo ẩn
                                window.location.href = '${pageContext.request.contextPath}/home'; // Thay đổi URL thành trang bạn muốn chuyển đến
                            }
                        };
                        toastr.success('${requestScope.report}', 'Thành công');
                    }
                }
            });
        </script>
    </body>
</html>