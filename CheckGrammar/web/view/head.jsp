<%-- 
    Document   : header
    Created on : May 19, 2024, 11:28:41 PM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <script
            type="text/javascript"
            async=""
            src=""
        ></script>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta
            name="robots"
            content="index, follow, max-image-preview:large, max-snippet:-1, max-video-preview:-1"
            />
        <title>Header</title>
        <link
            data-minify="1"
            rel="stylesheet"
            id="cream-magazine-style-css"
            href="css/style.css"
            type="text/css"
            media="all"
            />
        <link
            data-minify="1"
            rel="stylesheet"
            id="cream-magazine-main-css"
            href="css/main.css"
            type="text/css"
            media="all"
            />
    </head>

    <body>
        <header class="general-header cm-header-style-one">
            <div class="cm-container">
                <div class="row align-items-center">
                    <div class="cm-col-lg-6 cm-col-12">
                        <div class="logo">
                            <div class="site-logo">
                                <a href="images/home/logo.jpg" class="custom-logo-link" rel="home"><img
                                        width="250" height="250" src="images/home/logo.jpg"
                                        class="custom-logo" alt="The POET magazine" decoding="async"
                                        fetchpriority="high" srcset="
                                        images/home/logo.jpg         250w,
                                        images/home/logo.jpg 100w,
                                        images/home/logo.jpg    60w
                                        " sizes="(max-width: 250px) 100vw, 250px" /></a>
                            </div>
                            <div>
                                <span class="site-title"><a href="" rel="home">
                                        Grammar Tool
                                    </a></span>
                                <p class="site-description">
                                    Your writing assistant, Spelling check, Plagiarism check
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- .logo-container -->
                <nav class="main-navigation">
                    <div id="main-nav" class="primary-navigation dark desktop">
                        <a href="#" class="menu-toggle full"><i class="fa fa-bars"></i></a>
                        <ul id="menu-menu-chinh" class="">
                            <li
                                id="menu-item-508"
                                class="menu-item menu-item-type-custom menu-item-object-custom menu-item-508"
                                >
                                <a href="home">Homepage</a>
                            </li>
                            <li
                                id="menu-item-8669"
                                class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-8669"
                                >
                                <a href="checkGrammar">Check Grammar</a>
                            </li>
                            <li
                                id="menu-item-8669"
                                class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-8669"
                                >
                                <a href="checkSpell">Check Spelling</a>
                            </li>
                            <c:if test="${not empty loginedUser}" >
                                <li
                                    id="menu-item-9855"
                                    class="menu-item menu-item-type-taxonomy menu-item-object-category current-menu-item menu-item-9855"
                                    >
                                    <a
                                        href="history"
                                        aria-current="page"
                                        >History</a
                                    >
                                </li>
                                <c:if test="${loginedUser.isAdmin == 0}">
                                    <li
                                        id="menu-item-9855"
                                        class="menu-item menu-item-type-taxonomy menu-item-object-category current-menu-item menu-item-9855"
                                        >
                                        <a
                                            href="payment"
                                            aria-current="page"
                                            >Payment</a
                                        >
                                    </li>
                                </c:if>
                                <li
                                    id="menu-item-9855"
                                    class="menu-item menu-item-type-taxonomy menu-item-object-category current-menu-item menu-item-9855"
                                    onclick="showFeedbackModal(event)"
                                    >
                                    <a
                                        href="javascript:void(0);"
                                        aria-current="page"
                                        >Feedback</a>
                                </li>

                                <li
                                    id="menu-item-15093"
                                    class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-15093"
                                    >
                                    <a href="${pageContext.request.contextPath}/profile"
                                       >Profile</a
                                    >
                                </li>

                                <li
                                    id="menu-item-15093"
                                    class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-15093"
                                    >
                                    <a href="${pageContext.request.contextPath}/requestStatus"
                                       >Report</a
                                    >
                                </li>
                                <li
                                    id="menu-item-15093"
                                    class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-15093"
                                    >
                                    <a href="${pageContext.request.contextPath}/viewRequestStatus"
                                       >View Report</a
                                    >
                                </li>
                                <c:if test="${loginedUser.isAdmin == 1}">
                                    <li
                                        id="menu-item-15093"
                                        class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-15093"
                                        >
                                        <a href="${pageContext.request.contextPath}/manage-blog"
                                           >Admin</a
                                        >
                                    </li>
                                </c:if>
                                <li
                                    id="menu-item-15093"
                                    class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-15093"
                                    >
                                    <a href="${pageContext.request.contextPath}/toggleFavorite"
                                       >Favorite blogs</a
                                    >
                                </li>
                            </c:if>
                            <c:if test = "${sessionScope.loginedUser != null}"> 
                                <li><a href="logout"><i class="fa fa-sign-out"></i> logout</a></li>
                                </c:if>

                            <c:if test = "${sessionScope.loginedUser == null}">
                                <li><a href="login"><i class="fa fa-lock"></i> Login</a></li>
                                </c:if>
                        </ul>
                    </div>
                </nav>
            </div>
        </header>
        <jsp:include page="feedback.jsp"></jsp:include> 
    </body>
</html>
