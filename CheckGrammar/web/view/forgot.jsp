<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html
    lang="en"
    data-lt-script-installed="true"
    class="logged-out windows"
    data-country="VN"
    data-adjust-appearance=""
    data-premium="false"
    data-user="false"
    data-platform="windows"
    data-platform-apple="false"
    >
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <title>Forgot Password - LanguageTool</title>

        <link rel="stylesheet" href="./css/style.css" />
        <link href="./css/custom-style.css" rel="stylesheet" fetchpriority="high" />
        <link
            href="/fonts/Source-Sans-Pro-latin-win.woff2"
            rel="preload"
            as="font"
            type="font/woff2"
            crossorigin=""
            fetchpriority="high"
            />
        <link
            href="/fonts/Source-Sans-Pro-latin-bold-win.woff2"
            rel="preload"
            as="font"
            type="font/woff2"
            crossorigin=""
            fetchpriority="high"
            />
        <link
            href="/fonts/Source-Sans-Pro-latin-semi-bold-win.woff2"
            rel="preload"
            as="font"
            type="font/woff2"
            crossorigin=""
            fetchpriority="high"
            />
        <script src="./js/mainfest.js" defer="" fetchpriority="high"></script>
        <script src="./js/react.js" defer="" fetchpriority="high"></script>
        <script src="./js/custom-app.js" defer="" fetchpriority="high"></script>  
        <script src="./js/validation.js" defer></script>
    </head>
    <body class="body" data-force-appearance="default" cz-shortcut-listen="true">
        <div class="page">
            <header
                id="primary-website-header"
                class="header header--white | flex"
                itemtype=""
                itemscope=""
                >
                <div class="header__inner | flex">
                    <a
                        href="/"
                        class="logo logo--black"
                        title="Check your text quickly and easily. Grammar, punctuation, style, and spelling."
                        itemprop="url"
                        >
                        <span
                            class="header__inner__logo-name name notranslate"
                            itemprop="name"
                            >Grammar<strong>Tool</strong></span
                        >
                        <span class="header__inner__logo-claim claim"
                              >Your writing assistant, Spelling check, Plagiarism check</span
                        >
                    </a>
                </div>
            </header>
            <section class="content">
                <div id="content-register" class="content--register">
                    <h1 style="text-align: center" class="headline headline--1">
                        Forgot password
                    </h1>
                    <p class="paragraph--4">
                        You are just one step away from improving your writing. Already have
                        an account?
                        <a href="login" class="link link--primary">Log in here</a>
                    </p>

                   
                    <div class="content-email-forgot">
                        <form
                            class="form form--account"
                            method="POST"
                            action="forgot"
                            >
                            
                            <label class="label label--input">
                                <input
                                    type="email"
                                    name="email"
                                    maxlength="191"
                                    value=""
                                    autocomplete="email"
                                    aria-label="E-mail address"
                                    required=""
                                    class="empty"
                                    />
                                <span>E-mail address</span>
                            </label>
                            
                            <button
                                class="btn btn--primary btn--full-size btn--large"
                                type="submit"
                                >
                                Summit
                            </button>
                        </form>
                        <div
                            id="register-form-social-toggle"
                            >

                        </div>
                    </div>
                    <div class="form-legal-hint">
                        By signing up to LanguageTool, you indicate that you’ve read and
                        agree to our
                        <a           
                            class="link"
                            target="_blank"
                            >Terms of Service</a
                        >
                        and
                        <a          
                            class="link"
                            target="_blank"
                            >Privacy Policy</a
                        >.
                    </div>
                </div>
            </section>

            <div class="footer-nav | flex">
                <div class="footer-nav__wrapper | flex">
                    <ul class="footer-nav__wrapper__list | flex">
                        <li
                            class="made-in footer-nav__wrapper__list__item | block text-center"
                            id="made-in"
                            >
                            Made with Group 5 in FPT University
                        </li>
                        <li>
                            <ul
                                class="footer-nav__wrapper__list__sublist | flex lt-lg:flex-col lt-lg:items-center lt-xl:mb-4"
                                >
                                <li class="footer-nav__wrapper__list__item">
                                    <a href="/legal/privacy">Privacy</a>
                                </li>
                                <li class="footer-nav__wrapper__list__item">
                                    <a href="/legal/terms">Terms &amp; Conditions</a>
                                </li>
                                <li class="footer-nav__wrapper__list__item">
                                    <a href="/legal/">Imprint</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div id="errorPopup" class="popup">
            <span class="popup-close" onclick="closePopup()">&times;</span>
            <p id="errorMessage"></p>
        </div>

        <style>
            .popup {
                display: none;
                position: fixed;
                left: 50%;
                top: 50%;
                transform: translate(-50%, -50%);
                padding: 20px;
                background-color: #f44336;
                color: white;
                border: 1px solid #888;
                z-index: 1000;
            }
            .popup-close {
                float: right;
                cursor: pointer;
            }
        </style>

        <script>
            function showError(message) {
                alert(message);
            }

            window.onload = function () {
                const errorMessage = '<%= request.getAttribute("errorMessage") %>';
                if (errorMessage) {
                    showError(errorMessage);
                }
            }
        </script>
    </body>
</html>
