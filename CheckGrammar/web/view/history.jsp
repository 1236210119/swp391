<%@ page import="java.util.List" %>
<%@ page import="Until.History" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>History</title>
        <style>
            body {
                font-family: Arial, sans-serif;
            }
            .container {
                width: 80%;
                margin: 0 auto;
                border: 1px solid #ddd;
                border-radius: 5px;
                padding: 20px;
                box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
            }
            .header {
                text-align: center;
                margin-bottom: 20px;
            }
            table {
                width: 100%;
                border-collapse: collapse;
            }
            th, td {
                border: 1px solid #ddd;
                padding: 10px;
                text-align: center;
                vertical-align: top; /* Ensure vertical alignment */
            }
            th {
                background-color: #f4f4f4;
            }
            .btn {
                background-color: #007bff;
                color: white;
                border: none;
                padding: 10px 20px;
                text-align: center;
                text-decoration: none;
                display: inline-block;
                font-size: 14px;
                margin: 4px 2px;
                cursor: pointer;
                border-radius: 5px;
            }
            .search-box {
                text-align: right;
                margin-bottom: 10px;
            }
            .search-box input {
                padding: 5px;
                width: 200px;
            }
            .pagination {
                text-align: center;
                margin-top: 20px;
            }
            .pagination a {
                color: black;
                float: left;
                padding: 8px 16px;
                text-decoration: none;
                transition: background-color .3s;
            }
            .pagination a.active {
                background-color: #007bff;
                color: white;
                border-radius: 5px;
            }
            .pagination a:hover:not(.active) {
                background-color: #ddd;
                border-radius: 5px;
            }
            .modal {
                display: none;
                position: fixed;
                z-index: 1;
                left: 0;
                top: 0;
                width: 100%;
                height: 100%;
                overflow: auto;
                background-color: rgb(0,0,0);
                background-color: rgba(0,0,0,0.4);
                padding-top: 60px;
            }
            .modal-content {
                background-color: #fefefe;
                margin: 5% auto;
                padding: 20px;
                border: 1px solid #888;
                width: 80%;
                max-height: 80vh; /* Ensure the modal content does not exceed the viewport height */
                overflow-y: auto; /* Add vertical scroll bar if content overflows */
            }
            .close {
                color: #aaa;
                float: right;
                font-size: 28px;
                font-weight: bold;
            }
            .close:hover,
            .close:focus {
                color: black;
                text-decoration: none;
                cursor: pointer;
            }
        </style>
        <script>
            function showDetails(historyId) {
                fetch('HistoryDetailsServlet?historyId=' + historyId)
                        .then(response => response.json())
                        .then(data => {
                            console.log("Fetched data: ", data); // Debugging log
                            document.getElementById('modalTextContent').innerText = data.TextContent ? data.TextContent : 'N/A';
                            document.getElementById('modalWrongContent').innerHTML = data.WrongContent ? formatErrors(stripHTML(data.WrongContent)) : 'N/A';
                            document.getElementById('modalType').innerText = data.type ? data.type : 'N/A';
                            document.getElementById('modalUploadDate').innerText = data.UploadDate ? data.UploadDate : 'N/A';
                            document.getElementById('historyModal').style.display = 'block';
                        })
                        .catch(error => console.error("Error fetching data: ", error));
            }

            function stripHTML(html) {
                var tempDiv = document.createElement("div");
                tempDiv.innerHTML = html;
                return tempDiv.textContent || tempDiv.innerText || "";
            }

            function formatErrors(text) {
                // Add line breaks before each "Error #" or numbered errors for both Grammar and Spell check
                return text.replace(/(\d+ \".*?\" -> \".*?\")/g, '<br>$1').replace(/Error #/g, '<br>Error #');
            }

            function closeModal() {
                document.getElementById('historyModal').style.display = 'none';
            }
        </script>
    </head>
    <body>
        <jsp:include page="head.jsp"></jsp:include>
            <br>
            <div class="container">
                <div class="header">
                    <h1>History List</h1>
                </div>
                <div class="search-box">
                    <form method="GET" action="history">
                        <input type="text" name="searchText" placeholder="Search history..." value="<%= request.getParameter("searchText") != null ? request.getParameter("searchText") : "" %>">
                    </br>
                    <input type="date" name="startDate" value="<%= request.getParameter("startDate") != null ? request.getParameter("startDate") : "" %>">
                    <input type="date" name="endDate" value="<%= request.getParameter("endDate") != null ? request.getParameter("endDate") : "" %>">
<!--                    <select name="searchType">
                        <option value="">Select Type</option>
                        <option value="grammar" <%= "grammar".equals(request.getParameter("searchType")) ? "selected" : "" %>>Check Grammar</option>
                        <option value="spelling" <%= "spelling".equals(request.getParameter("searchType")) ? "selected" : "" %>>Check Spelling</option>
                    </select>-->
                    <input type="submit" value="Search" class="btn">
                </form>
            </div>
            <table class="history-table">
                <tr>
                    <th>History ID</th>
                    <th>Text Content</th>
                    <th>Wrong Content</th>
                    <th>Type</th>
                    <th>Upload Date</th>
                    <th>Action</th>
                </tr>
                <% 
                    List<History> histories = (List<History>) request.getAttribute("histories");
                    if (histories != null) {
                        for (History history : histories) {
                %>
                <tr>
                    <td><%= history.getHistoryID() %></td>
                    <td><%= history.getTextContent().length() > 100 ? history.getTextContent().substring(0, 100) + "..." : history.getTextContent() %></td>
                    <td><%= history.getWrongContent().length() > 100 ? history.getWrongContent().substring(0, 100) + "..." : history.getWrongContent() %></td>
                    <td><%= history.getType() %></td>
                    <td><%= history.getUploadDate() %></td>
                    <td><button class="btn" onclick="showDetails(<%= history.getHistoryID() %>)">Details</button></td>
                </tr>
                <% 
                        }
                    } else { 
                %>
                <tr>
                    <td colspan="7">No history records found.</td>
                </tr>
                <% } %>
            </table>
            <div class="pagination">
                <%
                    int currentPage = (int) request.getAttribute("currentPage");
                    int noOfPages = (int) request.getAttribute("noOfPages");
                    int startPage = Math.max(1, currentPage - 2); // Display 2 pages before current page
                    int endPage = Math.min(noOfPages, currentPage + 2); // Display 2 pages after current page
                    String searchText = request.getParameter("searchText") != null ? request.getParameter("searchText") : "";
                    String startDate = request.getParameter("startDate") != null ? request.getParameter("startDate") : "";
                    String endDate = request.getParameter("endDate") != null ? request.getParameter("endDate") : "";
                %>

                <% if (currentPage > 1) { %>
                <a href="history?page=<%= currentPage - 1 %>&searchText=<%= searchText %>&startDate=<%= startDate %>&endDate=<%= endDate %>">&lt; Prev</a>
                <% } %>

                <% for (int i = startPage; i <= endPage; i++) { %>
                <% if (i == currentPage) { %>
                <a class="active" href="history?page=<%= i %>&searchText=<%= searchText %>&startDate=<%= startDate %>&endDate=<%= endDate %>"><%= i %></a>
                <% } else { %>
                <a href="history?page=<%= i %>&searchText=<%= searchText %>&startDate=<%= startDate %>&endDate=<%= endDate %>"><%= i %></a>
                <% } %>
                <% } %>

                <% if (currentPage < noOfPages) { %>
                <a href="history?page=<%= currentPage + 1 %>&searchText=<%= searchText %>&startDate=<%= startDate %>&endDate=<%= endDate %>">Next &gt;</a>
                <% } %>
            </div>

        </div>
        <div id="historyModal" class="modal">
            <div class="modal-content">
                <span class="close" onclick="closeModal()">&times;</span>
                <h2>History Details</h2>
                <p><strong>Text Content:</strong> <span id="modalTextContent"></span></p>
                <p><strong>Wrong Content:</strong> <span id="modalWrongContent"></span></p>
                <p><strong>Type:</strong> <span id="modalType"></span></p>
                <p><strong>Upload Date:</strong> <span id="modalUploadDate"></span></p>
            </div>
        </div>
        <br>
        <jsp:include page="footer.jsp"></jsp:include>
    </body>
</html>
