<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="robots" content="index, follow, max-image-preview:large, max-snippet:-1, max-video-preview:-1">
        <title>Grammar and Spell Check</title>
        <link rel="stylesheet" href="./css/style.css" type="text/css" media="all">
        <link rel="stylesheet" href="./css/main.css" type="text/css" media="all">
        <style>
            .toolbar {
                display: flex;
                justify-content: space-between;
                background-color: #555;
                padding: 10px;
                color: white;
            }
            .toolbar div {
                cursor: pointer;
            }
            .copy-button {
                display: flex;
                justify-content: flex-end;
                margin-top: 10px;
            }
            .copy-button button {
                background-color: #4CAF50;
                color: white;
                border: none;
                padding: 5px 10px;
                cursor: pointer;
            }
            .copy-button button:hover {
                background-color: #45a049;
            }
            .highlighted-text {
                background-color: #f8d7da;
                padding: 5px;
                border: 1px solid #f5c6cb;
                color: #721c24;
                margin-top: 10px;
                white-space: pre-wrap;
            }
            .custom-file-input {
                display: none;
            }
            .custom-file-label {
                display: inline-block;
                margin-right: 10px;
                padding: 5px 10px;
                background-color: #fff;
                color: #000;
                border: 1px solid #ccc;
                border-radius: 5px;
                cursor: pointer;
                font-size: 12px;
            }
            .custom-file-label:hover {
                background-color: #f0f0f0;
            }
            .custom-file-name {
                display: inline-block;
                padding: 5px 10px;
                font-size: 12px;
            }
        </style>
    </head>
    <body>
        <jsp:include page="head.jsp"></jsp:include>

            <div id="content" class="site-content">
                <div class="cm-container">
                    <div class="inner-page-wrapper">
                        <div id="primary" class="content-area">
                            <main id="main" class="site-main">
                                <div class="cm_archive_page">
                                    <div class="breadcrumb yoast-breadcrumb">
                                        <span>
                                            <span><a href="home.jsp">Home page</a></span>
                                            » <span class="breadcrumb_last" aria-current="page"><strong>Grammar and Spell Check</strong></span>
                                        </span>
                                    </div>

                                    <div class="toolbar">
                                        <div id="copy"><i class="fas fa-copy"></i>COPY</div>
                                        <div id="paste"><i class="fas fa-paste"></i>PASTE</div>
                                        <div id="clear"><i class="fas fa-eraser"></i>CLEAR</div>
                                        <div id="paraphrasing"><i class="fas fa-sync-alt"></i>PARAPHRASING</div>
                                        <div id="paste-sample"><i class="fas fa-flask"></i>PASTE SAMPLE TEXT</div>
                                    </div>

                                    <form id="grammarForm" action="checkGrammar" method="post">
                                        <div class="container-check-spell">
                                            <section class="box-check-spell box-section">
                                                <div class="text-check-spell">Check Grammar here</div>
                                                <div class="content-box-check-spell">
                                                    <textarea name="input-text" id="input-text" cols="30" rows="10" class="textarea-check-spell" placeholder="Enter the text to be checked">${param['input-text']}</textarea>
                                                <div class="highlighted-text" id="highlighted-input" style="display: none;">${requestScope.input}</div>
                                                <div class="fix-spell" id="error">
                                                    <c:choose>
                                                        <c:when test="${not empty requestScope.error}">
                                                            ${requestScope.error}
                                                        </c:when>
                                                    </c:choose>
                                                </div>
                                            </div>
                                            <c:choose>
                                                <c:when test="${loginedUser != null}">
                                                    <c:choose>
                                                        <c:when test="${loginedUser.isPayfee == 1}">
                                                            <label class="custom-file-label" for="file-input">Choose file: <span class="custom-file-name" id="file-name">No file chosen</span></label>
                                                            <input type="file" id="file-input" name="file-input" accept=".txt" class="custom-file-input">
                                                            <button type="button" onclick="uploadFile()">Upload .txt File</button>
                                                        </c:when>
                                                        <c:when test="${loginedUser.isPayfee == 0}">
                                                            <button type="button" onclick="alert('Please upgrade your account')">Upload File .txt</button>
                                                        </c:when>
                                                    </c:choose>
                                                </c:when>
                                                <c:otherwise>
                                                    <button type="button" onclick="alert('Please log in to upload files')">Upload File .txt</button>
                                                </c:otherwise>
                                            </c:choose>

                                            <button class="button-check-spell search-submit" id="btn-check-grammar" type="submit">Check Grammar</button>
                                            <br/>
                                            <c:if test="${not empty requestScope.corrected}">
                                                <button type="button" id="btn-show-corrected" onclick="showCorrectedText()">Show Corrected Text</button>
                                            </c:if>
                                        </section>
                                    </div>
                                </form>
                                <div id="corrected-text" class="container-check-spell" style="display: none;">
                                    <section class="box-check-spell box-section">
                                        <div class="text-check-spell">Corrected Text:</div>
                                        <div class="content-box-check-spell">
                                            <div id="corrected-content">${requestScope.corrected}</div>
                                        </div>
                                        <div class="copy-button">
                                            <button onclick="copyCorrectedText()">Copy</button>
                                            <c:choose>
                                                <c:when test="${loginedUser != null}">
                                                    <c:choose>
                                                        <c:when test="${loginedUser.isPayfee == 1}">
                                                            <button onclick="downloadTextAsFile()">Download</button>
                                                        </c:when>
                                                        <c:when test="${loginedUser.isPayfee == 0}">
                                                            <button onclick="alert('Please upgrade your account')">Download</button>
                                                        </c:when>
                                                    </c:choose>
                                                </c:when>
                                                <c:otherwise>
                                                    <button onclick="alert('Please log in to download files')">Download</button>
                                                </c:otherwise>
                                            </c:choose>
                                        </div>
                                    </section>
                                </div>
                                <br/>
                            </div>
                        </main>
                    </div>
                </div>
            </div>
        </div>

        <jsp:include page="footer.jsp"></jsp:include>

            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>
            <script>
                                                        function showCorrectedText() {
                                                            var correctedTextDiv = document.getElementById('corrected-text');
                                                            correctedTextDiv.style.display = 'block';
                                                        }

                                                        function copyCorrectedText() {
                                                            var correctedContent = document.getElementById('corrected-content').innerText;
                                                            var tempInput = document.createElement("textarea");
                                                            tempInput.value = correctedContent;
                                                            document.body.appendChild(tempInput);
                                                            tempInput.select();
                                                            document.execCommand("copy");
                                                            document.body.removeChild(tempInput);
                                                            alert("Corrected text copied to clipboard");
                                                        }

                                                        // Toolbar functionality
                                                        document.getElementById('copy').addEventListener('click', function () {
                                                            var text = document.getElementById('input-text').value;
                                                            navigator.clipboard.writeText(text).then(function () {
                                                                alert('Text copied to clipboard');
                                                            });
                                                        });

                                                        document.getElementById('paste').addEventListener('click', function () {
                                                            navigator.clipboard.readText().then(function (text) {
                                                                document.getElementById('input-text').value = text;
                                                            });
                                                        });

                                                        document.getElementById('clear').addEventListener('click', function () {
                                                            document.getElementById('input-text').value = '';
                                                        });

                                                        document.getElementById('paraphrasing').addEventListener('click', function () {
                                                            alert('Paraphrasing clicked');
                                                        });

                                                        document.getElementById('paste-sample').addEventListener('click', function () {
                                                            var sampleText = "The cat eat the food quickly yesterday but now he don't want to eat anymore. She say that the food is too cold and doesn't likes it.";
                                                            document.getElementById('input-text').value = sampleText;
                                                        });

                                                        // Show highlighted input text if there are errors
            <c:if test="${not empty requestScope.error}">
                                                        document.getElementById('highlighted-input').style.display = 'block';
            </c:if>

                                                        // Validate forms before submission (optional)
                                                        document.getElementById('grammarForm').addEventListener('submit', function (event) {
                                                            if (!validateGrammarForm()) {
                                                                event.preventDefault();
                                                            }
                                                        });

                                                        function validateGrammarForm() {
                                                            // Perform validation if needed
                                                            return true; // Return true if valid, false otherwise
                                                        }

                                                        function uploadFile() {
                                                            const fileInput = document.getElementById('file-input');
                                                            const file = fileInput.files[0];

                                                            if (!file) {
                                                                alert('Please select a file.');
                                                                return;
                                                            }

                                                            const reader = new FileReader();
                                                            reader.onload = function (event) {
                                                                const content = event.target.result;
                                                                document.getElementById('input-text').value = content;
                                                            };
                                                            reader.readAsText(file);
                                                        }
                                                        function downloadTextAsFile() {
                                                            const correctedContent = document.getElementById('corrected-content').innerText;
                                                            const blob = new Blob([correctedContent], {type: 'text/plain'});
                                                            const a = document.createElement('a');
                                                            a.style.display = 'none';
                                                            const url = window.URL.createObjectURL(blob);
                                                            a.href = url;
                                                            a.download = 'corrected-text.txt';
                                                            document.body.appendChild(a);
                                                            a.click();
                                                            window.URL.revokeObjectURL(url);
                                                            document.body.removeChild(a);
                                                        }

                                                        document.getElementById('file-input').addEventListener('change', function (event) {
                                                            const fileInput = event.target;
                                                            const fileName = fileInput.files.length > 0 ? fileInput.files[0].name : 'No file chosen';
                                                            document.getElementById('file-name').textContent = fileName;
                                                        });

        </script>
    </body>
</html>
