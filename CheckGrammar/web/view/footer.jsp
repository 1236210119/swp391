<%-- 
    Document   : footer
    Created on : May 19, 2024, 11:28:12 PM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <script
            type="text/javascript"
            async=""
            src="https://www.googletagmanager.com/gtag/js?id=G-1JXPBD65W7&amp;l=dataLayer&amp;cx=c"
        ></script>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta
            name="robots"
            content="index, follow, max-image-preview:large, max-snippet:-1, max-video-preview:-1"
            />
        <title>Header</title>
        <link
            data-minify="1"
            rel="stylesheet"
            id="cream-magazine-style-css"
            href="./css/Home/style.css"
            type="text/css"
            media="all"
            />
        <link
            data-minify="1"
            rel="stylesheet"
            id="cream-magazine-main-css"
            href="./css/Home/main.css"
            type="text/css"
            media="all"
            />
    </head>

    <body>
        <footer class="footer">
            <div class="footer_inner">
                <div class="cm-container">
                    <div class="row footer-widget-container">
                        <div class="cm-col-lg-4 cm-col-12">
                            <div class="blocks">
                                <div id="text-2" class="widget widget_text">
                                    <div class="widget-title"><h2>Contact Info</h2></div>
                                    <div class="textwidget">
                                        <p>FPT University</p>
                                        <p>
                                            Location:
                                            <a href="">Here</a>
                                        </p>
                                        <p>Phone: 0396610870</p>
                                        <p>Email: checkgrammar@gmail.com</p>
                                        <p>Operating hours: 8am - 5pm (Monday - Saturday)</p>
                                    </div>
                                </div>
                                <div id="custom_html-3" class="widget_text widget widget_custom_html">
                                    <div class="textwidget custom-html-widget">
                                        <script
                                            defer=""
                                            src="https://web1s.com/site-v5.js?id=7sbGLeUzEe"
                                            data-rocket-status="executed"
                                        ></script>
                                        <div data-loading="false"></div>
                                    </div>
                                </div>
                            </div>
                            <!-- .blocks -->
                        </div>
                        <!-- .cm-col-->
                        <div class="cm-col-lg-4 cm-col-12">
                            <div class="blocks">
                                <div id="custom_html-2" class="widget_text widget widget_custom_html">
                                    <div class="widget-title"><h2>About Us</h2></div>
                                    <div class="textwidget custom-html-widget">
                                        <li><a href="">Introduction</a></li>
                                        <li><a href="">Expert Advice</a></li>
                                        <li><a href="">Content Responsibility</a></li>
                                        <li><a href="">Contact</a></li>
                                        <li><a href="">Terms & Conditions</a></li>
                                        <li><a href="">Privacy Policy</a></li>
                                        <li><a href="">Sponsorship</a></li>
                                    </div>
                                </div>
                            </div>
                            <!-- .blocks -->
                        </div>
                        <!-- .cm-col-->
                        <div class="cm-col-lg-4 cm-col-12">
                            <div class="blocks">
                                <div id="text-3" class="widget widget_text">
                                    <div class="textwidget">
                                        <p>
                                            <span
                                                data-sheets-root="1"
                                                data-sheets-value='{"1":2,"2":"The Grammar check - A comprehensive website offering a variety of useful information from folk poetry, valuable literary analysis to humorous jokes and folk tips."}'
                                                data-sheets-userformat='{"2":1391425,"3":{"1":0},"9":0,"11":0,"12":0,"14":{"1":2,"2":0},"15":"Arial","16":10,"19":0,"21":0,"23":1}'
                                                data-sheets-textstyleruns='{"1":0,"2":{"2":{"1":2,"2":1136076},"9":1}}{"1":17}'
                                                data-sheets-hyperlinkruns='{"1":0,"2":""}{"1":17}'
                                                >
                                                <a
                                                    class="in-cell-link"
                                                    href=""
                                                    target="_blank"
                                                    rel="noopener"
                                                    >The Grammar check</a>
                                                – A comprehensive website offering a variety of useful information from folk poetry, valuable literary analysis to humorous jokes and folk tips.
                                            </span>
                                        </p>
                                    </div>
                                </div>
                                <div id="cream-magazine-social-widget-2" class="widget social_widget_style_1">
                                    <div class="widget-title">
                                        <h2>Follow Us on Social Media</h2>
                                    </div>
                                    <div class="widget-contents">
                                        <ul>
                                            <li class="fb">
                                                <a href="" target="_blank">
                                                    <i class="fa fa-facebook-f"></i><span>Like</span>
                                                </a>
                                            </li>
                                            <li class="tw">
                                                <a href="" target="_blank">
                                                    <i class="fa fa-twitter"></i><span>Follow</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="copyright_section">
                        <div class="row">
                            <div class="cm-col-lg-5 cm-col-md-6 cm-col-12">
                                <div class="footer_nav"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </body>
</html>
