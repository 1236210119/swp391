<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="en">
    <body>
        <jsp:include page="head.jsp"></jsp:include>  
        <jsp:include page="checkSpelling.jsp"></jsp:include> 
        <jsp:include page="footer.jsp"></jsp:include>     
    </body>
</html>