<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-alpha1/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="./css/infom.css" type="text/css" media="all">
    <title>JSP Page</title>
    <style>
        .approved-status {
            color: green;
        }
        .pending-status {
            color: orange;
        }
        .pagination a {
            margin: 0 5px;
            padding: 5px 10px;
            text-decoration: none;
            color: #000;
        }
        .pagination a.active {
            font-weight: bold;
            color: #fff;
            background-color: #007bff;
            border-radius: 5px;
        }
    </style>
</head>
<body>
    <jsp:include page="head.jsp"></jsp:include>
    <div class="container">
        <br>
        <h1 style="text-align: center">Application Processing Information</h1>

        <!-- Search Form -->
        <form action="viewRequestStatus" method="get" class="form-inline">
            <div class="form-group">
                <label for="status">Status:</label>
                <select name="status" class="form-control">
                    <option value="">All</option>
                    <option value="Processed">Processed</option>
                    <option value="Waiting for response">Waiting for response</option>
                </select>
            </div>
            <div class="form-group">
                <label for="startDate">Create Date From:</label>
                <input type="date" name="startDate" class="form-control">
            </div>
            <div class="form-group">
                <label for="endDate">Create Date To:</label>
                <input type="date" name="endDate" class="form-control">
            </div>
            <div class="form-group">
                <label for="purpose">Purpose:</label>
                <input type="text" name="purpose" class="form-control" placeholder="Search Purpose">
            </div>
            <button type="submit" class="btn btn-primary">Search</button>
        </form>

        <br>
        <table class="table">
            <thead>
                <tr>
                    <c:if test="${isAdmin}">
                        <th>USER ID</th>
                    </c:if>
                    <th>TYPE</th>
                    <th>PURPOSE</th>
                    <th>CREATEDATE</th>
                    <th>PROCESSNOTE</th>
                    <th>FILE</th>
                    <th>STATUS</th>
                    <th>DATE</th>
                    <c:if test="${isAdmin}">
                        <th>Action</th>
                    </c:if>
                </tr>
            </thead>
            <tbody>
                <c:forEach var="requestStatus" items="${listRequest}">
                    <tr>
                        <c:if test="${isAdmin}">
                            <td>${requestStatus.userID}</td>
                        </c:if>
                        <td class="nowrap">${requestStatus.type}</td>
                        <td>${requestStatus.purpose}</td>
                        <td>${requestStatus.createDate}</td>
                        <td>${requestStatus.processNote}</td>
                        <td>
                            <c:if test="${not empty requestStatus.file}">
                                <a href="DownloadServlet?fileName=${requestStatus.file}">
                                    <button type="button" class="btn btn-primary">Download</button>
                                </a>
                            </c:if>
                        </td>
                        <td class="${requestStatus.status == 'Processed' ? 'approved-status' : requestStatus.status == 'Waiting for response' ? 'pending-status' : ''}">
                            ${requestStatus.status}
                        </td>
                        <td>${requestStatus.lastUpdate}</td>
                        <td>
                            <c:if test="${isAdmin}">
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#feedbackModal${requestStatus.requestID}">
                                    Feedback
                                </button>
                            </c:if>
                        </td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>

        <div class="pagination">
            <%
                int currentPage = (int) request.getAttribute("currentPage");
                int noOfPages = (int) request.getAttribute("noOfPages");
                int startPage = Math.max(1, currentPage - 2);
                int endPage = Math.min(noOfPages, currentPage + 2);
                String status = request.getParameter("status") != null ? request.getParameter("status") : "";
                String startDate = request.getParameter("startDate") != null ? request.getParameter("startDate") : "";
                String endDate = request.getParameter("endDate") != null ? request.getParameter("endDate") : "";
                String purpose = request.getParameter("purpose") != null ? request.getParameter("purpose") : "";
            %>

            <% if (currentPage > 1) { %>
            <a href="viewRequestStatus?page=<%= currentPage - 1 %>&status=<%= status %>&startDate=<%= startDate %>&endDate=<%= endDate %>&purpose=<%= purpose %>">&lt; Prev</a>
            <% } %>

            <% for (int i = startPage; i <= endPage; i++) { %>
            <% if (i == currentPage) { %>
            <a class="active" href="viewRequestStatus?page=<%= i %>&status=<%= status %>&startDate=<%= startDate %>&endDate=<%= endDate %>&purpose=<%= purpose %>"><%= i %></a>
            <% } else { %>
            <a href="viewRequestStatus?page=<%= i %>&status=<%= status %>&startDate=<%= startDate %>&endDate=<%= endDate %>&purpose=<%= purpose %>"><%= i %></a>
            <% } %>
            <% } %>

            <% if (currentPage < noOfPages) { %>
            <a href="viewRequestStatus?page=<%= currentPage + 1 %>&status=<%= status %>&startDate=<%= startDate %>&endDate=<%= endDate %>&purpose=<%= purpose %>">Next &gt;</a>
            <% } %>
        </div>
    </div>

    <!-- Modal feedback -->
    <c:forEach var="requestStatus" items="${listRequest}">
        <div class="modal fade" id="feedbackModal${requestStatus.requestID}" tabindex="-1" role="dialog" aria-labelledby="feedbackModalLabel${requestStatus.requestID}" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="feedbackModalLabel${requestStatus.requestID}">Feedback for Request ID: ${requestStatus.requestID}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="viewRequestStatus" method="post">
                        <div class="modal-body">
                            <input type="hidden" name="idRequest" value="${requestStatus.requestID}">
                            <div class="form-group">
                                <label for="feedbackContent">Feedback Content:</label>
                                <textarea class="form-control" id="feedbackContent" name="feedbackContent" rows="3" required></textarea>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </c:forEach>

    <jsp:include page="footer.jsp"></jsp:include>

    <!-- Scripts for Bootstrap -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-alpha1/dist/js/bootstrap.bundle.min.js"></script>
</body>
</html>
