<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="robots" content="index, follow, max-image-preview:large, max-snippet:-1, max-video-preview:-1" />
        <title>Spell Check</title>
        <link rel="stylesheet" href="./css/style.css" type="text/css" media="all" />
        <link rel="stylesheet" href="./css/main.css" type="text/css" media="all" />
        <style>
            .loader {
                display: none;
            }
            .statistics {
                border: 2px solid #ddd;
                padding: 20px;
                margin-top: 5px;
                background-color: #f9f9f9;
                box-shadow: 2px 2px 8px rgba(0, 0, 0, 0.1);
                border-radius: 5px;
                display: flex;
                flex-wrap: wrap;
                justify-content: space-between;
            }
            .statistics p {
                margin: 10px 0;
                flex-basis: calc(95% - 20px);
            }
            .copy-button {
                display: flex;
                justify-content: flex-end;
                margin-top: 10px;
            }
            .copy-button button {
                background-color: #4CAF50;
                color: white;
                border: none;
                padding: 5px 10px;
                cursor: pointer;
            }
            .copy-button button:hover {
                background-color: #45a049;
            }
            #corrected-text-container {
                display: none;
            }
            .custom-file-input {
                display: none;
            }
            .custom-file-label {
                display: inline-block;
                margin-right: 10px;
                padding: 5px 10px;
                background-color: #fff;
                color: #000;
                border: 1px solid #ccc;
                border-radius: 5px;
                cursor: pointer;
                font-size: 12px;
            }
            .custom-file-label:hover {
                background-color: #f0f0f0;
            }
            .custom-file-name {
                display: inline-block;
                padding: 5px 10px;
                font-size: 12px;
            }
        </style>
    </head>
    <body>
        <div id="content" class="site-content">
            <div class="cm-container">
                <div class="inner-page-wrapper">
                    <div id="primary" class="content-area">
                        <main id="main" class="site-main">
                            <div class="cm_archive_page">
                                <div class="breadcrumb yoast-breadcrumb">
                                    <span>
                                        <span><a href="home.jsp">Home page</a></span>
                                        » <span class="breadcrumb_last" aria-current="page"><strong>Spell Check</strong></span>
                                    </span>
                                </div>
                                <div class="toolbar-container">
                                    <jsp:include page="toolbar.jsp"></jsp:include>
                                    </div>
                                    <div class="container-check-spell">
                                        <section class="box-check-spell box-section">
                                            <div class="text-check-spell">Check spelling here</div>
                                            <div class="content-box-check-spell">  
                                                <textarea name="input-text" id="input-text" cols="30" rows="10" class="textarea-check-spell" placeholder="Enter the text to be checked">${param['input-text']}</textarea>
                                            <div id="highlighted-input" class="fix-spell" style="display: none;"></div>
                                            <div class="fix-spell" id="spell-check-results"></div>
                                            <div class="statistics">
                                                <p>Word count: <span id="word-count"></span></p>
                                                <p>Paragraph count: <span id="paragraph-count"></span></p>
                                                <p>Error count: <span id="error-count"></span></p>
                                                <p>Error density: <span id="error-density"></span>%</p>
                                            </div>
                                        </div>
                                        <c:choose>
                                            <c:when test="${loginedUser != null}">
                                                <c:choose>
                                                    <c:when test="${loginedUser.isPayfee == 1}">
                                                        <label class="custom-file-label" for="file-input">Choose file: <span class="custom-file-name" id="file-name">No file chosen</span></label>
                                                        <input type="file" id="file-input" name="file-input" accept=".txt" class="custom-file-input">
                                                        <button type="button" onclick="uploadFile()">Upload File .txt</button>
                                                    </c:when>
                                                    <c:when test="${loginedUser.isPayfee == 0}">
                                                        <button type="button" onclick="alert('Please upgrade your account')">Upload File .txt</button>
                                                    </c:when>
                                                </c:choose>
                                            </c:when>
                                            <c:otherwise>
                                                <button type="button" onclick="alert('Please log in to upload files')">Upload File .txt</button>
                                            </c:otherwise>
                                        </c:choose>

                                        <button class="button-check-spell search-submit" id="btn-check">Check</button>
                                        <div class="loader" id="loading">
                                            <li class="ball"></li>
                                            <li class="ball"></li>
                                            <li class="ball"></li>
                                        </div>
                                        <br/>
                                        <div id="show-corrected-text-button" style="display: none;">
                                            <button type="button" onclick="showCorrectedText()">Show Corrected Text</button>
                                        </div>
                                        <div id="corrected-text-container" class="container-check-spell" style="display: none;">
                                            <section class="box-check-spell box-section">
                                                <div class="text-check-spell">Corrected Text:</div>
                                                <div class="content-box-check-spell">
                                                    <div id="corrected-content">${requestScope.corrected}</div>
                                                </div>
                                                <div class="copy-button">
                                                    <button onclick="copyCorrectedText()">Copy</button>
                                                    <c:choose>
                                                        <c:when test="${loginedUser != null}">
                                                            <c:choose>
                                                                <c:when test="${loginedUser.isPayfee == 1}">
                                                                    <button onclick="downloadTextAsFile()">Download</button>
                                                                </c:when>
                                                                <c:when test="${loginedUser.isPayfee == 0}">
                                                                    <button onclick="alert('Please upgrade your account')">Download</button>
                                                                </c:when>
                                                            </c:choose>
                                                        </c:when>
                                                        <c:otherwise>
                                                            <button onclick="alert('Please log in to download files')">Download</button>
                                                        </c:otherwise>
                                                    </c:choose>
                                                </div>
                                            </section>
                                        </div>
                                    </section>
                                </div>
                            </div>
                        </main>
                    </div>        
                </div>
            </div>
        </div>
        <br/>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>
        <script>
                                                                document.getElementById('btn-check').addEventListener('click', function (event) {
                                                                    event.preventDefault(); // Prevent default form submission

                                                                    // Get the value from the textarea
                                                                    const inputText = document.getElementById('input-text').value;
                                                                    const resultDiv = document.getElementById('spell-check-results');
                                                                    const loadingDiv = document.getElementById('loading');
                                                                    const highlightedInput = document.getElementById('highlighted-input');
                                                                    const showCorrectedTextButton = document.getElementById('show-corrected-text-button');
                                                                    const correctedTextContainer = document.getElementById('corrected-text-container');
                                                                    const correctedContentDiv = document.getElementById('corrected-content');

                                                                    // Show loader
                                                                    loadingDiv.style.display = 'block';

                                                                    // Send POST request to servlet
                                                                    fetch('checkSpell', {
                                                                        method: 'POST',
                                                                        headers: {
                                                                            'Content-Type': 'application/x-www-form-urlencoded'
                                                                        },
                                                                        body: new URLSearchParams({
                                                                            'input-text': inputText
                                                                        })
                                                                    })
                                                                            .then(response => {
                                                                                if (!response.ok) {
                                                                                    throw new Error('Network response was not ok');
                                                                                }
                                                                                return response.json();
                                                                            })
                                                                            .then(data => {
                                                                                if (data.error) {
                                                                                    // Show popup notification
                                                                                    alert(data.error);
                                                                                } else {
                                                                                    console.log(data); // Log the data from the servlet

                                                                                    // Update the textarea content with the highlighted text
                                                                                    highlightedInput.innerHTML = data.inputText;
                                                                                    highlightedInput.style.display = 'block';

                                                                                    // Update the spell check results
                                                                                    resultDiv.innerHTML = data.spellCheckResults;

                                                                                    // Update statistics
                                                                                    document.getElementById('word-count').textContent = data.wordCount;
                                                                                    document.getElementById('paragraph-count').textContent = data.paragraphCount;
                                                                                    document.getElementById('error-count').textContent = data.errorCount;
                                                                                    document.getElementById('error-density').textContent = data.errorDensity.toFixed(2);

                                                                                    // Show corrected text button and container
                                                                                    showCorrectedTextButton.style.display = 'block';
                                                                                    correctedContentDiv.innerHTML = data.correctedText;
                                                                                }
                                                                            })
                                                                            .catch(error => {
                                                                                console.error('Error:', error);
                                                                                resultDiv.innerHTML = `<p class="noti">An error occurred. Please try again.</p>`;
                                                                            })
                                                                            .finally(() => {
                                                                                // Hide loader
                                                                                loadingDiv.style.display = 'none';
                                                                            });
                                                                });

                                                                function showCorrectedText() {
                                                                    const correctedTextContainer = document.getElementById('corrected-text-container');
                                                                    correctedTextContainer.style.display = 'block';
                                                                }

                                                                function copyCorrectedText() {
                                                                    const correctedContent = document.getElementById('corrected-content').innerText;
                                                                    const tempInput = document.createElement("textarea");
                                                                    tempInput.value = correctedContent;
                                                                    document.body.appendChild(tempInput);
                                                                    tempInput.select();
                                                                    document.execCommand("copy");
                                                                    document.body.removeChild(tempInput);
                                                                    alert("Corrected text copied to clipboard");
                                                                }

                                                                function downloadTextAsFile() {
                                                                    const correctedContent = document.getElementById('corrected-content').innerText;
                                                                    const blob = new Blob([correctedContent], {type: 'text/plain'});
                                                                    const a = document.createElement('a');
                                                                    a.style.display = 'none';
                                                                    const url = window.URL.createObjectURL(blob);
                                                                    a.href = url;
                                                                    a.download = 'corrected-text.txt';
                                                                    document.body.appendChild(a);
                                                                    a.click();
                                                                    window.URL.revokeObjectURL(url);
                                                                    document.body.removeChild(a);
                                                                }

                                                                document.getElementById('file-input').addEventListener('change', function (event) {
                                                                    const fileInput = event.target;
                                                                    const fileName = fileInput.files.length > 0 ? fileInput.files[0].name : 'No file chosen';
                                                                    document.getElementById('file-name').textContent = fileName;
                                                                });

                                                                function uploadFile() {
                                                                    const fileInput = document.getElementById('file-input');
                                                                    const file = fileInput.files[0];

                                                                    if (!file) {
                                                                        alert('Please select a file.');
                                                                        return;
                                                                    }

                                                                    const reader = new FileReader();
                                                                    reader.onload = function (event) {
                                                                        const content = event.target.result;
                                                                        document.getElementById('input-text').value = content;
                                                                    };
                                                                    reader.readAsText(file);
                                                                }
        </script>
    </body>
</html>
