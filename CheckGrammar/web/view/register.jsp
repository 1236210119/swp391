<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="en" data-lt-script-installed="true" class="logged-out windows" data-country="VN" data-adjust-appearance="" data-premium="false" data-user="false" data-platform="windows" data-platform-apple="false">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <title>Sign Up for Free - LanguageTool</title>

        <link rel="stylesheet" href="./css/style.css" />
        <link href="./css/custom-style.css" rel="stylesheet" fetchpriority="high" />
        <link href="/fonts/Source-Sans-Pro-latin-win.woff2" rel="preload" as="font" type="font/woff2" crossorigin="" fetchpriority="high" />
        <link href="/fonts/Source-Sans-Pro-latin-bold-win.woff2" rel="preload" as="font" type="font/woff2" crossorigin="" fetchpriority="high" />
        <link href="/fonts/Source-Sans-Pro-latin-semi-bold-win.woff2" rel="preload" as="font" type="font/woff2" crossorigin="" fetchpriority="high" />
        <script src="./js/mainfest.js" defer="" fetchpriority="high"></script>
        <script src="./js/react.js" defer="" fetchpriority="high"></script>
        <script src="./js/custom-app.js" defer="" fetchpriority="high"></script>  
        <script src="./js/validation.js" defer></script>
    </head>
    <body class="body" data-force-appearance="default" cz-shortcut-listen="true">
        <div class="page">
            <header id="primary-website-header" class="header header--white | flex" itemtype="" itemscope="">
                <div class="header__inner | flex">
                    <a href="/" class="logo logo--black" title="Check your text quickly and easily. Grammar, punctuation, style, and spelling." itemprop="url">
                        <span class="header__inner__logo-name name notranslate" itemprop="name">Grammar<strong>Tool</strong></span>
                        <span class="header__inner__logo-claim claim">Your writing assistant, Spelling check, Plagiarism check</span>
                    </a>
                </div>
            </header>
            <section class="content">
                <div id="content-register" class="content--register">
                    <h1 class="headline headline--1">Create Your Account and Unlock More Features</h1>
                    <p class="paragraph--4">
                        You are just one step away from improving your writing. Already have an account?
                        <a href="login" class="link link--primary">Log in here</a>
                    </p>
                    <form class="form form--account" method="POST" action="register">
                        <c:if test="${not empty errorMessageUsername}">
                            <span style="color: red">${errorMessageUsername}</span>
                        </c:if>
                        <label class="label label--input">
                            <input type="text" name="username" maxlength="191" autocomplete="username" value="${empty param.username ? (not empty requestScope.usernameValue ? requestScope.usernameValue : '') : param.username}" aria-label="User Name" required="" autofocus="" class="empty" />
                            <span>User Name</span>
                        </label>
                        <label class="label label--input">
                            <input type="password" name="password" value="" autocomplete="new-password" aria-label="Password" required="" class="empty" aria-autocomplete="list" />
                            <span>Password</span>
                        </label>
                        <label class="label label--input">
                            <input type="password" name="password_confirmation" value="" autocomplete="new-password" aria-label="Confirm password" required="" class="empty" />
                            <span>Confirm password</span>
                        </label>
                        <label class="label label--input">
                            <input type="text" name="name" maxlength="191" autocomplete="name" value="${empty param.name ? (not empty requestScope.nameValue ? requestScope.nameValue : '') : param.name}" aria-label="Your name" required="" autofocus="" class="empty" />
                            <span>Full Name</span>
                        </label>
                        <p class="err"><c:out value="${errorMessageEmail1}" /></p>
                        <p class="err"><c:out value="${errorMessageEmail2}" /></p>
                        <label class="label label--input">
                            <input type="email" name="email" maxlength="191" value="${empty param.email ? (not empty requestScope.emailValue ? requestScope.emailValue : '') : param.email}" autocomplete="email" aria-label="E-mail address" required="" class="empty" />
                            <span>E-mail address</span>
                        </label>
                        <label class="label label--input">
                            <input type="text" name="address" value="${empty param.address ? (not empty requestScope.addressValue ? requestScope.addressValue : '') : param.address}" autocomplete="address" aria-label="Address" required="" class="empty" />
                            <span>Address</span>
                        </label>
                        <p class="err"><c:out value="${errorMessagePhone}" /></p>
                        <label class="label label--input">
                            <input type="text" name="phone" value="${empty param.phone ? (not empty requestScope.phoneValue ? requestScope.phoneValue : '') : param.phone}" autocomplete="phone" aria-label="Phone" required="" class="empty" />
                            <span>Phone Number</span>
                        </label>
                        <button class="btn btn--primary btn--full-size btn--large" type="submit">Sign up</button>
                    </form>
                </div>
                <div id="errorPopup" class="popup">
                    <span class="popup-close" onclick="closePopup()">&times;</span>
                    <p id="errorMessage"></p>
                </div>

                <style>
                    .popup {
                        display: none;
                        position: fixed;
                        left: 50%;
                        top: 50%;
                        transform: translate(-50%, -50%);
                        padding: 20px;
                        background-color: #f44336;
                        color: white;
                        border: 1px solid #888;
                        z-index: 1000;
                    }
                    .popup-close {
                        float: right;
                        cursor: pointer;
                    }
                    .err {
                        color: red;
                    }
                </style>

                <script>
                    function showError(message) {
                        alert(message);
                    }

                    window.onload = function () {
                        const errorMessage = '<%= request.getAttribute("errorMessage") %>';
                        if (errorMessage) {
                            showError(errorMessage);
                        }
                    }
                </script>
            </body>
</html>
