<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-alpha1/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="./css/response.css" type="text/css" media="all" />
    <title>User Feedback</title>
    <style>
        .custom-file-input {
            position: relative;
            overflow: hidden;
        }
        .custom-file-input input[type="file"] {
            position: absolute;
            opacity: 0;
            right: 0;
            top: 0;
            cursor: pointer;
        }
        .custom-file-label {
            display: inline-block;
            padding: 0.375rem 0.75rem;
            line-height: 1.5;
            color: #495057;
            background-color: #fff;
            border: 1px solid #ced4da;
            border-radius: 0.25rem;
            width: 100%;
        }
    </style>
</head>
<body>
    <jsp:include page="head.jsp"></jsp:include>
        <h1></h1>
        <div class="container">
            <h1 style="text-align: center">Contact us</h1>
            <form id="feedbackForm" method="post" action="requestStatus" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="errorType">Error Type:</label>
                    <select id="errorType" name="errorType" required>
                        <option value="">Select error type</option>
                        <option value="Payment error">Payment Error</option>
                        <option value="Error checking grammar">Error Checking Grammar</option>
                        <option value="Error checking spelling">Error Checking Spelling</option>
                        <option value="Other">Other...</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="description">Description:</label>
                    <textarea id="description" name="description" rows="5" required></textarea>
                </div>
                <div class="form-group">
                    <label for="fileUpload">Upload File:</label>
                    <div class="custom-file-input">
                        <label class="custom-file-label" for="fileUpload">Choose file</label>
                        <input type="file" id="fileUpload" name="fileUpload" class="form-control" onchange="updateFileName()">
                    </div>
                </div>
                <br/>
                <button type="submit" class="btn btn-primary">Send Feedback</button>
                <h1></h1>
            </form>
        </div>
    <jsp:include page="footer.jsp"></jsp:include>
    
    <script>
        function updateFileName() {
            var input = document.getElementById('fileUpload');
            var label = document.querySelector('.custom-file-label');
            if (input.files.length > 0) {
                label.textContent = input.files[0].name;
            } else {
                label.textContent = "Choose file";
            }
        }
    </script>
</body>
</html>
