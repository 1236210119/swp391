<%-- 
    Document   : content
    Created on : May 19, 2024, 11:27:10 PM
    Author     : admin
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <script
            type="text/javascript"
            async=""
            src=""
        ></script>

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta
            name="robots"
            content="index, follow, max-image-preview:large, max-snippet:-1, max-video-preview:-1"
            />
        <title>Header</title>
        <link
            data-minify="1"
            rel="stylesheet"
            id="cream-magazine-style-css"
            href="./css/style.css"
            type="text/css"
            media="all"
            />
        <link
            data-minify="1"
            rel="stylesheet"
            id="cream-magazine-main-css"
            href="./css/main.css"
            type="text/css"
            media="all"
            />
        <style>
            body {
                font-family: Arial, sans-serif;
            }
            .blog-container {
                display: flex;
                flex-wrap: wrap;
                justify-content: space-between;
                margin-top: 20px;
            }
            .blog-card {
                width: 30%;
                border: 1px solid #ddd;
                border-radius: 10px;
                overflow: hidden;
                margin-bottom: 20px;
                transition: transform 0.3s;
            }
            .blog-card:hover {
                transform: translateY(-10px);
            }
            .blog-image {
                width: 100%;
                height: 200px;
                object-fit: cover;
            }
            .blog-content {
                padding: 15px;
            }
            .blog-title {
                font-size: 1.2rem;
                margin-bottom: 10px;
            }
            .blog-description {
                font-size: 1rem;
                color: #555;
            }
            .btn-read-more {
                display: inline-block;
                margin-top: 10px;
                color: #007bff;
                text-decoration: none;
            }
            .btn-read-more:hover {
                text-decoration: underline;
            }
            .loader {
                display: none;
            }
            .statistics {
                border: 2px solid #ddd;
                padding: 20px;
                margin-top: 5px;
                background-color: #f9f9f9;
                box-shadow: 2px 2px 8px rgba(0, 0, 0, 0.1);
                border-radius: 5px;
                display: flex;
                flex-wrap: wrap;

            }
            .statistics p {
                margin: 10px 0;
                flex-basis: calc(95% - 20px);
            }



            .blog-card {
                position: relative;
                margin-bottom: 20px;
            }

            .blog-content {
                position: relative;
            }

            .blog-footer {
                display: flex;
                justify-content: space-between;
                align-items: center;
            }

            .favorite {
                cursor: pointer;
            }

            .favorite-active {
                color: red; /* Color when active */
            }
            /* CSS cho thanh tìm kiếm */
            .search-container {
                display: flex;
                justify-content: center;
                margin: 20px 0;
            }

            #search-input {
                width: 100%;
                max-width: 600px;
                padding: 10px;
                border-radius: 20px;
                border: 1px solid #ccc;
                box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
                font-size: 16px;
                outline: none;
                transition: all 0.3s ease;
            }

            #search-input:focus {
                border-color: #007bff;
                box-shadow: 0 2px 8px rgba(0, 123, 255, 0.2);
            }

            /* CSS cho các thẻ blog */
            .blog-card {
                border-radius: 10px;
                overflow: hidden;
                box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
                margin-bottom: 20px;
                transition: all 0.3s ease;
            }

            .blog-card:hover {
                box-shadow: 0 4px 8px rgba(0, 0, 0, 0.2);
            }


        </style>
    </head>

    <body>
        <div id="content" class="site-content">
            <div class="cm-container">
                <div class="archive-container">
                    <div class="row">
                        <div class="cm-col-lg-12 cm-col-12">
                            <div class="content-entry">
                                <section class="list_page_iner">
                                    <div class="section-title">
                                        <!--<h1>Category: <span>Check Grammar, Spelling</span></h1>											</div> .section-title -->
                                    <div class="list_entry">
                                        <section class="post-display-grid">
                                            <div class="section_inner">
                                                <div class="row">
                                                    <c:forEach items="${requestScope.blogs}" var="item">
                                                        <div class="cm-col-lg-4 cm-col-md-6 cm-col-12">
                                                            <article id="post-12155" class="grid-post-holder post-12155 post type-post status-publish format-standard has-post-thumbnail hentry category-canh-sat-chinh-ta">
                                                                <div class="card">
                                                                    <div class="post_thumb">
                                                                        <a href="#">
                                                                            <figure class="imghover">
                                                                                <img width="800" height="450" src="${item.image}" class="attachment-cream-magazine-thumbnail-2 size-cream-magazine-thumbnail-2 wp-post-image" alt="Trãi nghiệm hay trải nghiệm mới là cách viết chính xác?" decoding="async">			</figure>
                                                                        </a>
                                                                    </div>
                                                                    <div class="card_content">
                                                                        <div class="post_title">
                                                                            <h2><a href="#">${item.name}</a></h2>
                                                                        </div>
                                                                        <div class="cm-post-meta">
                                                                            <ul class="post_meta">
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </article>
                                                        </div>
                                                    </c:forEach>
                                                </div>
                                            </div>
                                        </section>
                                        <div>
                                            <h1 style="text-align: center">Blog</h1>
                                        </div>
                                    </div>
                                    <div class="search-container">
                                        <input type="text" id="search-input" placeholder="Search by title..." onkeyup="searchBlogs()">
                                    </div>
                                    <div id="blog-container" class="blog-container">
                                        <!-- Các blog sẽ được thêm vào đây -->
                                        <c:forEach var="blog" items="${blogas}">
                                            <div class="blog-card">
                                                <img src="uploads/${blog.image}" alt="${blog.image}" class="blog-image">
                                                <div class="blog-content">
                                                    <h2 class="blog-title">${blog.title}</h2>
                                                    <div class="blog-footer">
                                                        <a href="blog-detail-user?blogID=${blog.id}" class="btn-read-more">Read more</a>
                                                        <c:if test="${check == 'check'}">
                                                            <i class="fas fa-heart favorite ${blog.favorite ? 'favorite-active' : ''}" 
                                                               data-blog-id="${blog.id}" 
                                                               data-is-favorite="${blog.favorite}" 
                                                               onclick="toggleFavorite(this)"></i>
                                                        </c:if>
                                                    </div>
                                                </div>
                                            </div>
                                        </c:forEach>
                                    </div>
                                    <% String check = (String) request.getAttribute("check"); %>
                                    <% String userId = request.getAttribute("userId") != null ? request.getAttribute("userId").toString() : "";%>
                                    <script>
                                        var check = "<%= check%>";
                                        var userId = "<%= userId%>";
                                    </script>

                                    <div id="blog-container" class="blog-container"></div>

                                    <div class="load-more-container" style="text-align: center;">
                                        <button class="load-more-btn" id="load-more-btn">Load more</button>
                                    </div>

                                    <script>
                                        function toggleFavorite(element) {
                                            var blogId = element.getAttribute("data-blog-id");
                                            var userId = element.getAttribute("data-user-id");
                                            var isFavorite = element.getAttribute("data-is-favorite") === "true";

                                            if (!userId) {
                                                alert("Please log in to favorite this blog.");
                                                return;
                                            }

                                            $.ajax({
                                                url: '/CheckGrammar/toggleFavorite?action=toggleFavorite',
                                                type: 'POST',
                                                data: {
                                                    blogId: blogId,
                                                    userId: userId,
                                                    isFavorite: isFavorite
                                                },
                                                success: function (response) {
                                                    if (response.status === 'success') {
                                                        element.classList.toggle("favorite-active");
                                                        element.setAttribute("data-is-favorite", !isFavorite);
                                                    }
                                                },
                                                error: function (error) {
                                                    console.error('Error:', error);
                                                }
                                            });
                                        }

                                        function createBlogCard(blog) {
                                            const blogCard = document.createElement('div');
                                            blogCard.classList.add('blog-card');
                                            blogCard.innerHTML =
                                                    '<img src="uploads/' + blog.image + '" alt="' + blog.image + '" class="blog-image">' +
                                                    '<div class="blog-content">' +
                                                    '<h2 class="blog-title">' + blog.title + '</h2>' +
                                                    '<div class="blog-footer">' +
                                                    '<a href="blog-detail-user?blogID=' + blog.blogID + '" class="btn-read-more">Read more</a>' +
                                                    (check === 'check' ? '<i class="fas fa-heart favorite ' + (blog.favorite ? 'favorite-active' : '') + '"' +
                                                            ' data-blog-id="' + blog.blogID + '"' +
                                                            ' data-user-id="' + userId + '"' +
                                                            ' data-is-favorite="' + blog.favorite + '"' +
                                                            ' onclick="toggleFavorite(this)"></i>' : '') +
                                                    '</div>' +
                                                    '</div>';

                                            return blogCard;
                                        }

                                        function loadBlogs(offset, limit) {
                                            fetch(`/CheckGrammar/api/blogs?offset=` + offset + `&limit=` + limit)
                                                    .then(response => response.json())
                                                    .then(blogs => {
                                                        const blogContainer = document.getElementById('blog-container');
                                                        blogs.forEach(blog => {
                                                            const blogCard = createBlogCard(blog);
                                                            blogContainer.appendChild(blogCard);
                                                        });
                                                    })
                                                    .catch(error => console.error('Error loading blogs:', error));
                                        }

                                        function searchBlogs() {
                                            const query = document.getElementById('search-input').value.toLowerCase();
                                            const blogCards = document.querySelectorAll('.blog-card');
                                            blogCards.forEach(card => {
                                                const title = card.querySelector('.blog-title').textContent.toLowerCase();
                                                card.style.display = title.includes(query) ? '' : 'none';
                                            });
                                        }


                                        document.addEventListener('DOMContentLoaded', () => {
                                            let offset = 0;
                                            const limit = 6;
                                            const loadMoreBtn = document.getElementById('load-more-btn');

                                            loadBlogs(offset, limit);

                                            if (loadMoreBtn) {
                                                loadMoreBtn.addEventListener('click', () => {
                                                    offset += limit;
                                                    loadBlogs(offset, limit);
                                                });
                                            } else {
                                                console.error('Element with id "load-more-btn" not found.');
                                            }
                                        });
                                    </script>



                                    <style>
                                        .site-content .archive-container .row .content-entry .blog-container .favorite {
                                            color: black;
                                        }

                                        .site-content .archive-container .row .content-entry .blog-container .favorite-active {
                                            color: red;
                                        }

                                    </style>




                                    <div class="membership-plan-options">
                                        <h3>Membership Plan Options</h3>
                                        <ul>
                                            <li>
                                                <strong>Free non-member:</strong> Unlimited grammar check (500 word limit), 30-click max total for “Improve writing” and “Check level”
                                            </li>
                                            <li>
                                                <strong>Free member:</strong> Unlimited grammar check (3000 word limit), 10 clicks per day max for “Improve writing,” “Check level,” and "Score essay"
                                            </li>
                                            <li>
                                                <strong>Paid membership:</strong> Unlimited grammar check (3000 word limit), unlimited access to all features
                                            </li>
                                        </ul>
                                        <p>Members can check 3000 words at a time, save text and feedback, translate feedback into 70 languages, create hypertext narratives and essay outlines, send PDFs with voice recordings, track errors, play error correction games, post essays to forums for additional feedback, with more features on their way.</p>
                                        <h3>Pricing:</h3>
                                        <ul>
                                            <li>One year: $36 USD (< $0.10 per day)</li>
                                            <li>Three months: $16 USD</li>
                                            <li>One month: $8 USD</li>
                                            <li>One week: $3 USD</li>
                                        </ul>
                                        <p>Buy the plan that’s right for you <a href="#">here</a>.</p>
                                        <p>Pay once. There’s nothing to cancel. Subscription prices do NOT renew automatically. And a one-year plan only costs $0.69 USD per week!</p>
                                    </div>
                            </div>
                            <div class="inner-page-wrapper">
                                <h3>Feedbacks</h3>
                                <div style="border: 1px solid"></div>
                                <c:forEach items="${sessionScope.feedbacks}" var="item">
                                    <c:if test="${item.status == 2}">
                                        <div>
                                            <div><h6 style="margin-bottom: 3px">${item.name}</h6></div>
                                            <div><p style="margin: 0;padding: 0">${item.feedback}</p></div>
                                            <div><p style="margin: 0;padding: 0;color: grey;font-style: italic">${item.feedbackDate}</p></div>
                                            <div style="border: 1px solid; width: 60vw; margin-bottom: 3px"></div>
                                        </div>
                                    </c:if>
                                </c:forEach>

                                <nav aria-label="Page navigation">
                                    <ul class="pagination">
                                        <c:if test="${currentPage > 1}">
                                            <li class="page-item">
                                                <a class="page-link" href="?page=${currentPage - 1}&size=${pageSize}" aria-label="Previous">
                                                    <span aria-hidden="true">&laquo;</span>
                                                </a>
                                            </li>
                                        </c:if>
                                        <c:forEach begin="1" end="${totalPages}" var="i">
                                            <li class="page-item <c:if test='${i == currentPage}'>active</c:if>'">
                                                <a class="page-link" href="?page=${i}&size=${pageSize}">${i}</a>
                                            </li>
                                        </c:forEach>
                                        <c:if test="${currentPage < totalPages}">
                                            <li class="page-item">
                                                <a class="page-link" href="?page=${currentPage + 1}&size=${pageSize}" aria-label="Next">
                                                    <span aria-hidden="true">&raquo;</span>
                                                </a>
                                            </li>
                                        </c:if>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

</body>
</html>
