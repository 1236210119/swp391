<%-- 
    Document   : profile
    Created on : May 21, 2024, 11:55:07 PM
    Author     : hung
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-alpha1/dist/js/bootstrap.bundle.min.js">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-alpha1/dist/css/bootstrap.min.css">
    </head>
    <body>
        <jsp:include page="head.jsp"></jsp:include>  
            <div class="container rounded bg-white mt-5 mb-5">
            <c:set var="acc" scope="session" value="${requestScope.acc}"></c:set>
            <form action="${pageContext.request.contextPath}/profile"
                  method="post" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-3 border-right">
                        <div class="d-flex flex-column align-items-center text-center p-3 py-5">
                            <img class="rounded-circle mt-5" width="150px" src="${acc.avatar}">
                            <span class="text-black-50">${acc.email}</span>
                            <input class="text-center" type="file" name="file"
                                   placeholder="Change" accept="image/*">
                        </div>
                    </div>
                    <div class="col-md-9 border-right">
                        <div class="p-3 py-5">
                            <div class="d-flex justify-content-between align-items-center mb-3">
                                <h4 class="text-right">Profile </h4>
                            </div>
                            <div class="row mt-3">
                                <input type="hidden" name="Id" value="${acc.id}">
                                <div class="col-md-12">
                                    <label class="labels">User Name</label>
                                    <input readonly type="text" class="form-control" placeholder="Enter username"
                                           required minlength="8" value="${acc.username}"></div>
                                <div class="col-md-12">
                                    <label class="labels">Mobile Number</label>
                                    <input type="text" class="form-control" required pattern="\d{1,10}$"
                                           placeholder="enter phone number" name="phone" value="${acc.phone}"></div>
                                <div class="col-md-12">
                                    <label class="labels">Address Line</label>
                                    <input type="text" class="form-control" 
                                           placeholder="Enter address" name="address" value="${acc.address}"></div>
                                <div class="col-md-12">
                                    <label class="labels">Full name</label>
                                    <input type="text" class="form-control" placeholder="Enter full name"
                                           required name="fullname" value="${acc.fullname}"></div>
                                <div class="mt-5 text-center">
                                    <button type="submit">Save</button>
                                    <button type="button" onclick="window.location.assign('view-profile')">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <jsp:include page="footer.jsp"></jsp:include> 
    </body>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.jsdelivr"></script>
</html>
