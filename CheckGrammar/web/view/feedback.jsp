<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Feedback Page</title>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-alpha1/dist/css/bootstrap.min.css">
        <link
            data-minify="1"
            rel="stylesheet"
            href="css/feedback.css"
            type="text/css"
            media="all"
            />
        <style>
            .modal {
                z-index: 1050 !important;
            }
            .primary-navigation {
                z-index: 1040;
                position: relative; /* Use relative or fixed */
            }
            /* Ensure the modal is always on top */
            .modal-backdrop {
                z-index: 1049 !important;
            }
        </style>
    </head>
    <body>
        <div style="margin-top: 80px" id="feedback-form-wrapper">
            <div style="margin-top: 80px"  id="feedback-form-modal">
                <div style="margin-top: 150px"  class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Feedback Form</h5>
                            </div>
                            <form action="${pageContext.request.contextPath}/feedback" method="post">
                                <div class="modal-body">
                                    <div class="form-group">
                                        <label for="input-two">Would you like to say something?</label>
                                        <textarea class="form-control" minlength="10" id="input-two" name="feedback" required rows="3"></textarea>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@11/dist/sweetalert2.min.css">
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>                    
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
        <script>
            function showFeedbackModal(event) {
                event.preventDefault();
                var myModal = new bootstrap.Modal(document.getElementById('exampleModal'));
                myModal.show();
            }
       
        </script>
        
        
         <script>
        window.onload = function () {
            // L?y th�ng b�o v� lo?i th�ng b�o t? session
            var alertMessage = '<%= session.getAttribute("alertMessage") != null ? session.getAttribute("alertMessage") : ""%>';
            var alertType = '<%= session.getAttribute("alertType") != null ? session.getAttribute("alertType") : ""%>';
            // Ki?m tra n?u th�ng b�o v� lo?i th�ng b�o kh�ng r?ng
            if (alertMessage && alertType) {
                Swal.fire({
                    icon: alertType,
                    title: alertMessage,
                    showConfirmButton: false,
                    timer: 2000
                });
                // X�a attribute alertMessage v� alertType kh?i session sau khi hi?n th? th�ng b�o
        <%
            session.removeAttribute("alertMessage");
            session.removeAttribute("alertType");
        %>
            }
        };
    </script>   
    </body>
</html>
