<%-- 
    Document   : manageAccount
    Created on : May 28, 2024, 11:43:13 PM
    Author     : hung
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Manage Account</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" 
              rel="stylesheet"
              integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" 
              crossorigin="anonymous">
        <link rel="stylesheet" href="css/sidebar.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css"
              integrity="sha512-iecdLmaskl7CVkqkXNQ/ZH/XLlvWZOJyj7Yy7tcenmpD1ypASozpmT/E0iPtmFIB46ZmdtAc9eNBvH0H/ZpiBw=="
              crossorigin="anonymous" referrerpolicy="no-referrer" />
        <link href="../../css/style.css" rel="stylesheet" type="text/css"/>

    </head>
    <body>
        <div class="container-fluid p-0 d-flex h-100">
            <div id="bdSidebar" 
                 class="d-flex flex-column flex-shrink-0 p-3 bg-success text-white offcanvas-md offcanvas-start">
                <a href="home" class="navbar-brand">HOME</a><hr>
                <ul class="mynav nav nav-pills flex-column mb-auto">
                    <li class="nav-item mb-1">
                        <a href="profile">
                            <i class="fa-regular fa-user"></i>
                            Profile
                        </a>
                    </li>
                    <li class="nav-item mb-1">
                        <a href="admin-feedback">
                            <i class="fa fa-commenting" aria-hidden="true"></i>
                            Manage Feedback
                        </a>
                    </li>
                    <li class="nav-item mb-1">
                        <a href="manage-account">
                            <i class="fa fa-commenting" aria-hidden="true"></i>
                            Manage Account
                        </a>
                    </li>
                    <li class="nav-item mb-1">
                        <a href="manage-blog">
                            <i class="fa fa-commenting" aria-hidden="true"></i>
                            manage-blog
                        </a>
                    </li>
                    <li class="sidebar-item  nav-item mb-1">
                        <a href="#" class="sidebar-link collapsed" data-bs-toggle="collapse" data-bs-target="#settings" aria-expanded="false" aria-controls="settings">
                            <i class="fas fa-cog pe-2"></i>
                            <span class="topic">Settings </span>
                        </a>
                        <ul id="settings" class="sidebar-dropdown list-unstyled collapse" data-bs-parent="#sidebar">
                            <li class="sidebar-item">
                                <a href="#" class="sidebar-link">
                                    <i class="fas fa-sign-in-alt pe-2"></i>
                                    <span class="topic"> Login</span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a href="#" class="sidebar-link">
                                    <i class="fas fa-user-plus pe-2"></i>
                                    <span class="topic">Register</span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a href="#" class="sidebar-link">
                                    <i class="fas fa-sign-out-alt pe-2"></i>
                                    <span class="topic">Log Out</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
                <hr>
                <div class="d-flex">
                    <i class="fa-solid fa-book  me-2"></i>
                    <span>
                        <h6 class="mt-1 mb-0">Grammar Tool</h6>
                    </span>
                </div>
            </div>
            <div class="bg-light flex-fill">
                <div class="p-2 d-md-none d-flex text-white bg-success">
                    <a href="#" class="text-white" data-bs-toggle="offcanvas" data-bs-target="#bdSidebar">
                        <i class="fa-solid fa-bars"></i>
                    </a>
                </div>
                <div class="p-4">
                    <nav style="--bs-breadcrumb-divider:'>';font-size:14px">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <i class="fa-solid fa-house"></i>
                            </li>
                            <li class="breadcrumb-item">Manage Account</li>
                        </ol>
                    </nav>
                    <!-- Search Form -->

                    <hr>
                    <div class="row justify-content-center mt-4">
                        <div class="col-12 col-md-8">
                            <div class="card">
                                <div class="card-footer bg-light text-right">
                                    <form action="${pageContext.request.contextPath}/manage-blog" method="post" style="display: inline;">
                                        <input type="hidden" name="action" value="update">
                                        <input type="hidden" value="${blog.blogID}" name="blogID">
                                        <button class="btn btn-primary" type="submit">Update</button>
                                    </form>
                                </div>
                                <div class="card-header bg-primary text-white">
                                    <h2>${blog.title}</h2>
                                </div>
                                <div class="card-body">
                               
                                    <p><strong>Detail:</strong></p>
                                    <p>${blog.detail}</p>
                                  
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </body>
</html>
