<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Manage Account</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" 
              rel="stylesheet"
              integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" 
              crossorigin="anonymous">
        <link rel="stylesheet" href="css/sidebar.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css"
              integrity="sha512-iecdLmaskl7CVkqkXNQ/ZH/XLlvWZOJyj7Yy7tcenmpD1ypASozpmT/E0iPtmFIB46ZmdtAc9eNBvH0H/ZpiBw=="
              crossorigin="anonymous" referrerpolicy="no-referrer" />
        <link href="../../css/style.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div class="container-fluid p-0 d-flex h-100">
            <div id="bdSidebar" 
                 class="d-flex flex-column flex-shrink-0 p-3 bg-success text-white offcanvas-md offcanvas-start">
                <a href="home" class="navbar-brand">HOME</a><hr>
                <ul class="mynav nav nav-pills flex-column mb-auto">
                    <li class="nav-item mb-1">
                        <a href="profile">
                            <i class="fa-regular fa-user"></i>
                            Profile
                        </a>
                    </li>
                    <li class="nav-item mb-1">
                        <a href="admin-feedback">
                            <i class="fa fa-commenting" aria-hidden="true"></i>
                            Manage Feedback
                        </a>
                    </li>
                    <li class="nav-item mb-1">
                        <a href="manage-account">
                            <i class="fa fa-commenting" aria-hidden="true"></i>
                            Manage Account
                        </a>
                    </li>
                    </li>
                    <li class="nav-item mb-1">
                        <a href="manage-blog">
                            <i class="fa fa-commenting" aria-hidden="true"></i>
                            Manage Blog
                        </a>
                    </li>
                    <li class="sidebar-item nav-item mb-1">
                        <a href="#" class="sidebar-link collapsed" data-bs-toggle="collapse" data-bs-target="#settings" aria-expanded="false" aria-controls="settings">
                            <i class="fas fa-cog pe-2"></i>
                            <span class="topic">Settings</span>
                        </a>
                        <ul id="settings" class="sidebar-dropdown list-unstyled collapse" data-bs-parent="#sidebar">
                            <li class="sidebar-item">
                                <a href="#" class="sidebar-link">
                                    <i class="fas fa-sign-in-alt pe-2"></i>
                                    <span class="topic">Login</span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a href="#" class="sidebar-link">
                                    <i class="fas fa-user-plus pe-2"></i>
                                    <span class="topic">Register</span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a href="#" class="sidebar-link">
                                    <i class="fas fa-sign-out-alt pe-2"></i>
                                    <span class="topic">Log Out</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
                <hr>
                <div class="d-flex">
                    <i class="fa-solid fa-book me-2"></i>
                    <span>
                        <h6 class="mt-1 mb-0">Grammar Tool</h6>
                    </span>
                </div>
            </div>
            <div class="bg-light flex-fill">
                <div class="p-2 d-md-none d-flex text-white bg-success">
                    <a href="#" class="text-white" data-bs-toggle="offcanvas" data-bs-target="#bdSidebar">
                        <i class="fa-solid fa-bars"></i>
                    </a>
                </div>
                <div class="p-4">
                    <nav style="--bs-breadcrumb-divider:'>';font-size:14px">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <i class="fa-solid fa-house"></i>
                            </li>
                            <li class="breadcrumb-item">Manage Blog</li>
                        </ol>
                    </nav>
                    <div class="row">
                        <div class="col-12 col-md-8 offset-md-2">
                            <div class="card">
                                <div class="card-header">
                                    <h2>Update Blog</h2>
                                </div>
                                <div class="card-body">
                                    <form id="blogForm">
                                        <input type="hidden" id="blogID" name="blogID" value="${blog.blogID}">

                                        <div class="mb-3">
                                            <label for="title" class="form-label">Title</label>
                                            <input type="text" class="form-control" id="title" name="title" value="${blog.title}" required>
                                        </div>
                                        <div class="mb-3">
                                            <label for="detail" class="form-label">Detail</label>
                                            <textarea class="form-control" id="detail" name="detail" rows="3" required>${blog.detail}</textarea>
                                        </div>

                                        <div class="text-right">
                                            <button type="submit" class="btn btn-primary">Update</button>
                                            <a href="${pageContext.request.contextPath}/manage-blog" class="btn btn-secondary">Cancel</a>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      <script>
            document.addEventListener("DOMContentLoaded", function () {
                tinymce.init({
                    selector: 'textarea#detail',
                    plugins: 'anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount checklist mediaembed casechange export formatpainter pageembed linkchecker a11ychecker tinymcespellchecker permanentpen powerpaste advtable advcode editimage advtemplate ai mentions tinycomments tableofcontents footnotes mergetags autocorrect typography inlinecss markdown',
                    toolbar: 'undo redo | blocks fontfamily fontsize | bold italic underline strikethrough | link image media table mergetags | addcomment showcomments | spellcheckdialog a11ycheck typography | align lineheight | checklist numlist bullist indent outdent | emoticons charmap | removeformat',
                    tinycomments_mode: 'embedded',
                    tinycomments_author: 'Author name',
                    mergetags_list: [
                        {value: 'First.Name', title: 'First Name'},
                        {value: 'Email', title: 'Email'},
                    ],
                    setup: function (editor) {
                        editor.on('change', function () {
                            editor.save();
                        });
                    }
                });

                document.getElementById("blogForm").addEventListener("submit", submitForm);
            });

            function submitForm(event) {
                event.preventDefault();
                tinymce.triggerSave(); // C?p nh?t n?i dung t? TinyMCE v�o textarea
                  window.location.href = "/CheckGrammar/manage-blog";

                const blogID = document.getElementById('blogID').value;
                const title = document.getElementById('title').value;
                const detail = document.getElementById('detail').value;
            

                const formData = {
                    blogID: blogID,
                    title: title,
                    detail: detail,
               
                };

                fetch('/CheckGrammar/api/updateBlog', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify(formData),
                })
                        .then(response => response.json())
                        .then(data => {
                            if (data.success) {
                                alert(data.message);
                                window.location.href = '/manage-blog';
                            } else {
                                alert(data.message);
                            }
                        })
                        .catch(error => {
                            console.error('Error:', error);
                        });
            }
        </script>

    <script src="https://cdn.tiny.cloud/1/a8bupn4z7i2ylh2d0n3ay615vkpivibpcfnwzhuo4l7dn8s5/tinymce/7/tinymce.min.js" referrerpolicy="origin"></script>
    <script src="<c:url value='/js/bootstrap.bundle.min.js' />"></script>
</body>
</html>