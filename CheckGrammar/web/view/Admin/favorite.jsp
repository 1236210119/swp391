<%-- 
    Document   : profile
    Created on : May 21, 2024, 11:55:07 PM
    Author     : hung
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
           <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-alpha1/dist/js/bootstrap.bundle.min.js">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-alpha1/dist/css/bootstrap.min.css">
 <style>
    body {
        font-family: Arial, sans-serif;
    }
    .blog-container {
        margin-top: 20px;
    }
    .blog-card {
        border: 1px solid #ddd;
        border-radius: 10px;
        overflow: hidden;
        margin-bottom: 20px;
        transition: transform 0.3s;
    }
    .blog-card:hover {
        transform: translateY(-10px);
    }
    .blog-image {
        width: 100%;
        height: auto;
        object-fit: cover;
    }
    .blog-content {
        padding: 15px;
    }
    .blog-title {
        font-size: 1.2rem;
        margin-bottom: 10px;
    }
    .btn-read-more {
        display: inline-block;
        margin-top: 10px;
        color: #007bff;
        text-decoration: none;
    }
    .btn-read-more:hover {
        text-decoration: underline;
    }
</style>
    </head>
    <body>
        <jsp:include page="../head.jsp"></jsp:include>  
            <div class="container rounded bg-white mt-5 mb-5">
                <div id="blog-container" class="blog-container">
                    <!-- Các blog sẽ được thêm vào đây -->
                <c:forEach var="blog" items="${blogas}">
                    <div class="blog-card">
                       <img src="uploads/${blog.image}" alt="${blog.image}" class="blog-image" style="max-height: 250px; width: auto;">

                        <div class="blog-content">
                            <h2 class="blog-title">${blog.title}</h2>

                            <a href="blog-detail-user?blogID=${blog.id}" class="btn-read-more">Read more</a>
                            <i  class="fas fa-heart favorite ${blog.favorite ? 'favorite-active' : ''}" 
                               data-blog-id="${blog.id}" 
                               data-is-favorite="${blog.favorite}" 
                               onclick="toggleFavorite(this)"></i>

                        </div>
                    </div>


                </c:forEach>
            </div>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>


            <script>
                function toggleFavorite(element) {
                    var blogId = element.getAttribute("data-blog-id");
                    var isFavorite = element.getAttribute("data-is-favorite") === "true";

                    $.ajax({
                        url: '/CheckGrammar/toggleFavorite',
                        type: 'POST',
                        data: {
                            blogId: blogId,
                            isFavorite: isFavorite
                        },
                        success: function (response) {
                            if (response.status === 'success') {
                                     window.location.reload();
                            }
                        },
                        error: function (error) {
                            console.error('Error:', error);
                        }
                    });
                }

            </script>

            <style>
                .container .blog-container .blog-card .favorite {
                    color: black;
                }

                       .container .blog-container .blog-card  .favorite-active {
                    color: red;
                }

            </style>
              <div style="text-align: center;" class="load-more-container">
                                            <button  class="load-more-btn" id="load-more-btn">Load more</button>
                                        </div>


        </div>
        <jsp:include page="../footer.jsp"></jsp:include> 
    </body>

    <script>
        document.addEventListener('DOMContentLoaded', () => {
            let offset = 6;
            const limit = 6;
            const loadMoreBtn = document.getElementById('load-more-btn');
            const blogContainer = document.getElementById('blog-container');

            if (loadMoreBtn && blogContainer) {
                loadMoreBtn.addEventListener('click', () => {
                    fetch(`/CheckGrammar/toggleFavorite?action=loadMore&offset=` + offset + `&limit=` + limit)
                            .then(response => response.json())
                            .then(blogs => {
                                blogs.forEach(blog => {
                                    const blogCard = document.createElement('div');
                                    blogCard.classList.add('blog-card');
                                    blogCard.innerHTML =
                                            '<img src="uploads/' + blog.image + '" alt="' + blog.image + '" class="blog-image"  style="max-height: 250px; width: auto;">' +
                                            '<div class="blog-content">' +
                                            '<h2 class="blog-title">' + blog.title + '</h2>' +
                                            '<a href="blog-detail-user?blogID=' + blog.id + '" class="btn-read-more">Read more</a>' +
                                            '</div>';

                                    const favoriteIcon = document.createElement('i');
                                    favoriteIcon.classList.add('fas', 'fa-heart', 'favorite');
                                    if (blog.favorite) {
                                        favoriteIcon.classList.add('favorite-active');
                                    }
                                    favoriteIcon.setAttribute('data-blog-id', blog.id);
                                    favoriteIcon.setAttribute('data-is-favorite', blog.favorite);
                                    favoriteIcon.setAttribute('onclick', 'toggleFavorite(this)');

                                    blogContainer.appendChild(blogCard);
                                    blogContainer.appendChild(favoriteIcon);
                                });
                                offset += limit;
                            })
                            .catch(error => console.error('Error loading more blogs:', error));
                });
            } else {
                console.error('Element with id "load-more-btn" or "blog-container" not found.');
            }
        });

    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.jsdelivr"></script>
</html>
