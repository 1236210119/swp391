<%-- 
    Document   : manageAccount
    Created on : May 28, 2024, 11:43:13 PM
    Author     : hung
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Manage Account</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" 
              rel="stylesheet"
              integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" 
              crossorigin="anonymous">
        <link rel="stylesheet" href="css/sidebar.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css"
              integrity="sha512-iecdLmaskl7CVkqkXNQ/ZH/XLlvWZOJyj7Yy7tcenmpD1ypASozpmT/E0iPtmFIB46ZmdtAc9eNBvH0H/ZpiBw=="
              crossorigin="anonymous" referrerpolicy="no-referrer" />
        <link href="../../css/style.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div class="container-fluid p-0 d-flex h-100">
            <div id="bdSidebar" 
                 class="d-flex flex-column flex-shrink-0 p-3 bg-success text-white offcanvas-md offcanvas-start">
                <a href="home" class="navbar-brand">HOME</a><hr>
                <ul class="mynav nav nav-pills flex-column mb-auto">
                    <li class="nav-item mb-1">
                        <a href="profile">
                            <i class="fa-regular fa-user"></i>
                            Profile
                        </a>
                    </li>
                    <li class="nav-item mb-1">
                        <a href="admin-feedback">
                            <i class="fa fa-commenting" aria-hidden="true"></i>
                            Manage Feedback
                        </a>
                    </li>
                    <li class="nav-item mb-1">
                        <a href="manage-account">
                            <i class="fa fa-commenting" aria-hidden="true"></i>
                            Manage Account
                        </a>
                    </li>
                    </li>
                    <li class="nav-item mb-1">
                        <a href="manage-blog">
                            <i class="fa fa-commenting" aria-hidden="true"></i>
                            Manage-Blog
                        </a>
                    </li>
                    <li class="sidebar-item  nav-item mb-1">
                        <a href="#" class="sidebar-link collapsed" data-bs-toggle="collapse" data-bs-target="#settings" aria-expanded="false" aria-controls="settings">
                            <i class="fas fa-cog pe-2"></i>
                            <span class="topic">Settings </span>
                        </a>
                        <ul id="settings" class="sidebar-dropdown list-unstyled collapse" data-bs-parent="#sidebar">
                            <li class="sidebar-item">
                                <a href="#" class="sidebar-link">
                                    <i class="fas fa-sign-in-alt pe-2"></i>
                                    <span class="topic"> Login</span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a href="#" class="sidebar-link">
                                    <i class="fas fa-user-plus pe-2"></i>
                                    <span class="topic">Register</span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a href="#" class="sidebar-link">
                                    <i class="fas fa-sign-out-alt pe-2"></i>
                                    <span class="topic">Log Out</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
                <hr>
                <div class="d-flex">
                    <i class="fa-solid fa-book  me-2"></i>
                    <span>
                        <h6 class="mt-1 mb-0">Grammar Tool</h6>
                    </span>
                </div>
            </div>
            <div class="bg-light flex-fill">
                <div class="p-2 d-md-none d-flex text-white bg-success">
                    <a href="#" class="text-white" data-bs-toggle="offcanvas" data-bs-target="#bdSidebar">
                        <i class="fa-solid fa-bars"></i>
                    </a>
                </div>
                <div class="p-4">
                    <nav style="--bs-breadcrumb-divider:'>';font-size:14px">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <i class="fa-solid fa-house"></i>
                            </li>
                            <li class="breadcrumb-item">Manage Account</li>
                        </ol>
                    </nav>
                    <!-- Search Form -->
                    <form action="${pageContext.request.contextPath}/manage-blog" method="get" class="mb-3">
                        <div class="row">
                            <div class="col-md-6 mb-2">
                                <input type="text" class="form-control" name="search" placeholder="Search by UserID, Title " value="${param.search}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <button type="submit" class="btn btn-primary">Search</button>
                            </div>
                        </div>
                    </form>
                    <hr>
                    <div class="row mb-3">
                        <div class="col">

                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="row">
                                <div class="col">
                                    <h4>Blog</h4>
                                </div>
                                <div class="col">
                                    <form action="${pageContext.request.contextPath}/manage-blog" method="post" class="form-inline">
                                        <input type="hidden" name="action" value="create">
                                        <button type="submit" class="btn btn-success">Create New Blog</button>
                                    </form>
                                </div>
                            </div>


                            <hr>

                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th scope="col">BlogID</th>
                                        <th scope="col">UserID</th>
                                        <th scope="col">Title</th>
                                        <th scope="col">Status</th>
                                        <th scope="col">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:forEach items="${list}" var="item">
                                        <tr>     
                                            <td class="centered">${item.blogID}</td>
                                            <td class="centered">${item.userID}</td>
                                            <td class="centered">${item.title}</td>
                                            <td class="centered">
                                                <c:choose>
                                                    <c:when test="${item.isStatus == 1}">
                                                        Active
                                                    </c:when>
                                                    <c:otherwise>
                                                        Inactive
                                                    </c:otherwise>
                                                </c:choose>
                                            </td>

                                            <td class="d-flex gap-2 centered">
                                                <c:if test="${item.isStatus == 0}">
                                                    <form action="${pageContext.request.contextPath}/manage-blog" method="post">
                                                        <input type="hidden" value="${item.blogID}" name="blogID">
                                                        <input type="hidden" value="1" name="status">
                                                        <button class="btn btn-success"> Active</button>
                                                    </form>
                                                </c:if>
                                                <c:if test="${item.isStatus == 1}">
                                                    <form action="${pageContext.request.contextPath}/manage-blog" method="post">
                                                        <input type="hidden" value="${item.blogID}" name="blogID">
                                                        <input type="hidden" value="0" name="status">
                                                        <button class="btn btn-warning">Inactive</button>
                                                    </form>
                                                </c:if>
                                                <form action="${pageContext.request.contextPath}/manage-blog" method="post">
                                                    <input type="hidden" name="action" value="delete">
                                                    <input type="hidden" value="${item.blogID}" name="blogID">
                                                    <button class="btn btn-danger" type="submit" onclick="return confirm('Are you sure you want to delete this blog?')">Delete</button>
                                                </form>
                                                <form action="${pageContext.request.contextPath}/manage-blog" method="post">
                                                    <input type="hidden" name="action" value="viewDetails">
                                                    <input type="hidden" value="${item.blogID}" name="blogID">
                                                    <button class="btn btn-info" type="submit">View Details</button>
                                                </form>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <c:if test="${endPage > 1}">
                        <div class="d-flex justify-content-center mt-1">
                            <nav aria-label="Page navigation example col-12">
                                <ul class="pagination">

                                    <c:if test="${currentPage != 1}">
                                        <li class="page-item">
                                            <a class="page-link" href="manage-blog?page=${currentPage - 1}" aria-label="Previous">
                                                <span aria-hidden="true">&laquo;</span>
                                            </a>
                                        </li>
                                    </c:if>

                                    <c:forEach begin="1" end="${endPage}" var="i">
                                        <c:choose>
                                            <c:when test="${currentPage eq i}">
                                                <li class="page-item"><a class="page-link bg-light">${i}</a></li>
                                                </c:when>
                                                <c:otherwise>
                                                <li class="page-item"><a class="page-link" href="manage-blog?page=${i}">${i}</a></li>
                                                </c:otherwise>
                                            </c:choose>
                                        </c:forEach>

                                    <c:if test="${currentPage lt endPage}">
                                        <li class="page-item">
                                            <a class="page-link" href="manage-blog?page=${currentPage + 1}" aria-label="Next">
                                                <span aria-hidden="true">&raquo;</span>
                                            </a>
                                        </li>
                                    </c:if>
                                </ul>
                            </nav>
                        </div>
                    </c:if>
                </div>

            </div>
        </div>
    </body>
</html>
