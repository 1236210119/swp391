<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Blog Detail</title>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-alpha1/dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css" integrity="sha512-iecdLmaskl7CVkqkXNQ/ZH/XLlvWZOJyj7Yy7tcenmpD1ypASozpmT/E0iPtmFIB46ZmdtAc9eNBvH0H/ZpiBw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        <style>
            body {
                font-family: Arial, sans-serif;
                background-color: #f8f9fa;
            }
            .container {
                margin-top: 50px;
            }
            .card {
                margin-bottom: 30px;
            }
            .card-header h2 {
                margin-bottom: 0;
            }
            .card-footer {
                background-color: #f8f9fa;
            }
            .blog-card {
                margin: 20px 0;
            }
            .blog-image {
                max-width: 100%;
                height: auto;
                display: block;
                margin-bottom: 20px;
            }
            .blog-title {
                font-size: 2em;
                margin-bottom: 10px;
            }
            .blog-content {
                font-size: 1.2em;
            }
        </style>
    </head>
    <body>
                <jsp:include page="../head.jsp"></jsp:include>  

        <div class="container rounded bg-white mt-5 mb-5">
            <div id="blog-container" class="blog-container">
                <div class="card">
                    <div class="card-header text-center">
                        <h2 class="blog-title">${blog.title}</h2>
                    </div>
                    <div class="card-body">
                        <c:if test="${not empty blog.blogImage}">
                            <img src="uploads/${blog.blogImage}" alt="${blog.title}" class="blog-image">
                        </c:if>
                            <p class="text-muted"><strong>Created By:</strong> ${blog.nameadmin}</p>
                        <p class="text-muted"><strong>Date: </strong> ${blog.create}</p>
                        <p><strong></strong></p>
                        <c:out value="${blog.detail}" escapeXml="false"/>
                        <hr>

                    </div>

                </div>
            </div>
        </div>
                                <jsp:include page="../footer.jsp"></jsp:include> 

        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-alpha1/dist/js/bootstrap.bundle.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    </body>
</html>
