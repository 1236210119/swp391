<%-- 
    Document   : manageFeedback
    Created on : May 28, 2024, 11:43:13 PM
    Author     : hung
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width,
              initial-scale=1.0">
        <title>Bootstrap5 Sidebar</title>
        <link href=
              "https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" 
              rel="stylesheet"
              integrity=
              "sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" 
              crossorigin="anonymous">
        <link rel="stylesheet" 
              href="css/sidebar.css">
        <link rel="stylesheet" href=
              "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css"
              integrity=
              "sha512-iecdLmaskl7CVkqkXNQ/ZH/XLlvWZOJyj7Yy7tcenmpD1ypASozpmT/E0iPtmFIB46ZmdtAc9eNBvH0H/ZpiBw=="
              crossorigin="anonymous" 
              referrerpolicy="no-referrer" />
    </head>

    <body>

        <div class="container-fluid p-0 d-flex h-100">
            <div id="bdSidebar" 
                 class="d-flex flex-column
                 flex-shrink-0
                 p-3 bg-success
                 text-white offcanvas-md offcanvas-start">
                <a href="home" 
                   class="navbar-brand">HOME
                </a><hr>
                <ul class="mynav nav nav-pills flex-column mb-auto">
                    <li class="nav-item mb-1">
                        <a href="profile">
                            <i class="fa-regular fa-user"></i>
                            Profile
                        </a>
                    </li>

                    <li class="nav-item mb-1">
                        <a href="admin-feedback">
                            <i class="fa fa-commenting" aria-hidden="true"></i>
                            Manage Feedback
                        </a>
                    </li>

                    <li class="nav-item mb-1">
                        <a href="manage-account">
                            <i class="fa fa-commenting" aria-hidden="true"></i>
                            Manage Account
                        </a>
                    </li>
                    <li class="nav-item mb-1">
                        <a href="manage-blog">
                            <i class="fa fa-commenting" aria-hidden="true"></i>
                            Manage Blog
                        </a>
                    </li>

                    <li class="sidebar-item  nav-item mb-1">
                        <a href="#" 
                           class="sidebar-link collapsed" 
                           data-bs-toggle="collapse"
                           data-bs-target="#settings"
                           aria-expanded="false"
                           aria-controls="settings">
                            <i class="fas fa-cog pe-2"></i>
                            <span class="topic">Settings </span>
                        </a>
                        <ul id="settings" 
                            class="sidebar-dropdown list-unstyled collapse" 
                            data-bs-parent="#sidebar">
                            <li class="sidebar-item">
                                <a href="#" class="sidebar-link">
                                    <i class="fas fa-sign-in-alt pe-2"></i>
                                    <span class="topic"> Login</span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a href="#" class="sidebar-link">
                                    <i class="fas fa-user-plus pe-2"></i>
                                    <span class="topic">Register</span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a href="#" class="sidebar-link">
                                    <i class="fas fa-sign-out-alt pe-2"></i>
                                    <span class="topic">Log Out</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
                <hr>
                <div class="d-flex">

                    <i class="fa-solid fa-book  me-2"></i>
                    <span>
                        <h6 class="mt-1 mb-0">
                            Grammar Tool
                        </h6>
                    </span>
                </div>
            </div>

            <div class="bg-light flex-fill">
                <div class="p-2 d-md-none d-flex text-white bg-success">
                    <a href="#" class="text-white" 
                       data-bs-toggle="offcanvas"
                       data-bs-target="#bdSidebar">
                        <i class="fa-solid fa-bars"></i>
                    </a>
                </div>
                <div class="p-4">
                    <nav style="--bs-breadcrumb-divider:'>';font-size:14px">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <i class="fa-solid fa-house"></i>
                            </li>
                            <li class="breadcrumb-item">Manage feedback</li>
                        </ol>
                    </nav>

                    <hr>
                    <div class="row">
                        <div class="col">
                            <h4>Feedback management</h4>
                            <hr>
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th scope="col">Id</th>
                                        <th scope="col">Feeback</th>
                                        <th scope="col">Create at</th>
                                        <th scope="col">Creat by userId</th>
                                        <th scope="col">Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:forEach items="${requestScope.feedbacks}" var="item">
                                        <tr>
                                            <th scope="row">${item.id}</th>
                                            <th scope="row">${item.feedback}</th>
                                            <th scope="row">${item.feedbackDate}</th>
                                            <th scope="row">${item.userId}</th>
                                            <th class="d-flex gap-2" scope="row">
                                                <c:if test="${item.status == 1}">
                                                    <form action="${pageContext.request.contextPath}/admin-feedback"
                                                          method="post">
                                                        <button class="btn btn-success">
                                                            <input type="hidden" value="${item.id}" name="fbId">
                                                            <input type="hidden" value="2" name="status">
                                                            <i class="fa fa-check" aria-hidden="true"></i>
                                                        </button>
                                                    </form>
                                                    <form action="${pageContext.request.contextPath}/admin-feedback" 
                                                          method="post">
                                                        <button class="btn btn-danger">
                                                            <input type="hidden" value="${item.id}" name="fbId">
                                                            <input type="hidden" value="3" name="status">
                                                            <i class="fa fa-times" aria-hidden="true"></i>
                                                        </button>
                                                    </form>
                                                </c:if>
                                                <c:if test="${item.status == 2}">
                                                    <form action="${pageContext.request.contextPath}/admin-feedback" 
                                                          method="post">
                                                        <input type="hidden" value="${item.id}" name="fbId">
                                                        <input type="hidden" value="3" name="status">
                                                        <button class="btn btn-success">
                                                            Rejected
                                                        </button>
                                                    </form>
                                                </c:if>
                                                <c:if test="${item.status == 3}">
                                                    <form action="${pageContext.request.contextPath}/admin-feedback" 
                                                          method="post">
                                                        <input type="hidden" value="${item.id}" name="fbId">
                                                        <input type="hidden" value="2" name="status">
                                                        <button class="btn btn-danger">
                                                             Approved
                                                        </button>
                                                    </form>
                                                </c:if>
                                            </th>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </table>

                            <!-- Pagination Controls -->
    <div class="d-flex justify-content-center align-items-center">
                                <ul class="pagination">
                                    <c:if test="${currentPage > 1}">
                                        <li class="page-item">
                                            <a class="page-link" href="?page=${currentPage - 1}" aria-label="Previous">
                                                <span aria-hidden="true">&laquo;</span>
                                            </a>
                                        </li>
                                    </c:if>
                                    <c:forEach begin="1" end="${totalPages}" var="i">
                                        <li class="page-item ${i == currentPage ? 'active' : ''}">
                                            <a class="page-link" href="?page=${i}">${i}</a>
                                        </li>
                                    </c:forEach>
                                    <c:if test="${currentPage < totalPages}">
                                        <li class="page-item">
                                            <a class="page-link" href="?page=${currentPage + 1}" aria-label="Next">
                                                <span aria-hidden="true">&raquo;</span>
                                            </a>
                                        </li>
                                    </c:if>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </body>

</html>

