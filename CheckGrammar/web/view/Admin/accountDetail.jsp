<%-- 
    Document   : accountDetails
    Created on : [Date]
    Author     : hung
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Account Details</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" 
              rel="stylesheet"
              integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" 
              crossorigin="anonymous">
        <link href="../../css/style.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div class="container mt-5">
            <h2>Account Details</h2>
            <hr>
            <div class="row">
                <div class="col-md-6">
                    <p><strong>UserID:</strong> ${account.id}</p>
                    <p><strong>Username:</strong> ${account.username}</p>
                    <p><strong>Email:</strong> ${account.email}</p>
                    <p><strong>Full Name:</strong> ${account.fullname}</p>
                    <p><strong>Address:</strong> ${account.address}</p>
                    <p><strong>Phone:</strong> ${account.phone}</p>
                    <p><strong>Payment Status:</strong>
                        <c:choose>
                            <c:when test="${account.isPayfee == 1}">
                                Paid Member
                            </c:when>
                            <c:otherwise>
                                Free Member
                            </c:otherwise>
                        </c:choose>
                    </p>
                    <p><strong>Account Status:</strong>
                        <c:choose>
                            <c:when test="${account.isStatus == 1}">
                                Active
                            </c:when>
                            <c:otherwise>
                                Blocked
                            </c:otherwise>
                        </c:choose>
                    </p>
                </div>
                <div class="col-md-6">
                    <p><strong>Avatar:</strong></p>
                    <img src="${account.avatar}" alt="Avatar" class="img-thumbnail" width="150px">
                </div>
            </div>
            <div class="mt-4">
                <a href="${pageContext.request.contextPath}/manage-account" class="btn btn-primary">Back to Manage Account</a>
            </div>
        </div>
    </body>
</html>
