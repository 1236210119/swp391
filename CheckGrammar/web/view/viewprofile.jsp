<%-- 
    Document   : profile
    Created on : May 21, 2024, 11:55:07 PM
    Author     : hung
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Profile Page</title>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-alpha1/dist/css/bootstrap.min.css">
        <style>
            /* Style for the popup form */
            .popup-form {
                display: none;
                position: fixed;
                top: 50%;
                left: 50%;
                transform: translate(-50%, -50%);
                border: 1px solid #ccc;
                background-color: #fff;
                z-index: 1000;
                padding: 20px;
                box-shadow: 0 5px 15px rgba(0,0,0,0.3);
            }
            .popup-overlay {
                display: none;
                position: fixed;
                top: 0;
                left: 0;
                width: 100%;
                height: 100%;
                background-color: rgba(0, 0, 0, 0.5);
                z-index: 999;
            }
        </style>
    </head>
    <body>
        <jsp:include page="head.jsp"></jsp:include>  
            <div class="container rounded bg-white mt-5 mb-5">
            <c:set var="acc" scope="session" value="${requestScope.acc}"></c:set>
                <div class="row">
                    <div class="col-md-3 border-right">
                        <div class="d-flex flex-column align-items-center text-center p-3 py-5">
                            <img class="rounded-circle mt-5" width="150px" src="${acc.avatar}"  alt="${acc.avatar}">
                        <span class="text-black-50">${acc.email}</span>
                    </div>
                </div>
                <div class="col-md-9 border-right">
                    <div class="p-3 py-5">
                        <div class="d-flex justify-content-between align-items-center mb-3">
                            <h4 class="text-right">Profile</h4>
                        </div>
                        <div class="row mt-3">
                            <div class="col-md-12">
                                <label class="labels">User Name</label>
                                <input readonly type="text" class="form-control" value="${acc.username}">
                            </div>
                            <div class="col-md-12">
                                <label class="labels">Mobile Number</label>
                                <input readonly type="text" class="form-control" value="${acc.phone}">
                            </div>
                            <div class="col-md-12">
                                <label class="labels">Address Line</label>
                                <input readonly type="text" class="form-control" value="${acc.address}">
                            </div>
                            <div class="col-md-12">
                                <label class="labels">Full name</label>
                                <input readonly type="text" class="form-control" value="${acc.fullname}">
                            </div>
                            <div class="mt-5 text-center">
                                <button class="btn btn-primary" id="editButton">Edit</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Popup Overlay -->
        <div class="popup-overlay" id="popupOverlay"></div>

        <!-- Popup Form -->
        <div class="popup-form" id="popupForm">
            <form action="${pageContext.request.contextPath}/profile" method="post" enctype="multipart/form-data" onsubmit="return validateForm()">
                <input type="hidden" name="Id" value="${acc.id}">
                <div class="form-group">
                    <label for="phone">Mobile Number</label>
                    <input type="text" class="form-control" id="phone" name="phone" value="${acc.phone}" required>
                    <small id="phoneHelp" class="form-text text-danger"></small>
                </div>
                <div class="form-group">
                    <label for="address">Address Line</label>
                    <input type="text" class="form-control" id="address" name="address" value="${acc.address}" required>
                </div>
                <div class="form-group">
                    <label for="fullname">Full Name</label>
                    <input type="text" class="form-control" id="fullname" name="fullname" value="${acc.fullname}" required>
                </div>
                <div class="form-group">
                    <label for="file">Profile Picture</label>
                    <input type="file" class="form-control" id="file" name="file" accept="image/*">
                </div>
                <div class="mt-3 text-center">
                    <button type="submit" class="btn btn-success">Save</button>
                    <button type="button" class="btn btn-secondary" id="cancelButton">Cancel</button>
                </div>
            </form>
        </div>

        <jsp:include page="footer.jsp"></jsp:include> 
    </body>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#editButton').click(function () {
                $('#popupOverlay').show();
                $('#popupForm').show();
            });
            $('#cancelButton').click(function () {
                $('#popupOverlay').hide();
                $('#popupForm').hide();
            });
        });
    </script>
    <script>
    function validateForm() {
        var phoneInput = document.getElementById('phone');
        var phoneValue = phoneInput.value.trim();

     //  10 chữ số bắt đầu bằng '0'
        var phoneRegex = /^0\d{9}$/;

        if (!phoneRegex.test(phoneValue)) {
            var phoneHelp = document.getElementById('phoneHelp');
            phoneHelp.textContent = 'Phone number must start with 0 and have 10 digits.';
            return false; // Prevent form submission
        }

        return true; // Allow form submission
    }

    document.getElementById('phone').addEventListener('input', function() {
        document.getElementById('phoneHelp').textContent = '';
    });
</script>
</html>
